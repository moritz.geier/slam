#pragma once

// eigen
#include <Eigen/Dense>
#include <Eigen/Geometry>

// std
#include <chrono>
#include <concepts>
#include <string>
#include <vector>

///////////////////////////////////////////////////////////////////////////
namespace utils::concepts
{
    ///////////////////////////////////////////////////////////////////////////
    template<typename Vector>
    concept AnyEigenVector = std::same_as<Vector, Eigen::Vector<float, Vector::RowsAtCompileTime>>;

    ///////////////////////////////////////////////////////////////////////////
    template<typename Transform>
    concept AnyTransformation = std::same_as<Transform, Eigen::Transform<float, Transform::VectorType::RowsAtCompileTime, Eigen::AffineCompact>>;

    ///////////////////////////////////////////////////////////////////////////
    template<typename Pointcloud>
    concept AnyPointcloud = requires(Pointcloud p)
    {
        std::same_as<decltype(p), std::vector<typename Pointcloud::value_type>>;
        AnyEigenVector<decltype(p.front())>;
    };

    ///////////////////////////////////////////////////////////////////////////
    template<typename Step>
    concept Timestep = requires(Step step)
    {
        AnyPointcloud<decltype(step.measurements)>;
        AnyTransformation<decltype(step.pose)>;
        { decltype(step.measurements)::value_type::RowsAtCompileTime == decltype(step.pose)::VectorType::RowsAtCompileTime };
    };
}

///////////////////////////////////////////////////////////////////////////
namespace utils::definitions
{
    ///////////////////////////////////////////////////////////////////////////
    template<typename T>
    using Point2D = Eigen::Vector2<T>;

    ///////////////////////////////////////////////////////////////////////////
    template<int Dimensions>
    using Transform = Eigen::Transform<float, Dimensions, Eigen::AffineCompact>;

    ///////////////////////////////////////////////////////////////////////////
    struct Timestep
    {
        /// @brief Measurement points relative to the robot position
        std::vector<Point2D<float>> measurements;
        Eigen::AffineCompact2f pose;
        std::chrono::microseconds timestamp;
        std::string id;
    };

    ///////////////////////////////////////////////////////////////////////////
    struct GrayScale
    {
        uint8_t value;

        static constexpr size_t size();

        bool operator==(const GrayScale& greyScale) const;
        GrayScale operator+(const GrayScale& greyScale) const;
        GrayScale operator-(const GrayScale& greyScale) const;
        GrayScale operator*(float intensity) const;
    };

    ///////////////////////////////////////////////////////////////////////////
    struct RGB
    {
        uint32_t red : 8;
        uint32_t green : 8;
        uint32_t blue : 8;

        static constexpr size_t size();

        bool operator==(const RGB& rgb) const;
        RGB operator+(const RGB &rgb) const;
        RGB operator-(const RGB &rgb) const;
        RGB operator*(float intensity) const;
    };

    ///////////////////////////////////////////////////////////////////////////
    struct RGBA
    {
        uint32_t red : 8;
        uint32_t green : 8;
        uint32_t blue : 8;
        uint32_t alpha : 8;

        static constexpr size_t size();

        bool operator==(const RGBA& rgba) const;
        RGBA operator+(const RGBA &rgba) const;
        RGBA operator-(const RGBA &rgba) const;
        RGBA operator*(float intensity) const;
    };
}

///////////////////////////////////////////////////////////////////////////
constexpr size_t utils::definitions::GrayScale::size()
{
    return 1;
}

///////////////////////////////////////////////////////////////////////////
constexpr size_t utils::definitions::RGB::size()
{
    return 3;
}

///////////////////////////////////////////////////////////////////////////
constexpr size_t utils::definitions::RGBA::size()
{
    return 4;
}