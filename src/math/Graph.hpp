#pragma once

// cereal
#include <cereal/types/list.hpp>
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#endif
#include <cereal/types/memory.hpp>
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

// std
#include <experimental/list>
#include <list>
#include <memory>
#include <utility>

///////////////////////////////////////////////////////////////////////////
namespace math
{
    ///////////////////////////////////////////////////////////////////////////
    template<class NodeType, class EdgeType>
    class Graph
    {
    public:
        struct Edge;

        ///////////////////////////////////////////////////////////////////////////
        class Node
        {
        public:
            Node() = default;
            Node(const NodeType& value, size_t id);
            Node(NodeType&& value, size_t id);
            Node(Node&&);
            Node& operator=(Node&&);
            Node(const Node&);
            Node& operator=(const Node&);
            virtual ~Node();

            const NodeType* operator->() const;
            NodeType* operator->();
            const NodeType& get() const;
            NodeType& get();

            size_t id() const;

            const std::list<Edge>& getConnections() const;
            const std::list<Edge>& getConnections();

            template<typename Archive>
            void serialize(Archive& archive);

        protected:
            friend class Graph;

            void connect(std::shared_ptr<EdgeType> edgeValue, std::weak_ptr<Node> node);
            void disconnect(const Node& neighbor);

        private:
            size_t identifier;
            NodeType value;
            std::list<Edge> connections;
        };

        ///////////////////////////////////////////////////////////////////////////
        struct Edge
        {
            std::shared_ptr<EdgeType> edge;
            std::weak_ptr<Node> node;

            template<typename Archive>
            void serialize(Archive& archive);
        };

    public:
        using NodeContainer = std::list<std::shared_ptr<Node>>;
        Graph();
        Graph(Graph&&);
        Graph& operator=(Graph&&);
        Graph(const Graph&);
        Graph& operator=(const Graph&);
        virtual ~Graph();

        size_t nodeSize() const;
        size_t edgeSize() const;

        Node& addNode(NodeType&& value);
        Node& addNode(const NodeType& value);
        void addEdge(EdgeType&& value, Node& nodeOne, Node& nodeTwo);
        void addEdge(const EdgeType& value, Node& nodeOne, Node& nodeTwo);

        void removeNode(Node& node);
        void removeNode(size_t id);
        void removeEdge(Node& nodeOne, Node& nodeTwo);
        void clear();


        const Node& get(size_t id) const;
        Node& get(size_t id);
        const NodeContainer& getNodes() const;
        NodeContainer& getNodes();
        size_t nextId() const;
        const Node& front() const;
        Node& front();

        template<typename Archive>
        void serialize(Archive& archive);

    protected:
        const std::shared_ptr<Node> getPtr(size_t id) const;
        std::shared_ptr<Node> getPtr(size_t id);

    private:
        NodeContainer nodes;
        size_t _nextId;
        size_t edgeCount;
    };
}

///////////////////////////////////////////////////////////////////////////
/// @brief Creates a new node from a value which this node will hold and an unique identifier.
/// @tparam NodeType Type of the value of the node.
/// @tparam EdgeType Type of the value of the edges.
/// @param value Value of the node.
/// @param id Unique identifier for the node to distinguish different nodes from one another.
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node::Node(const NodeType& value, size_t id)
: identifier{id}
, value{value}
, connections{}
{
}

///////////////////////////////////////////////////////////////////////////
/// @brief Creates a new node from a value which this node will hold and an unique identifier.
/// @tparam NodeType Type of the value of the node.
/// @tparam EdgeType Type of the value of the edges.
/// @param value Value of the node.
/// @param id Unique identifier for the node to distinguish different nodes from one another.
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node::Node(NodeType&& value, size_t id)
: identifier{id}
, value{std::move(value)}
, connections{}
{
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node::Node(Node&& node)
: identifier{node.identifier}
, value{std::move(node.value)}
, connections{std::move(node.connections)}
{
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node& math::Graph<NodeType, EdgeType>::Node::operator=(Node&& node)
{
    if (&node != this)
    {
        connections = std::move(node.connections);
        value = std::move(node.value);
        identifier = node.identifier;
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node::Node(const Node& node)
: identifier{node.identifier}
, value{node.value}
, connections{node.connections}
{
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node& math::Graph<NodeType, EdgeType>::Node::operator=(const Node& node)
{
    if (&node != this)
    {
        connections = node.connections;
        value = node.value;
        identifier = node.identifier;
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node::~Node()
{
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns pointer to node value.
/// @return Node value.
template<class NodeType, class EdgeType>
const NodeType* math::Graph<NodeType, EdgeType>::Node::operator->() const
{
    return &value;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns pointer to node value.
/// @return Node value.
template<class NodeType, class EdgeType>
NodeType* math::Graph<NodeType, EdgeType>::Node::operator->()
{
    return &value;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns reference to node value.
/// @return Node value.
template<class NodeType, class EdgeType>
const NodeType& math::Graph<NodeType, EdgeType>::Node::get() const
{
    return value;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns reference to node value.
/// @return Node value.
template<class NodeType, class EdgeType>
NodeType& math::Graph<NodeType, EdgeType>::Node::get()
{
    return value;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns id of the node.
/// @return Unique id.
template<class NodeType, class EdgeType>
size_t math::Graph<NodeType, EdgeType>::Node::id() const
{
    return identifier;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Gives access to all the connected nodes and value of the edges of this node.
/// @return Edge values and nodes on which the connection terminates.
template<class NodeType, class EdgeType>
const std::list<typename math::Graph<NodeType, EdgeType>::Edge>& math::Graph<NodeType, EdgeType>::Node::getConnections() const
{
    return connections;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Gives access to all the connected nodes and value of the edges of this node.
/// @return Edge values and nodes on which the connection terminates.
template<class NodeType, class EdgeType>
const std::list<typename math::Graph<NodeType, EdgeType>::Edge>& math::Graph<NodeType, EdgeType>::Node::getConnections()
{
    return connections;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Creates a new connection between two nodes.
/// @param edgeValue The value given to the edge connecting the two nodes.
/// @param node Node on which the edge will terminate.
template<class NodeType, class EdgeType>
void math::Graph<NodeType, EdgeType>::Node::connect(std::shared_ptr<EdgeType> edgeValue, std::weak_ptr<Node> node)
{
    connections.emplace_back(edgeValue, node);
}

///////////////////////////////////////////////////////////////////////////

/// @brief Removes an edge between this node and the one specified in the parameters.
/// @param neighbor Selects which edge should be deleted.
/// @return Returns a pointer to the edge value.
template<class NodeType, class EdgeType>
void math::Graph<NodeType, EdgeType>::Node::disconnect(const Node& neighbor)
{
    const size_t neighborId = neighbor.id();
    std::erase_if(connections, [neighborId](const auto& element) { return neighborId == element.node.lock()->id(); });
}

///////////////////////////////////////////////////////////////////////////
template<typename NodeType, typename EdgeType>
template<typename Archive>
void math::Graph<NodeType, EdgeType>::Node::serialize(Archive& archive)
{
    archive(identifier, value, connections);
}

///////////////////////////////////////////////////////////////////////////
template<typename NodeType, typename EdgeType>
template<typename Archive>
void math::Graph<NodeType, EdgeType>::Edge::serialize(Archive& archive)
{
    archive(node, edge);
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Graph()
: nodes{}
, _nextId{0}
, edgeCount{0}
{
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Graph(Graph&& graph)
: nodes{std::move(graph.nodes)}
, _nextId{graph._nextId}
, edgeCount{graph.edgeCount}
{
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>& math::Graph<NodeType, EdgeType>::operator=(Graph&& graph)
{
    if (&graph != this)
    {
        nodes = std::move(graph.nodes);
        _nextId = graph._nextId;
        edgeCount = graph.edgeCount;
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Graph(const Graph& graph)
: nodes{graph.nodes}
, _nextId{graph._nextId}
, edgeCount{graph.edgeCount}
{
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>& math::Graph<NodeType, EdgeType>::operator=(const Graph& graph)
{
    if (&graph != this)
    {
        nodes = graph.nodes;
        _nextId = graph._nextId;
        edgeCount = graph.edgeCount;
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::~Graph()
{
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns the amount of nodes currently in the graph.
/// @return Count of nodes in the graph.
template<class NodeType, class EdgeType>
size_t math::Graph<NodeType, EdgeType>::nodeSize() const
{
    return nodes.size();
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns the amount of edges currently in the graph.
/// @return Count of edges in the graph.
template<class NodeType, class EdgeType>
size_t math::Graph<NodeType, EdgeType>::edgeSize() const
{
    return edgeCount;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Adds a new node to the graph.
/// @param value The value given to the node.
/// @return A reference to the new node.
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node& math::Graph<NodeType, EdgeType>::addNode(NodeType&& value)
{
    auto& node = nodes.emplace_back(std::make_shared<Node>(std::move(value), _nextId));
    _nextId++;
    return *node;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Adds a new node to the graph.
/// @param value The value given to the node.
/// @return A reference to the new node.
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node& math::Graph<NodeType, EdgeType>::addNode(const NodeType& value)
{
    auto& node = nodes.emplace_back(std::make_shared<Node>(value, _nextId));
    _nextId++;
    return *node;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Creates a unidirectional edge between two nodes. If the edge already exists only the value will be updated on the existing edge.
/// @param value The value given to the edge
/// @param nodeOne One node that gets connected to the edge.
/// @param nodeTwo One node that gets connected to the edge.
template<class NodeType, class EdgeType>
void math::Graph<NodeType, EdgeType>::addEdge(EdgeType&& value, Node& nodeOne, Node& nodeTwo)
{
    const auto iteratorOne = std::find_if(nodeOne.connections.begin(), nodeOne.connections.end(), [&nodeTwo](const auto& currentNode) { return nodeTwo.id() == currentNode.node.lock()->id(); });
    const auto iteratorTwo = std::find_if(nodeTwo.connections.begin(), nodeTwo.connections.end(), [&nodeOne](const auto& currentNode) { return nodeOne.id() == currentNode.node.lock()->id(); });

    if (iteratorOne == nodeOne.connections.end() && iteratorTwo == nodeTwo.connections.end())
    {
        edgeCount++;
        auto pValue = std::make_shared<EdgeType>(std::move(value));
        nodeOne.connect(pValue, getPtr(nodeTwo.id()));
        if (nodeOne.id() != nodeTwo.id())
            nodeTwo.connect(pValue, getPtr(nodeOne.id()));
    }
    else if (iteratorOne != nodeOne.connections.end() && iteratorTwo == nodeTwo.connections.end())
    {
        *iteratorOne->edge = std::move(value);
        nodeTwo.connect(iteratorOne->edge, getPtr(nodeOne.id()));
    }
    else if (iteratorOne == nodeOne.connections.end() && iteratorTwo != nodeTwo.connections.end())
    {
        *iteratorTwo->edge = std::move(value);
        nodeOne.connect(iteratorTwo->edge, getPtr(nodeTwo.id()));
    }
    else
    {
        *iteratorOne->edge = std::move(value);
    }
}

///////////////////////////////////////////////////////////////////////////
/// @brief Creates a unidirectional edge between two nodes. If the edge already exists only the value will be updated on the existing edge.
/// @param value The value given to the edge
/// @param nodeOne One node that gets connected to the edge.
/// @param nodeTwo One node that gets connected to the edge.
template<class NodeType, class EdgeType>
void math::Graph<NodeType, EdgeType>::addEdge(const EdgeType& value, Node& nodeOne, Node& nodeTwo)
{
    const auto iteratorOne = std::find_if(nodeOne.connections.begin(), nodeOne.connections.end(), [&nodeTwo](const auto& currentNode) { return nodeTwo.id() == currentNode.node.lock()->id(); });
    const auto iteratorTwo = std::find_if(nodeTwo.connections.begin(), nodeTwo.connections.end(), [&nodeOne](const auto& currentNode) { return nodeOne.id() == currentNode.node.lock()->id(); });

    if (iteratorOne == nodeOne.connections.end() && iteratorTwo == nodeTwo.connections.end())
    {
        edgeCount++;
        auto pValue = std::make_shared<EdgeType>(value);
        nodeOne.connect(pValue, getPtr(nodeTwo.id()));
        if (nodeOne.id() != nodeTwo.id())
            nodeTwo.connect(pValue, getPtr(nodeOne.id()));
    }
    else if (iteratorOne != nodeOne.connections.end() && iteratorTwo == nodeTwo.connections.end())
    {
        *iteratorOne->edge = value;
        nodeTwo.connect(iteratorOne->edge, getPtr(nodeOne.id()));
    }
    else if (iteratorOne == nodeOne.connections.end() && iteratorTwo != nodeTwo.connections.end())
    {
        *iteratorTwo->edge = value;
        nodeOne.connect(iteratorTwo->edge, getPtr(nodeTwo.id()));
    }
    else
    {
        *iteratorOne->edge = value;
    }
}

///////////////////////////////////////////////////////////////////////////
/// @brief Removes a node from the graph.
/// @param node The node that should be deleted.
template<class NodeType, class EdgeType>
void math::Graph<NodeType, EdgeType>::removeNode(Node& node)
{
    std::list<Edge> connections = node.getConnections();
    for (const auto& connection : connections)
    {
        if(auto pNode = connection.node.lock())
        {
            node.disconnect(*pNode);
            pNode->disconnect(node);
            edgeCount--;
        }
    }
    std::erase_if(nodes, [&node](const auto& currentNode) { return node.id() == currentNode->id(); });
}

///////////////////////////////////////////////////////////////////////////
/// @brief Removes a node from the graph.
/// @param id The id of the node that should be deleted.
template<class NodeType, class EdgeType>
void math::Graph<NodeType, EdgeType>::removeNode(size_t id)
{
    removeNode(get(id));
}

///////////////////////////////////////////////////////////////////////////
/// @brief This function will delete all edges between two nodes. If no edge exist nothing will happen.
/// @param nodeOne One node connected by an edge.
/// @param nodeTwo One node connected by an edge.
template<class NodeType, class EdgeType>
void math::Graph<NodeType, EdgeType>::removeEdge(Node& nodeOne, Node& nodeTwo)
{
    nodeOne.disconnect(nodeTwo);
    nodeTwo.disconnect(nodeOne);
    edgeCount--;
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
void math::Graph<NodeType, EdgeType>::clear()
{
    nodes.clear();
    edgeCount = 0;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns a list of all nodes in the graph.
/// @return List of nodes.
template<class NodeType, class EdgeType>
const typename math::Graph<NodeType, EdgeType>::NodeContainer& math::Graph<NodeType, EdgeType>::getNodes() const
{
    return nodes;
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns a list of all nodes in the graph.
/// @return List of nodes.
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::NodeContainer& math::Graph<NodeType, EdgeType>::getNodes()
{
    return nodes;
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
const typename math::Graph<NodeType, EdgeType>::Node& math::Graph<NodeType, EdgeType>::get(size_t id) const
{
    return *getPtr(id);
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node& math::Graph<NodeType, EdgeType>::get(size_t id)
{
    return *getPtr(id);
}

///////////////////////////////////////////////////////////////////////////
/// @brief Return the id that will be given to the next node.
/// @return Id given to next node.
template<class NodeType, class EdgeType>
size_t math::Graph<NodeType, EdgeType>::nextId() const
{
    return _nextId;
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
const typename math::Graph<NodeType, EdgeType>::Node& math::Graph<NodeType, EdgeType>::front() const
{
    return nodes.front();
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
math::Graph<NodeType, EdgeType>::Node& math::Graph<NodeType, EdgeType>::front()
{
    return nodes.front();
}

///////////////////////////////////////////////////////////////////////////
template<typename NodeType, typename EdgeType>
template<typename Archive>
void math::Graph<NodeType, EdgeType>::serialize(Archive& archive)
{
    archive(nodes, _nextId, edgeCount);
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
const std::shared_ptr<typename math::Graph<NodeType, EdgeType>::Node> math::Graph<NodeType, EdgeType>::getPtr(size_t id) const
{
    return *std::find_if(nodes.begin(), nodes.end(), [id](const auto& node) { return id == node.id(); });
}

///////////////////////////////////////////////////////////////////////////
template<class NodeType, class EdgeType>
std::shared_ptr<typename math::Graph<NodeType, EdgeType>::Node> math::Graph<NodeType, EdgeType>::getPtr(size_t id)
{
    return *std::find_if(nodes.begin(), nodes.end(), [id](const auto& node) { return id == node->id(); });
}