// catch2
#include <catch2/catch_test_macros.hpp>

// std
#include <chrono>
#include <filesystem>
#include <string>

// utils
#include <utils/File.hpp>

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Reading a whole file.", "[utils::file::readAll]" )
{
    const uint32_t seed = static_cast<uint32_t>(std::chrono::system_clock::now().time_since_epoch().count());
    std::srand(seed);
    std::filesystem::path filePath = std::filesystem::current_path() / "temp";
    std::filesystem::create_directories(filePath);

    SECTION("Reading empty file.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".txt";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output.close();

        std::ifstream input;
        input.open(filePath);

        const auto result = utils::file::readAll(input);
        REQUIRE(result.empty());

        input.close();
    }

    SECTION("Reading file containing a single line.")
    {
        const std::string fileContents = "Single Line " + std::to_string(std::rand());
        std::string fileName = "test" + std::to_string(std::rand()) + ".txt";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output << fileContents;
        output.close();

        std::ifstream input;
        input.open(filePath);

        const auto result = utils::file::readAll(input);
        REQUIRE(!result.empty());
        REQUIRE(result == fileContents);

        input.close();
    }

    SECTION("Reading a large file.")
    {
        constexpr size_t fileLines = 1000;
        std::string fileContents;
        for(size_t i = 0; i < fileLines; i++)
            fileContents = std::to_string(std::rand()) + "\n";

        std::string fileName = "test" + std::to_string(std::rand()) + ".txt";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output << fileContents;
        output.close();

        std::ifstream input;
        input.open(filePath);

        const auto result = utils::file::readAll(input, fileLines);
        REQUIRE(!result.empty());
        REQUIRE(result == fileContents);

        input.close();
    }

    std::filesystem::remove_all(filePath);
}