#pragma once

// algorithm
#include <algorithm/slam/Grid.hpp>

// ui
#include <ui/internal/ViewportConfig.hpp>

namespace utils::definitions
{
    struct Timestep;
}

namespace math
{
    template<typename N, typename E>
    class Graph;
}

namespace algorithm::slam
{
    struct EdgeValue;
}

namespace utils
{
    ///////////////////////////////////////////////////////////////////////////
    struct PoseGraphConfig
    {
        ui::ViewportGridColorMode odometry;
        ui::ViewportGridColorMode loopClosure;
        float scale;
    };

    ///////////////////////////////////////////////////////////////////////////
    struct PoseGraphResult
    {
        float minRmse;
        float maxRmse;
        float averageRmse;
    };

    ///////////////////////////////////////////////////////////////////////////
    PoseGraphResult poseGraphView(algorithm::slam::Grid<ui::ViewportGridColorMode>& grid, const math::Graph<utils::definitions::Timestep, algorithm::slam::EdgeValue>& graph, const PoseGraphConfig& config);
}