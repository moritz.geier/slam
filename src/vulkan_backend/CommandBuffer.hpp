#pragma once

// vulkan
#include <vulkan/vulkan.hpp>

class CommandPool;
class LogicalDevice;

///////////////////////////////////////////////////////////////////////////
class CommandBuffer
{
public:
    CommandBuffer(vk::UniqueCommandBuffer&& buffer);
    CommandBuffer(const vk::CommandBuffer& buffer);
    CommandBuffer(CommandBuffer&&);
    CommandBuffer& operator=(CommandBuffer&&);
    CommandBuffer(const CommandBuffer&) = delete;
    CommandBuffer& operator=(const CommandBuffer&) = delete;
    virtual ~CommandBuffer();

    const vk::CommandBuffer& get() const;
    vk::CommandBuffer& get();

    const vk::UniqueCommandBuffer& operator->() const;
    vk::UniqueCommandBuffer& operator->();

    [[nodiscard]]
    static vk::CommandBuffer singleTimeBegin(const LogicalDevice& device, const CommandPool& commandPool);
    static void singleTimeEnd(const LogicalDevice& device, const CommandPool& commandPool, vk::CommandBuffer& commandBuffer);

protected:
    vk::UniqueCommandBuffer commandBuffer;

    CommandBuffer();
};
