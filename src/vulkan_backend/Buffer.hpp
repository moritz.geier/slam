#pragma once

// std
#include <concepts>
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

#include "LogicalDevice.hpp"
#include "PhysicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
template<class Object>
class Buffer
{
public:
    Buffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags property, size_t size);
    Buffer(Buffer<Object>&&);
    Buffer<Object>& operator=(Buffer<Object>&&);
    Buffer(const Buffer<Object>&) = delete;
    Buffer<Object>& operator=(const Buffer<Object>&) = delete;
    virtual ~Buffer();

    const vk::Buffer& get() const;
    vk::Buffer& get();

    const Object* data() const;
    Object* data();

protected:
    const LogicalDevice& logicalDevice;
    vk::UniqueBuffer buffer;
    vk::UniqueDeviceMemory memory;

    const size_t bufferSize;
    Object* mappedMemory;
    bool shouldDelete;
};

///////////////////////////////////////////////////////////////////////////
template<class Object>
class Buffers : public std::vector<Buffer<Object>>
{
public:
    Buffers(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t amount);
};

///////////////////////////////////////////////////////////////////////////
template<class Object>
Buffer<Object>::Buffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags property, size_t size)
: logicalDevice{logicalDevice}
, bufferSize{sizeof(Object) * size}
, shouldDelete{true}
{
    vk::BufferCreateInfo createInfo{};
    createInfo.sType = createInfo.structureType;
    createInfo.usage = usage;
    createInfo.size = bufferSize;
    createInfo.sharingMode = vk::SharingMode::eExclusive;

    buffer = logicalDevice->createBufferUnique(createInfo);

    const auto requirements = logicalDevice->getBufferMemoryRequirements(buffer.get());
    const auto index = physicalDevice.getMemoryTypeIndex(requirements.memoryTypeBits, property);

    vk::MemoryAllocateInfo allocateInfo{};
    allocateInfo.sType = vk::StructureType::eMemoryAllocateInfo;
    allocateInfo.allocationSize = requirements.size;
    allocateInfo.memoryTypeIndex = index;

    memory = logicalDevice->allocateMemoryUnique(allocateInfo);
    logicalDevice->bindBufferMemory(buffer.get(), memory.get(), 0);

    mappedMemory = reinterpret_cast<Object*>(logicalDevice->mapMemory(memory.get(), 0, bufferSize));
    if (mappedMemory == nullptr)
        throw std::runtime_error{"[Staging Buffer] Unable to map buffer to host memory"};
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
Buffer<Object>::Buffer(Buffer<Object>&& moveUniformBuffer)
: logicalDevice{moveUniformBuffer.logicalDevice}
, buffer{std::move(moveUniformBuffer.buffer)}
, memory{std::move(moveUniformBuffer.memory)}
, bufferSize{moveUniformBuffer.bufferSize}
, mappedMemory{moveUniformBuffer.mappedMemory}
, shouldDelete{true}
{
    moveUniformBuffer.shouldDelete = false;
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
Buffer<Object>& Buffer<Object>::operator=(Buffer<Object>&& moveUniformBuffer)
{
    if (&moveUniformBuffer != this)
    {
        logicalDevice = moveUniformBuffer.logicalDevice;
        buffer = std::move(moveUniformBuffer.buffer);
        memory = std::move(moveUniformBuffer.memory);
        bufferSize = moveUniformBuffer.bufferSize;
        mappedMemory = moveUniformBuffer.mappedMemory;
        shouldDelete = true;
        moveUniformBuffer.shouldDelete = false;
    }
    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
Buffer<Object>::~Buffer()
{
    if (shouldDelete)
        logicalDevice->unmapMemory(memory.get());
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
const vk::Buffer& Buffer<Object>::get() const
{
    return buffer.get();
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
vk::Buffer& Buffer<Object>::get()
{
    return buffer.get();
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
const Object* Buffer<Object>::data() const
{
    return mappedMemory;
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
Object* Buffer<Object>::data()
{
    return mappedMemory;
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
Buffers<Object>::Buffers(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t amount)
: std::vector<Buffer<Object>>{}
{
    for (size_t i = 0; i < amount; i++)
        std::vector<Buffer<Object>>::emplace_back(logicalDevice, physicalDevice);
}