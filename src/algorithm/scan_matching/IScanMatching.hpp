#pragma once

// eigen
#include <Eigen/Geometry>

// std
#include <vector>

// utils
#include <utils/Definitions.hpp>

///////////////////////////////////////////////////////////////////////////
namespace algorithm
{
    ///////////////////////////////////////////////////////////////////////////
    template<utils::concepts::Timestep StepType>
    class IScanMatching
    {
    protected:
        static constexpr int DIMENSIONS = decltype(StepType::measurements)::value_type::RowsAtCompileTime;
    public:
        using TransformationType = decltype(StepType::pose);
        using PointcloudType = decltype(StepType::measurements);

        struct AlignmentResult
        {
            TransformationType transformMatrix;
            float error;
        };

        struct LoopClosureInput
        {
            StepType step;
            size_t id;
        };

        struct LoopClosureResult
        {
            TransformationType pose;
            size_t matchedId;
            TransformationType transformMatrix;
            float error;
        };

        virtual AlignmentResult findAlignment(PointcloudType pointsToMatch, const PointcloudType& referencePoints, const TransformationType& initialAlignment) = 0;
        virtual LoopClosureResult findLoopClosure(const StepType& currentStep, const std::vector<LoopClosureInput>& referenceSteps);
    };
}

///////////////////////////////////////////////////////////////////////////
template<utils::concepts::Timestep StepType>
algorithm::IScanMatching<StepType>::LoopClosureResult algorithm::IScanMatching<StepType>::findLoopClosure(const StepType& currentStep, const std::vector<LoopClosureInput>& referenceSteps)
{
    LoopClosureResult result;
    result.pose.setIdentity();
    result.matchedId = 0;
    result.transformMatrix.setIdentity();
    result.error = std::numeric_limits<float>().infinity();

    for (size_t i = 0; i < referenceSteps.size(); i++)
    {
        const TransformationType initialAlignment = currentStep.pose.inverse() * referenceSteps[i].step.pose;
        const auto currentResult = findAlignment(referenceSteps[i].step.measurements, currentStep.measurements, initialAlignment);
        if (currentResult.error < result.error)
            result = {referenceSteps[i].step.pose, referenceSteps[i].id, currentResult.transformMatrix, currentResult.error};
    }

    return result;
}