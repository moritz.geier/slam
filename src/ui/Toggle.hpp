#pragma once

// std
#include <string>

namespace ui
{
    void Toggle(const std::string& name, bool& value);
}
