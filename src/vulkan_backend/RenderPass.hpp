#pragma once

// vulkan
#include <vulkan/vulkan.hpp>

class LogicalDevice;

class RenderPass final
{
public:
    RenderPass(const LogicalDevice& device, const vk::Format& format, const vk::ImageLayout finalLayout);
    RenderPass(RenderPass&&);
    RenderPass& operator=(RenderPass&&);
    RenderPass(const RenderPass&) = delete;
    RenderPass& operator=(const RenderPass&) = delete;
    ~RenderPass();

    const vk::RenderPass& get() const;
    vk::RenderPass& get();

private:
    vk::UniqueRenderPass renderPass;
};