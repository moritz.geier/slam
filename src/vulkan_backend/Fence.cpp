#include "Fence.hpp"
#include "LogicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
Fence::Fence(const LogicalDevice& logicalDevice, bool isSignaled)
: device{logicalDevice.get()}
{
    vk::FenceCreateInfo createInfo{};
    createInfo.sType = vk::StructureType::eFenceCreateInfo;
    if (isSignaled)
        createInfo.flags = vk::FenceCreateFlagBits::eSignaled;

    fence = device.createFenceUnique(createInfo);
}

///////////////////////////////////////////////////////////////////////////
Fence::Fence(Fence&& moveFence)
{
    fence = std::move(moveFence.fence);
    device = std::move(moveFence.device);
}

///////////////////////////////////////////////////////////////////////////
Fence& Fence::operator=(Fence&& moveFence)
{
    if (&moveFence != this)
    {
        fence = std::move(moveFence.fence);
        device = std::move(moveFence.device);
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Fence::~Fence()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::Fence& Fence::get() const
{
    return fence.get();
}

///////////////////////////////////////////////////////////////////////////
vk::Fence& Fence::get()
{
    return fence.get();
}

///////////////////////////////////////////////////////////////////////////
void Fence::wait() const
{
    (void)device.waitForFences(fence.get(), VK_TRUE, UINT64_MAX);
}

///////////////////////////////////////////////////////////////////////////
void Fence::reset() const
{
    (void)device.resetFences(fence.get());
}
