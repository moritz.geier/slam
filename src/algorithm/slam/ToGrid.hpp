#pragma once

// algorithm
#include <algorithm/line/AntiAliasedLine.hpp>
#include <algorithm/line/Util.hpp>

// math
#include <math/Graph.hpp>

// std
#include <cmath>
#include <concepts>
#include <vector>

// utils
#include <utils/Definitions.hpp>
#include <utils/LogReader.hpp>

#include "Grid.hpp"

namespace algorithm::slam
{
    ///////////////////////////////////////////////////////////////////////////
    template<class GridType>
    struct ToGridConfiguration
    {
        float scale;
        size_t size;
        GridType occupiedSpace;
        GridType freeSpace;
        GridType unknownSpace;
    };

    ///////////////////////////////////////////////////////////////////////////
    namespace
    {
        ///////////////////////////////////////////////////////////////////////////
        template<class GridType, slam::GridLayout Layout>
        void generateOccupancyGrid(slam::Grid<GridType, Layout>& grid, const std::vector<utils::definitions::Point2D<float>>& measurement, const Eigen::AffineCompact2f& robotPose, const utils::definitions::Point2D<float>& origin, const ToGridConfiguration<GridType>& configuration)
        {
            const utils::definitions::Point2D<float> center = origin + robotPose.translation() * configuration.scale;

            for (const auto& point : measurement)
            {
                const auto absolutMeasurement = robotPose * point;
                const auto scaledMeasurement = origin + absolutMeasurement * configuration.scale;
                const auto result = algorithm::line::antiAliasedLine(grid, center, scaledMeasurement, configuration.freeSpace);

                const auto wallPosition = result.pointTwo;
                if (result.inBound && (wallPosition - scaledMeasurement).squaredNorm() < 0.5f)
                {
                    if (algorithm::line::isLineThroughPointsShallow<float>(center, scaledMeasurement))
                    {
                        grid.insert(wallPosition.x(), wallPosition.y(), configuration.occupiedSpace);
                        grid.insert(wallPosition.x(), wallPosition.y() + 1, configuration.occupiedSpace);
                    }
                    else
                    {
                        grid.insert(wallPosition.x(), wallPosition.y(), configuration.occupiedSpace);
                        grid.insert(wallPosition.x() + 1, wallPosition.y(), configuration.occupiedSpace);
                    }
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Converts a log file datapoint into a occupancy grid.
    /// @tparam GridType Defines the datatype of the grid.
    /// @tparam Layout Defines whether the grid should be row or column major.
    /// @param step Raw measurement data of an laser range finder.
    /// @param gridSize How many pixels the grid should be high and wide.
    /// @param scale Defines the scale of a single pixel, e.g. a scale of 1 would mean 1 pixel = 1 meter, a scale of 10 would mean 1 pixel = 10 m and a scale of 0.1 would mean 1 pixel = 10 cm. Thus this parameter has to be grater than 0.
    /// @return Occupancy grid created from the measurements.
    template<class GridType, utils::concepts::Timestep Step, slam::GridLayout Layout = slam::GridLayout::eRowMajor>
    slam::Grid<GridType, Layout> toGrid(const Step& step, const ToGridConfiguration<GridType>& configuration)
    {
        const float center = static_cast<float>(configuration.size) / 2;
        slam::Grid<GridType, Layout> grid{configuration.size, configuration.size};

        grid.fill(configuration.unknownSpace);

        Eigen::AffineCompact2f pose;
        pose.setIdentity();
        pose.linear() = step.pose.linear();
        generateOccupancyGrid(grid, step.measurements, pose, {center, center}, configuration);

        return grid;
    }

    ///////////////////////////////////////////////////////////////////////////
    template<class GridType, utils::concepts::Timestep Step, class EdgeType, slam::GridLayout Layout = slam::GridLayout::eRowMajor>
    slam::Grid<GridType, Layout> toGrid(const math::Graph<Step, EdgeType>& graph, const ToGridConfiguration<GridType>& configuration, float& progress)
    {
        slam::Grid<GridType, Layout> grid{configuration.size, configuration.size};
        grid.fill(configuration.unknownSpace);
        const utils::definitions::Point2D<float> origin{grid.width() / 2, grid.height() / 2};
        const float progressIncrease = 1.0f / graph.nodeSize();

        const auto& nodes = graph.getNodes();
        for (const auto& node : nodes)
        {
            generateOccupancyGrid(grid, node->get().measurements, node->get().pose, origin, configuration);
            progress += progressIncrease;
        }

        return grid;
    }

    ///////////////////////////////////////////////////////////////////////////
    template<class GridType, utils::concepts::Timestep Step, class EdgeType, slam::GridLayout Layout = slam::GridLayout::eRowMajor>
    slam::Grid<GridType, Layout> toGrid(const math::Graph<Step, EdgeType>& graph, const ToGridConfiguration<GridType>& configuration)
    {
        float progress = 0.0f;
        return toGrid(graph, configuration, progress);
    }
}