#pragma once

// std
#include <string>
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

class CommandBuffer;
class LogicalDevice;
class PhysicalDevice;

///////////////////////////////////////////////////////////////////////////
class Image
{
public:
    Image(const LogicalDevice& device, const PhysicalDevice& physicalDevice, const vk::Format& format, const vk::Extent2D& extent, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties);
    Image(Image&&);
    Image& operator=(Image&&);
    Image(const Image&) = delete;
    Image& operator=(const Image&) = delete;
    Image(const vk::Image&);
    Image& operator=(const vk::Image&);
    virtual ~Image();

    const vk::Image& get() const;
    vk::Image& get();

    const vk::DeviceMemory& getMemory() const;
    vk::DeviceMemory& getMemory();

    void release();

    void changeLayout(const vk::CommandBuffer& buffer, const vk::ImageLayout& oldLayout, const vk::ImageLayout& newLayout) const;

protected:
    vk::UniqueImage image;
    vk::UniqueDeviceMemory imageMemory;
};

///////////////////////////////////////////////////////////////////////////
class Images final : public std::vector<Image>
{
public:
    Images(const LogicalDevice& device, const PhysicalDevice& physicalDevice, const vk::Format& format, const vk::Extent2D& extent, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties, size_t count);
    Images(std::vector<Image>&&);
    Images();
};