#include "ViewportConfig.hpp"

// algorithm
#include <algorithm/slam/ToGrid.hpp>

///////////////////////////////////////////////////////////////////////////
namespace ui
{
    ///////////////////////////////////////////////////////////////////////////
    ViewportGridColorMode getPathColor()
    {
        return {255, 255, 0, 255};
    }

    ///////////////////////////////////////////////////////////////////////////
    ViewportGridColorMode getLoopClosureColor()
    {
        return {255, 0, 0, 255};
    }

    ///////////////////////////////////////////////////////////////////////////
    algorithm::slam::ToGridConfiguration<ViewportGridColorMode> getGridConfiguration(float scale)
    {
        algorithm::slam::ToGridConfiguration<ViewportGridColorMode> config;
        config.freeSpace = {255, 255, 255, 255};
        config.unknownSpace = {127, 127, 127, 255};
        config.occupiedSpace = {0, 0, 0, 255};
        config.size = 1024;
        config.scale = scale;

        return config;
    }
}