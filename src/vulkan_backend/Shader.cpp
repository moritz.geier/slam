#include "Shader.hpp"
#include "LogicalDevice.hpp"

// std
#include <fstream>
#include <iostream>
#include <iterator>

///////////////////////////////////////////////////////////////////////////
Shader::Shader(const LogicalDevice& logicalDevice, const std::filesystem::path& shader)
{
    if (shader.extension() != ".spv")
        throw std::runtime_error{"[Shader] Bytecodes should have a .spv extension, file: " + shader.string()};

    if (!std::filesystem::exists(shader))
        throw std::runtime_error{"[Shader] File doesn't exist: " + shader.string() + " current working directory is " + std::filesystem::current_path().string()};

    std::ifstream bytecodeFile{shader, std::ios::ate | std::ios::binary};
    if (!bytecodeFile.is_open())
        throw std::runtime_error{"[Shader] Could not open file: " + shader.string()};

    size_t codeSize = bytecodeFile.tellg();
    bytecodeFile.seekg(0);

    std::vector<char> bytecode;
    bytecode.resize(codeSize);

    bytecodeFile.read(bytecode.data(), codeSize);
    bytecodeFile.close();

    vk::ShaderModuleCreateInfo createInfo{};
    createInfo.sType = vk::StructureType::eShaderModuleCreateInfo;
    createInfo.codeSize = bytecode.size();
    createInfo.pCode = reinterpret_cast<const uint32_t*>(bytecode.data());


    shaderModule = logicalDevice->createShaderModuleUnique(createInfo);

    auto filename = shader.stem();
    auto shaderExtension = filename.extension().string();

    if(shaderExtension == ".vert")
        type = vk::ShaderStageFlagBits::eVertex;
    else if(shaderExtension == ".frag")
        type = vk::ShaderStageFlagBits::eFragment;
    else
        type = vk::ShaderStageFlagBits::eAll;
}

///////////////////////////////////////////////////////////////////////////
Shader::Shader(Shader&& shader)
{
    shaderModule = std::move(shader.shaderModule);
    type = shader.type;
}

///////////////////////////////////////////////////////////////////////////
Shader& Shader::operator=(Shader&& shader)
{
    if (this != &shader)
    {
        shaderModule = std::move(shader.shaderModule);
        type = shader.type;
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Shader::~Shader()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::ShaderModule& Shader::get() const
{
    return shaderModule.get();
}

///////////////////////////////////////////////////////////////////////////
vk::ShaderModule& Shader::get()
{
    return shaderModule.get();
}

///////////////////////////////////////////////////////////////////////////
vk::ShaderStageFlagBits Shader::getType() const
{
    return type;
}