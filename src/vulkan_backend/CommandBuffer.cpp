#include "CommandBuffer.hpp"
#include "CommandPool.hpp"
#include "LogicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
CommandBuffer::CommandBuffer(vk::UniqueCommandBuffer&& buffer)
{
    commandBuffer = std::move(buffer);
}

///////////////////////////////////////////////////////////////////////////
CommandBuffer::CommandBuffer(const vk::CommandBuffer& buffer)
{
    commandBuffer = vk::UniqueCommandBuffer{buffer};
}

///////////////////////////////////////////////////////////////////////////
CommandBuffer::CommandBuffer(CommandBuffer&& moveCommandBuffer)
{
    commandBuffer = std::move(moveCommandBuffer.commandBuffer);
}

///////////////////////////////////////////////////////////////////////////
CommandBuffer& CommandBuffer::operator=(CommandBuffer&& moveCommandBuffer)
{
    if (&moveCommandBuffer != this)
        commandBuffer = std::move(moveCommandBuffer.commandBuffer);
    return *this;
}

///////////////////////////////////////////////////////////////////////////
CommandBuffer::CommandBuffer()
{
}

///////////////////////////////////////////////////////////////////////////
CommandBuffer::~CommandBuffer()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::CommandBuffer& CommandBuffer::get() const
{
    return commandBuffer.get();
}

///////////////////////////////////////////////////////////////////////////
vk::CommandBuffer& CommandBuffer::get()
{
    return commandBuffer.get();
}

///////////////////////////////////////////////////////////////////////////
const vk::UniqueCommandBuffer& CommandBuffer::operator->() const
{
    return commandBuffer;
}

///////////////////////////////////////////////////////////////////////////
vk::UniqueCommandBuffer& CommandBuffer::operator->()
{
    return commandBuffer;
}

///////////////////////////////////////////////////////////////////////////
vk::CommandBuffer CommandBuffer::singleTimeBegin(const LogicalDevice& device, const CommandPool& commandPool)
{
    vk::CommandBufferAllocateInfo allocateInfo{};
    allocateInfo.sType = allocateInfo.structureType;
    allocateInfo.level = vk::CommandBufferLevel::ePrimary;
    allocateInfo.commandPool = commandPool.get();
    allocateInfo.commandBufferCount = 1;

    vk::CommandBuffer commandBuffer = device->allocateCommandBuffers(allocateInfo)[0];

    vk::CommandBufferBeginInfo beginInfo{};
    beginInfo.sType = beginInfo.structureType;
    beginInfo.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;

    commandBuffer.begin(beginInfo);

    return commandBuffer;
}

///////////////////////////////////////////////////////////////////////////
void CommandBuffer::singleTimeEnd(const LogicalDevice& device, const CommandPool& commandPool, vk::CommandBuffer& commandBuffer)
{
    commandBuffer.end();

    vk::SubmitInfo submitInfo{};
    submitInfo.sType = submitInfo.structureType;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    const auto& queue = device.getGraphicsQueue();
    auto result = queue.submit(1, &submitInfo, VK_NULL_HANDLE);
    if (result != vk::Result::eSuccess)
        throw std::runtime_error{"[CommandBuffer] Could not submit single time buffer"};

    queue.waitIdle();
    device->freeCommandBuffers(commandPool.get(), 1, &commandBuffer);
}