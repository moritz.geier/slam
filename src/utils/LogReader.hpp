#pragma once

// std
#include <chrono>
#include <filesystem>
#include <fstream>
#include <optional>
#include <vector>

#include "Definitions.hpp"

namespace utils
{
    class LogReader final
    {
    public:
        LogReader(const std::filesystem::path& log);
        ~LogReader();

        /// @brief Returns one timestep of the recorded data.
        /// @return `std::nullopt` if inputed data was in the wrong format, otherwise a `definitions::Timestep` instance containing the measurements.
        std::optional<definitions::Timestep> nextStep();

        /// @brief Reads out the whole file and returns all valid measurements.
        /// @param log Path to the logfile to be read.
        /// @param progress Indicaties how much of the file is already been processed in % [0,1]. Can be used for e.g. loading bars.
        /// @return A vector containing all valid measurements from the specified log file.
        static std::vector<definitions::Timestep> allSteps(const std::filesystem::path& log, float& progress);

        /// @brief Reads out the whole file and returns all valid measurements.
        /// @param log Path to the logfile to be read.
        /// @return A vector containing all valid measurements from the specified log file.
        static std::vector<definitions::Timestep> allSteps(const std::filesystem::path& log);

    private:
        std::ifstream logFile;
        std::chrono::microseconds lastTimestamp;

        static std::ifstream openFile(const std::filesystem::path& log);
        static std::optional<definitions::Timestep> parseLine(const std::string& line, std::chrono::microseconds& lastTimestamp);
    };
}