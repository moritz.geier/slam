#pragma once

// std
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

class Image;
class Images;
class LogicalDevice;

///////////////////////////////////////////////////////////////////////////
class ImageView final
{
public:
    ImageView(const LogicalDevice& device, const vk::Format& format, const Image& image);
    ImageView(ImageView&&);
    ImageView& operator=(ImageView&&);
    ImageView(const ImageView&) = delete;
    ImageView& operator=(const ImageView&) = delete;
    ~ImageView();

    const vk::ImageView& get() const;
    vk::ImageView& get();

private:
    vk::UniqueImageView imageView;
};

///////////////////////////////////////////////////////////////////////////
class ImageViews final : public std::vector<ImageView>
{
public:
    ImageViews(const LogicalDevice& device, const vk::Format& format, const Images& images);
};