#pragma once

// imgui
#include <imgui.h>

// std
#include <string>
#include <type_traits>

// utils
#include <utils/AsyncWithProgress.hpp>

namespace ui
{
    ///////////////////////////////////////////////////////////////////////////
    template<typename ReturnType, typename ...Args>
    void ProgressBar(const std::string& name, utils::AsyncWithProgress<ReturnType, Args...>& asyncFunction, typename std::remove_const<typename std::remove_reference<Args>::type>::type... args);
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
void ui::ProgressBar(const std::string& name, utils::AsyncWithProgress<ReturnType, Args...>& asyncFunction, typename std::remove_const<typename std::remove_reference<Args>::type>::type... args)
{
    if (!ImGui::IsPopupOpen(name.c_str()))
        ImGui::OpenPopup(name.c_str());

    if (ImGui::BeginPopupModal(name.c_str()))
    {
        ImGui::ProgressBar(asyncFunction.getProgress());

        if (!asyncFunction.isRunning())
            asyncFunction.start(std::forward<Args>(args)...);

        ImGui::EndPopup();
    }
}