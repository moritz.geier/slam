#pragma once

// std
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

#include "DescriptorPool.hpp"
#include "DescriptorSetLayout.hpp"
#include "ImageView.hpp"
#include "LogicalDevice.hpp"
#include "Sampler.hpp"
#include "UniformBuffer.hpp"

template<class T>
class DescriptorSets;

///////////////////////////////////////////////////////////////////////////
struct Empty
{
};

///////////////////////////////////////////////////////////////////////////
template<class T = Empty>
class DescriptorSet final
{
public:
    DescriptorSet(vk::DescriptorSet&&, const LogicalDevice& device);
    DescriptorSet(DescriptorSet&&);
    DescriptorSet& operator=(DescriptorSet&&) = delete;
    DescriptorSet(const DescriptorSet&) = delete;
    DescriptorSet& operator=(const DescriptorSet&) = delete;
    ~DescriptorSet();

    const vk::DescriptorSet& get() const;
    vk::DescriptorSet& get();

protected:
    friend DescriptorSets<T>;

    void assignBuffer(const UniformBuffer<T>& buffer, uint32_t bindingNumber);

    void assignSampler(const Sampler& sampler, const vk::ImageView& imageView, uint32_t bindingNumber);

    void updateSets();
    void clearSets();

private:
    vk::UniqueDescriptorSet descriptorSet;
    const LogicalDevice& device;

    vk::DescriptorImageInfo imageInfo;
    vk::DescriptorBufferInfo uniformBuffer;
    std::vector<vk::WriteDescriptorSet> writeSets;
};

///////////////////////////////////////////////////////////////////////////
template<class T = Empty>
class DescriptorSets : public std::vector<DescriptorSet<T>>
{
public:
    DescriptorSets(const LogicalDevice& device, const DescriptorPool& pool, const DescriptorSetLayout& layout, const std::vector<UniformBuffer<T>>& buffers);
    DescriptorSets(const LogicalDevice& device, const DescriptorPool& pool, const DescriptorSetLayout& layout, const Sampler& sampler, const std::vector<vk::ImageView>& imageViews);
    DescriptorSets(const LogicalDevice& device, const DescriptorPool& pool, const DescriptorSetLayout& layout, const std::vector<UniformBuffer<T>>& buffers, const Sampler& sampler, const vk::ImageView& imageView);
};

///////////////////////////////////////////////////////////////////////////
template<class T>
DescriptorSet<T>::DescriptorSet(vk::DescriptorSet&& set, const LogicalDevice& device)
: descriptorSet{std::move(set)}
, device{device}
{
}

///////////////////////////////////////////////////////////////////////////
template<class T>
DescriptorSet<T>::DescriptorSet(DescriptorSet&& set)
: descriptorSet{std::move(set.descriptorSet)}
, device{set.device}
{
}

///////////////////////////////////////////////////////////////////////////
template<class T>
DescriptorSet<T>::~DescriptorSet()
{
}

///////////////////////////////////////////////////////////////////////////
template<class T>
const vk::DescriptorSet& DescriptorSet<T>::get() const
{
    return descriptorSet.get();
}

///////////////////////////////////////////////////////////////////////////
template<class T>
vk::DescriptorSet& DescriptorSet<T>::get()
{
    return descriptorSet.get();
}

///////////////////////////////////////////////////////////////////////////
template<class T>
void DescriptorSet<T>::assignBuffer(const UniformBuffer<T>& buffer, uint32_t bindingNumber)
{
    uniformBuffer.buffer = buffer.get();
    uniformBuffer.offset = 0;
    uniformBuffer.range = sizeof(T);

    vk::WriteDescriptorSet writeSet{};
    writeSet.sType = writeSet.structureType;
    writeSet.dstSet = descriptorSet.get();
    writeSet.dstBinding = bindingNumber;
    writeSet.dstArrayElement = 0;
    writeSet.descriptorType = vk::DescriptorType::eUniformBuffer;
    writeSet.descriptorCount = 1;
    writeSet.pBufferInfo = &uniformBuffer;
    writeSet.pImageInfo = nullptr;
    writeSet.pTexelBufferView = nullptr;

    writeSets.push_back(writeSet);
}

///////////////////////////////////////////////////////////////////////////
template<class T>
void DescriptorSet<T>::assignSampler(const Sampler& sampler, const vk::ImageView& imageView, uint32_t bindingNumber)
{
    imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
    imageInfo.imageView = imageView;
    imageInfo.sampler = sampler.get();

    vk::WriteDescriptorSet writeSet{};
    writeSet.sType = writeSet.structureType;
    writeSet.dstSet = descriptorSet.get();
    writeSet.dstBinding = bindingNumber;
    writeSet.dstArrayElement = 0;
    writeSet.descriptorType = vk::DescriptorType::eCombinedImageSampler;
    writeSet.descriptorCount = 1;
    writeSet.pBufferInfo = nullptr;
    writeSet.pImageInfo = &imageInfo;
    writeSet.pTexelBufferView = nullptr;

    writeSets.push_back(writeSet);
}

///////////////////////////////////////////////////////////////////////////
template<class T>
void DescriptorSet<T>::updateSets()
{
    device->updateDescriptorSets(static_cast<uint32_t>(writeSets.size()), writeSets.data(), 0, nullptr);
}

///////////////////////////////////////////////////////////////////////////
template<class T>
void DescriptorSet<T>::clearSets()
{
    writeSets.clear();
}

///////////////////////////////////////////////////////////////////////////
template<class T>
DescriptorSets<T>::DescriptorSets(const LogicalDevice& device, const DescriptorPool& pool, const DescriptorSetLayout& layout, const std::vector<UniformBuffer<T>>& buffers)
: std::vector<DescriptorSet<T>>{}
{
    const size_t size = buffers.size();
    const std::vector<vk::DescriptorSetLayout> layouts{size, layout.get()};

    vk::DescriptorSetAllocateInfo allocateInfo{};
    allocateInfo.sType = allocateInfo.structureType;
    allocateInfo.descriptorPool = pool.get();
    allocateInfo.descriptorSetCount = static_cast<uint32_t>(size);
    allocateInfo.pSetLayouts = layouts.data();

    auto sets = device->allocateDescriptorSets(allocateInfo);

    for (size_t i = 0; i < size; i++)
    {
        std::vector<DescriptorSet<T>>::emplace_back(std::move(sets[i]), device);
        const auto currentElement = std::vector<DescriptorSet<T>>::rbegin();

        currentElement->assignBuffer(buffers[i], 0);
        currentElement->updateSets();
    }
}

///////////////////////////////////////////////////////////////////////////
template<class T>
DescriptorSets<T>::DescriptorSets(const LogicalDevice& device, const DescriptorPool& pool, const DescriptorSetLayout& layout, const Sampler& sampler, const std::vector<vk::ImageView>& imageViews)
{
    const size_t size = imageViews.size();
    const std::vector<vk::DescriptorSetLayout> layouts{size, layout.get()};

    vk::DescriptorSetAllocateInfo allocateInfo{};
    allocateInfo.sType = allocateInfo.structureType;
    allocateInfo.descriptorPool = pool.get();
    allocateInfo.descriptorSetCount = static_cast<uint32_t>(size);
    allocateInfo.pSetLayouts = layouts.data();

    auto sets = device->allocateDescriptorSets(allocateInfo);

    for (size_t i = 0; i < size; i++)
    {
        std::vector<DescriptorSet<T>>::emplace_back(std::move(sets[i]), device);
        const auto currentElement = std::vector<DescriptorSet<T>>::rbegin();

        currentElement->assignSampler(sampler, imageViews[i], 0);
        currentElement->updateSets();
    }
}

///////////////////////////////////////////////////////////////////////////
template<class T>
DescriptorSets<T>::DescriptorSets(const LogicalDevice& device, const DescriptorPool& pool, const DescriptorSetLayout& layout, const std::vector<UniformBuffer<T>>& buffers, const Sampler& sampler, const vk::ImageView& imageView)
{
    const size_t size = buffers.size();
    const std::vector<vk::DescriptorSetLayout> layouts{size, layout.get()};

    vk::DescriptorSetAllocateInfo allocateInfo{};
    allocateInfo.sType = allocateInfo.structureType;
    allocateInfo.descriptorPool = pool.get();
    allocateInfo.descriptorSetCount = static_cast<uint32_t>(size);
    allocateInfo.pSetLayouts = layouts.data();

    auto sets = device->allocateDescriptorSets(allocateInfo);

    for (size_t i = 0; i < size; i++)
    {
        std::vector<DescriptorSet<T>>::emplace_back(std::move(sets[i]), device);
        const auto currentElement = std::vector<DescriptorSet<T>>::rbegin();

        currentElement->assignBuffer(buffers[i], 0);
        currentElement->assignSampler(sampler, imageView, 1);
        currentElement->updateSets();
    }
}