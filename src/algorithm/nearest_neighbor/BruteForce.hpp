#pragma once

// std
#include <cmath>
#include <limits>

#include "INearestNeighbor.hpp"

///////////////////////////////////////////////////////////////////////////
namespace algorithm
{
    ///////////////////////////////////////////////////////////////////////////
    template<NearestNeighborEntry EntryType>
    class BruteForceNearestNeighbor : public INearestNeighbors<EntryType>
    {
    private:
        static constexpr int Dimensions = decltype(EntryType::coordinate)::RowsAtCompileTime;
    public:
        BruteForceNearestNeighbor();
        virtual ~BruteForceNearestNeighbor();

        inline virtual void set(const std::vector<EntryType>& referencePoints) override;
        inline virtual INearestNeighbors<EntryType>::Result find(const Eigen::Vector<float, Dimensions>& target) const override;

    private:
        const std::vector<EntryType>* referencePoints;
    };
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
algorithm::BruteForceNearestNeighbor<EntryType>::BruteForceNearestNeighbor()
: referencePoints{nullptr}
{
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
algorithm::BruteForceNearestNeighbor<EntryType>::~BruteForceNearestNeighbor()
{
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
void algorithm::BruteForceNearestNeighbor<EntryType>::set(const std::vector<EntryType>& referencePoints)
{
    this->referencePoints = &referencePoints;
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
algorithm::INearestNeighbors<EntryType>::Result algorithm::BruteForceNearestNeighbor<EntryType>::find(const Eigen::Vector<float, Dimensions>& target) const
{
    const EntryType* nearestReferencePoint = nullptr;
    float smallestNorm = std::numeric_limits<float>().max();

    for (const auto& referencePoint : *referencePoints)
    {
        const auto currentDelta = referencePoint.coordinate - target;
        const auto currentNorm = currentDelta.squaredNorm();

        if (currentNorm < smallestNorm)
        {
            smallestNorm = currentNorm;
            nearestReferencePoint = &referencePoint;
        }
    }

    return {nearestReferencePoint, std::sqrt(smallestNorm)};
}