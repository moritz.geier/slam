#include "DescriptorPool.hpp"
#include "LogicalDevice.hpp"

// std
#include <vector>

///////////////////////////////////////////////////////////////////////////
DescriptorPool::DescriptorPool(const LogicalDevice& logicalDevice)
{
    constexpr uint32_t descriptorCount = 1000;
    const std::vector<vk::DescriptorPoolSize> poolSizes = {
        { vk::DescriptorType::eSampler, descriptorCount },
        { vk::DescriptorType::eCombinedImageSampler, descriptorCount },
        { vk::DescriptorType::eSampledImage, descriptorCount },
        { vk::DescriptorType::eStorageImage, descriptorCount },
        { vk::DescriptorType::eUniformTexelBuffer, descriptorCount },
        { vk::DescriptorType::eStorageTexelBuffer, descriptorCount },
        { vk::DescriptorType::eUniformBuffer, descriptorCount },
        { vk::DescriptorType::eStorageBuffer, descriptorCount },
        { vk::DescriptorType::eUniformBufferDynamic, descriptorCount },
        { vk::DescriptorType::eStorageBufferDynamic, descriptorCount },
        { vk::DescriptorType::eInputAttachment, descriptorCount }
    };

    const uint32_t poolSizesCount = static_cast<uint32_t>(poolSizes.size());

    vk::DescriptorPoolCreateInfo poolInfo{};
    poolInfo.sType = vk::StructureType::eDescriptorPoolCreateInfo;
    poolInfo.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
    poolInfo.maxSets = descriptorCount * poolSizesCount;
    poolInfo.poolSizeCount = poolSizesCount;
    poolInfo.pPoolSizes = poolSizes.data();

    descriptorPool = logicalDevice->createDescriptorPoolUnique(poolInfo);
}

///////////////////////////////////////////////////////////////////////////
DescriptorPool::DescriptorPool(DescriptorPool&& moveCommandPool)
{
    descriptorPool = std::move(moveCommandPool.descriptorPool);
}

///////////////////////////////////////////////////////////////////////////
DescriptorPool& DescriptorPool::operator=(DescriptorPool&& moveCommandPool)
{
    if (&moveCommandPool != this)
        descriptorPool = std::move(moveCommandPool.descriptorPool);
    return *this;
}

///////////////////////////////////////////////////////////////////////////
DescriptorPool::~DescriptorPool()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::DescriptorPool& DescriptorPool::get() const
{
    return descriptorPool.get();
}

///////////////////////////////////////////////////////////////////////////
vk::DescriptorPool& DescriptorPool::get()
{
    return descriptorPool.get();
}
