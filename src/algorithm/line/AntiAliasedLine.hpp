#pragma once

// slam
#include <algorithm/slam/Grid.hpp>

// std
#include <algorithm>
#include <array>
#include <cmath>
#include <numbers>
#include <limits>
#include <stdint.h>
#include <type_traits>

// utils
#include <utils/Definitions.hpp>

#include "Util.hpp"

namespace algorithm::line
{
    ///////////////////////////////////////////////////////////////////////////
    enum class LineMode : uint8_t
    {
        eAdditive = 0,
        eSubtractive,
        eOverride
    };

    ///////////////////////////////////////////////////////////////////////////
    struct Result
    {
        bool inBound;
        utils::definitions::Point2D<PointType> pointOne;
        utils::definitions::Point2D<PointType> pointTwo;
    };

    ///////////////////////////////////////////////////////////////////////////
    namespace
    {
        ///////////////////////////////////////////////////////////////////////////
        template<bool XAxis>
        float inline calculateSlope(const utils::definitions::Point2D<PointType>& start, const utils::definitions::Point2D<PointType>& end)
        {
            const utils::definitions::Point2D<SignedPointType> startSigned{static_cast<SignedPointType>(start.x()), static_cast<SignedPointType>(start.y())};
            const utils::definitions::Point2D<SignedPointType> endSigned{static_cast<SignedPointType>(end.x()), static_cast<SignedPointType>(end.y())};
            if constexpr (XAxis)
                return (endSigned.y() - startSigned.y()) / static_cast<float>(endSigned.x() - startSigned.x());
            else
                return (endSigned.x() - startSigned.x()) / static_cast<float>(endSigned.y() - startSigned.y());
        }

        ///////////////////////////////////////////////////////////////////////////
        template<bool XAxis>
        bool inline isAxisSmallerThan(const utils::definitions::Point2D<PointType>& start, const utils::definitions::Point2D<PointType>& end)
        {
            if constexpr (XAxis)
                return start.x() < end.x();
            else
                return start.y() < end.y();
        }

        ///////////////////////////////////////////////////////////////////////////
        template<typename T, slam::GridLayout Layout>
        void inline insertIntoGrid(slam::Grid<T, Layout>& grid, size_t x, size_t y, const T& value, LineMode lineMode)
        {
            auto& currentValue = grid.at(x, y);
            switch (lineMode)
            {
            case LineMode::eAdditive:
                currentValue = currentValue + value;
                break;
            case LineMode::eSubtractive:
                currentValue = currentValue - value;
                break;
            case LineMode::eOverride:
                currentValue = value;
                break;
            }
        }

        ///////////////////////////////////////////////////////////////////////////
        template<bool XAxis, typename T, slam::GridLayout Layout>
        void inline setGridValues(slam::Grid<T, Layout>& grid, const utils::definitions::Point2D<PointType>& start, const T& color, LineMode lineMode)
        {
            float intensity;

            if constexpr (XAxis)
                intensity = start.y() - static_cast<int>(start.y());
            else
                intensity = start.x() - static_cast<int>(start.x());


            const T scaledColor = color * intensity;
            if constexpr (XAxis)
                insertIntoGrid(grid, start.x(), start.y() + 1, scaledColor, lineMode);
            else
                insertIntoGrid(grid, start.x() + 1, start.y(), scaledColor, lineMode);

            const float inverseIntensity = (1 - intensity);
            const T inverseScaledColor = color * inverseIntensity;
            insertIntoGrid(grid, start.x(), start.y(), inverseScaledColor, lineMode);
        }

        ///////////////////////////////////////////////////////////////////////////
        template<bool XAxis>
        inline utils::definitions::Point2D<PointType> iterateAxis(utils::definitions::Point2D<PointType> point, float slope)
        {
            if constexpr(XAxis)
            {
                point.x()++;
                point.y() += slope;
            }
            else
            {
                point.x() += slope;
                point.y()++;
            }

            return point;
        }

        ///////////////////////////////////////////////////////////////////////////
        /// @brief Line clipping algorithm by [Liang–Barsky](https://en.wikipedia.org/wiki/Liang%E2%80%93Barsky_algorithm)
        /// @param pointOne One point of the line.
        /// @param pointTwo One point of the line.
        /// @param max Bounding box width and height of viewport.
        /// @return True if the line should be clipped, false otherwise.
        inline bool isLineOutOfFrame(utils::definitions::Point2D<PointType>& pointOne, utils::definitions::Point2D<PointType>& pointTwo, utils::definitions::Point2D<PointType> max)
        {
            const float p1 = -(pointTwo.x() - pointOne.x());
            const float p2 = -p1;
            const float p3 = -(pointTwo.y() - pointOne.y());
            const float p4 = -p3;

            const float q1 = pointOne.x() - 0;
            const float q2 = max.x() - pointOne.x();
            const float q3 = pointOne.y() - 0;
            const float q4 = max.y() - pointOne.y();

            std::array<float, 3> positiveValues;
            std::array<float, 3> negativeValues;
            size_t index = 1;
            positiveValues[0] = 1.0f;
            negativeValues[0] = 0.0f;

            // Is line parallel to clipping plane.
            if ((p1 == 0 && q1 < 0) || (p2 == 0 && q2 < 0) || (p3 == 0 && q3 < 0) || (p4 == 0 && q4 < 0))
                return true;

            if (p1 != 0)
            {
                const float r1 = q1 / p1;
                const float r2 = q2 / p2;
                if (p1 < 0)
                {
                    negativeValues[index] = r1;
                    positiveValues[index] = r2;
                }
                else
                {
                    negativeValues[index] = r2;
                    positiveValues[index] = r1;
                }
                index++;
            }

            if (p3 != 0)
            {
                const float r3 = q3 / p3;
                const float r4 = q4 / p4;
                if (p3 < 0)
                {
                    negativeValues[index] = r3;
                    positiveValues[index] = r4;
                }
                else
                {
                    negativeValues[index] = r4;
                    positiveValues[index] = r3;
                }
                index++;
            }

            const float rn1 = *std::max_element(negativeValues.begin(), negativeValues.begin() + index);
            const float rn2 = *std::min_element(positiveValues.begin(), positiveValues.begin() + index);

            // Is line outside clipping plane.
            if (rn1 > rn2)
                return true;

            pointTwo.x() = pointOne.x() + p2 * rn2;
            pointTwo.y() = pointOne.y() + p4 * rn2;

            pointOne.x() += p2 * rn1;
            pointOne.y() += p4 * rn1;

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////
        template<bool XPrimary, typename T, slam::GridLayout Layout>
        Result xiaolinWuLineAlgorithm(slam::Grid<T, Layout>& grid, utils::definitions::Point2D<PointType> start, utils::definitions::Point2D<PointType> end, T color, LineMode lineMode)
        {
            const float slope = calculateSlope<XPrimary>(start, end);
            // Line has to end before 1000 and since we always draw on e.g. x and x + 1 it has to be -2
            const utils::definitions::Point2D<PointType> boundingBox{static_cast<float>(grid.width() - 2), static_cast<float>(grid.height() - 2)};

            if (isLineOutOfFrame(start, end, boundingBox))
                return {false, {}, {}};

            Result result{true, start, end};

            do
            {
                setGridValues<XPrimary>(grid, start, color, lineMode);
                start = iterateAxis<XPrimary>(start, slope);
            } while (isAxisSmallerThan<XPrimary>(start, end));

            return result;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    template<class GridType, slam::GridLayout GridLayout>
    Result antiAliasedLine(slam::Grid<GridType, GridLayout>& grid, const utils::definitions::Point2D<PointType>& pointOne, const utils::definitions::Point2D<PointType>& pointTwo, GridType color, LineMode lineMode = LineMode::eAdditive)
    {
        if (pointOne == pointTwo)
        {
            return {false, {}, {}};
        }

        if (isLineThroughPointsShallow(pointOne, pointTwo))
        {
            if (pointOne.x() < pointTwo.x())
            {
                return xiaolinWuLineAlgorithm<true>(grid, pointOne, pointTwo, color, lineMode);
            }
            else
            {
                auto result = xiaolinWuLineAlgorithm<true>(grid, pointTwo, pointOne, color, lineMode);
                std::swap(result.pointOne, result.pointTwo);
                return result;
            }
        }
        else
        {
            if (pointOne.y() < pointTwo.y())
            {
                return xiaolinWuLineAlgorithm<false>(grid, pointOne, pointTwo, color, lineMode);
            }
            else
            {
                auto result = xiaolinWuLineAlgorithm<false>(grid, pointTwo, pointOne, color, lineMode);
                std::swap(result.pointOne, result.pointTwo);
                return result;
            }
        }
    }
}