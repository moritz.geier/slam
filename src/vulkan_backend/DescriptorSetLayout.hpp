#pragma once

// vulkan
#include <vulkan/vulkan.hpp>

class LogicalDevice;

///////////////////////////////////////////////////////////////////////////
class DescriptorSetLayout final
{
public:
    DescriptorSetLayout(const LogicalDevice& device);
    DescriptorSetLayout(DescriptorSetLayout&&);
    DescriptorSetLayout& operator=(DescriptorSetLayout&&);
    DescriptorSetLayout(const DescriptorSetLayout&) = delete;
    DescriptorSetLayout& operator=(const DescriptorSetLayout&) = delete;
    ~DescriptorSetLayout();

    const vk::DescriptorSetLayout& get() const;
    vk::DescriptorSetLayout& get();

private:
    vk::UniqueDescriptorSetLayout descriptorSetLayout;
};
