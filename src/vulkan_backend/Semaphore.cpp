#include "Semaphore.hpp"
#include "LogicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
Semaphore::Semaphore(const LogicalDevice& logicalDevice)
{
    vk::SemaphoreCreateInfo createInfo{};
    createInfo.sType = vk::StructureType::eSemaphoreCreateInfo;

    semaphore = logicalDevice->createSemaphoreUnique(createInfo);
}

///////////////////////////////////////////////////////////////////////////
Semaphore::Semaphore(Semaphore&& moveSemaphore)
{
    semaphore = std::move(moveSemaphore.semaphore);
}

///////////////////////////////////////////////////////////////////////////
Semaphore& Semaphore::operator=(Semaphore&& moveSemaphore)
{
    if (&moveSemaphore != this)
        semaphore = std::move(moveSemaphore.semaphore);

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Semaphore::~Semaphore()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::Semaphore& Semaphore::get() const
{
    return semaphore.get();
}

///////////////////////////////////////////////////////////////////////////
vk::Semaphore& Semaphore::get()
{
    return semaphore.get();
}
