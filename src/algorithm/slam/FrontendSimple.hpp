#pragma once

// rl
#include <rl/math/KdtreeNearestNeighbors.h>

// std
#include <cmath>
#include <numbers>
#include <ranges>

#include "SlamTypes.hpp"

///////////////////////////////////////////////////////////////////////////
namespace algorithm::slam
{
    ///////////////////////////////////////////////////////////////////////////
    template<utils::concepts::Timestep StepType>
    SlamGraph<StepType> frontendSimple(const SlamGraph<StepType>& inputGraph, const SolveConfiguration<StepType>& config, float& progress)
    {
        const float progressIncreasePerIteration = 1.0f / inputGraph.nodeSize();
        SlamGraph<StepType> graph;

        size_t posesInsertedSinceLoopClosure = 0;
        rl::math::KdtreeNearestNeighbors<LoopClosureSearchType> nearestNeighborSearch;

        if (config.runScanMatchingForOdometry)
        {
            const auto& steps = inputGraph.getNodes();
            auto* pLastNode = &graph.addNode(steps.front()->get());
            auto pLastAcceptedStepValue = steps.front();
            for (auto step = ++steps.begin(); step != steps.end(); step++)
            {
                const auto& node = *step;
                const auto translationDelta = (node->get().pose.translation() - pLastAcceptedStepValue->get().pose.translation()).squaredNorm();
                const auto rotationDeltaMatrix = (node->get().pose.rotation().transpose() * pLastAcceptedStepValue->get().pose.rotation());
                const float rotationDelta = std::atan2(rotationDeltaMatrix(1, 0), rotationDeltaMatrix(0, 0));
                if (translationDelta > 0.05 || rotationDelta > std::numbers::pi / 64 || true)
                {
                    const auto initialAlignment = pLastAcceptedStepValue->get().pose.inverse() * node->get().pose;
                    const auto result = config.pScanMatcher->findAlignment(node->get().measurements, pLastAcceptedStepValue->get().measurements, initialAlignment);

                    StepType newStep = node->get();
                    newStep.pose = pLastNode->get().pose * result.transformMatrix;

                    auto& currentNode = graph.addNode(std::move(newStep));
                    graph.addEdge({MeasurementType::eOdometry, result.transformMatrix, pLastNode->id(), 0}, currentNode, *pLastNode);
                    pLastNode = &currentNode;
                    pLastAcceptedStepValue = *step;
                    nearestNeighborSearch.push({currentNode->pose.translation(), currentNode.id()});

                    if (posesInsertedSinceLoopClosure % config.loopClosureAt == 0 && config.runLoopClosure)
                    {
                        size_t maxNodeId = currentNode.id() < 10 ? 0 : currentNode.id() - 10;
                        const auto neighborsInRadius = nearestNeighborSearch.radius({currentNode->pose.translation(), 0}, config.loopClosureRadius);
                        auto loopClosureInput = neighborsInRadius
                            | std::views::filter([maxNodeId](const auto& element) { return element.second.id < maxNodeId; })
                            | std::views::transform([&graph](const auto & element) { return typename algorithm::IScanMatching<StepType>::LoopClosureInput{graph.get(element.second.id).get(), element.second.id}; })
                            | std::views::common;

                        const auto result = config.pScanMatcher->findLoopClosure(currentNode.get(), {loopClosureInput.begin(), loopClosureInput.end()});
                        if (result.error <= config.loopClosureAcceptanceThreshold)
                        {
                            graph.addEdge({MeasurementType::eLoopClosure, result.transformMatrix, currentNode.id(), result.error}, currentNode, graph.get(result.matchedId));
                        }
                    }
                    posesInsertedSinceLoopClosure++;
                }

                progress += progressIncreasePerIteration;
            }
        }
        else
        {
            graph = inputGraph;
        }

        config.backend(graph, config);

        return graph;
    }
}
