#pragma once

// catch
#include <catch2/matchers/catch_matchers_floating_point.hpp>

// eigen
#include <Eigen/Eigen>

///////////////////////////////////////////////////////////////////////////
template<int N, int M>
void CheckMatrixEqual(const Eigen::Matrix<float, N, M>& left, const Eigen::Matrix<float, N, M>& right)
{
    static_assert(N > 0);
    static_assert(M > 0);
    for (size_t column = 0; column < M; column++)
    {
        for (size_t row = 0; row < N; row++)
            CHECK_THAT(right(row, column), Catch::Matchers::WithinAbs(left(row, column), 0.0001));
    }
    
}