#pragma once

// std
#include <concepts>
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

#include "Buffer.hpp"
#include "LogicalDevice.hpp"
#include "PhysicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
template<class Object>
class UniformBuffer final : public Buffer<Object>
{
public:
    UniformBuffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice);
    UniformBuffer(UniformBuffer<Object>&&);
    UniformBuffer<Object>& operator=(UniformBuffer<Object>&&);
    UniformBuffer(const UniformBuffer<Object>&) = delete;
    UniformBuffer<Object>& operator=(const UniformBuffer<Object>&) = delete;
    ~UniformBuffer() override;
};

///////////////////////////////////////////////////////////////////////////
template<class Object>
class UniformBuffers : public std::vector<UniformBuffer<Object>>
{
public:
    UniformBuffers(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t amount);
};

///////////////////////////////////////////////////////////////////////////
template<class Object>
UniformBuffer<Object>::UniformBuffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice)
: Buffer<Object>{logicalDevice, physicalDevice, vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, 1}
{
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
UniformBuffer<Object>::UniformBuffer(UniformBuffer<Object>&& moveBuffer)
: Buffer<Object>{std::move(moveBuffer)}
{
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
UniformBuffer<Object>& UniformBuffer<Object>::operator=(UniformBuffer<Object>&& moveBuffer)
{
    return Buffer<Object>::operator=(std::move(moveBuffer));
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
UniformBuffer<Object>::~UniformBuffer()
{
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
UniformBuffers<Object>::UniformBuffers(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t amount)
: std::vector<UniformBuffer<Object>>{}
{
    for (size_t i = 0; i < amount; i++)
        std::vector<UniformBuffer<Object>>::emplace_back(logicalDevice, physicalDevice);
}