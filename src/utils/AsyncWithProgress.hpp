#pragma once

// std
#include <functional>
#include <future>

#include "Async.hpp"

///////////////////////////////////////////////////////////////////////////
namespace utils
{
    ///////////////////////////////////////////////////////////////////////////
    template<typename ReturnType, typename ...Args>
    class AsyncWithProgress : public Async<ReturnType, Args...>
    {
    public:
        AsyncWithProgress();
        AsyncWithProgress(const std::function<ReturnType(float&, Args...)>& asyncFunction);
        AsyncWithProgress(AsyncWithProgress<ReturnType, Args...>&&);
        AsyncWithProgress<ReturnType, Args...>& operator=(AsyncWithProgress<ReturnType, Args...>&&);
        AsyncWithProgress(const AsyncWithProgress<ReturnType, Args...>&) = delete;
        AsyncWithProgress<ReturnType, Args...>& operator=(const AsyncWithProgress<ReturnType, Args...>&) = delete;
        virtual ~AsyncWithProgress() override;

        float getProgress() const;

    private:
        std::function<ReturnType(float&, Args...)> progressAsyncFunction;
        float progress;
    };
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
utils::AsyncWithProgress<ReturnType, Args...>::AsyncWithProgress()
: Async<ReturnType>{}
, progressAsyncFunction{}
, progress{0.0f}
{
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
utils::AsyncWithProgress<ReturnType, Args...>::AsyncWithProgress(const std::function<ReturnType(float&, Args...)>& asyncFunction)
: Async<ReturnType, Args...>{[&](Args... args) { progress = 0.0f; return progressAsyncFunction(progress, args...); }}
, progressAsyncFunction{asyncFunction}
, progress{0.0f}
{
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
utils::AsyncWithProgress<ReturnType, Args...>::AsyncWithProgress(AsyncWithProgress<ReturnType, Args...>&& async)
: Async<ReturnType>{[&]() { return progressAsyncFunction(progress); }}
, progressAsyncFunction{std::move(async.progessAsyncFunction)}
, progress{0.0f}
{
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
utils::AsyncWithProgress<ReturnType, Args...>& utils::AsyncWithProgress<ReturnType, Args...>::operator=(AsyncWithProgress<ReturnType, Args...>&& async)
{
    if (&async != this)
    {
        Async<ReturnType>::operator=([&]() { return progressAsyncFunction(progress); });
        progressAsyncFunction = std::move(async.progressAsyncFunction);
        progress = async.progress;
    }
    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
utils::AsyncWithProgress<ReturnType, Args...>::~AsyncWithProgress()
{
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
float utils::AsyncWithProgress<ReturnType, Args...>::getProgress() const
{
    return progress;
}
