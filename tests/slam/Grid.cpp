// catch2
#include <catch2/catch_test_macros.hpp>

// slam
#include <algorithm/slam/Grid.hpp>

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Grid Datastructure.", "[slam::Grid]")
{
    SECTION("Construction with width and height")
    {
        algorithm::slam::Grid<uint8_t> grid{8, 12};

        REQUIRE(grid.size() == 8 * 12);
        REQUIRE(grid.width() == 8);
        REQUIRE(grid.height() == 12);
        REQUIRE(!grid.empty());
    }

    algorithm::slam::Grid<uint16_t> grid;

    SECTION("Empty construction")
    {
        REQUIRE(grid.size() == 0);
        REQUIRE(grid.width() == 0);
        REQUIRE(grid.height() == 0);
        REQUIRE(grid.empty());
    }

    SECTION("Resize Grid.")
    {
        REQUIRE(grid.empty());

        constexpr size_t WIDTH = 100;
        constexpr size_t HEIGHT = 50;
        grid.resize(WIDTH, HEIGHT);

        REQUIRE(grid.size() == WIDTH * HEIGHT);
        REQUIRE(grid.width() == WIDTH);
        REQUIRE(grid.height() == HEIGHT);

        grid.insert(0, 0, 5);
        grid.insert(10, 10, 20);
        grid.resize(11, 11);

        REQUIRE(grid.size() == 11 * 11);
        REQUIRE(grid.width() == 11);
        REQUIRE(grid.height() == 11);
        REQUIRE(grid.at(0, 0) == 5);
        REQUIRE(grid.at(10, 10) == 20);

        grid.resize(5, 0);
        REQUIRE(grid.size() == 0);

        grid.resize(0, 5);
        REQUIRE(grid.size() == 0);
    }

    SECTION("Setting and getting data.")
    {
        constexpr size_t WIDTH = 20;
        constexpr size_t HEIGHT = 10;
        grid.resize(WIDTH, HEIGHT);
        grid.fill(0);

        grid.insert(0, 0, 20);
        REQUIRE(*grid.data() == 20);
        REQUIRE(grid.at(0, 0) == 20);
        grid.at(WIDTH / 2, HEIGHT / 2) = 5;
        REQUIRE(*(grid.data() + WIDTH / 2 + HEIGHT / 2 * WIDTH) == 5);
        REQUIRE(grid.at(WIDTH / 2, HEIGHT / 2) == 5);
        grid.insert(WIDTH - 1, HEIGHT - 1, 15);
        REQUIRE(*(grid.data() + (WIDTH - 1) + (HEIGHT - 1) * WIDTH) == 15);
        REQUIRE(grid.at(WIDTH - 1, HEIGHT - 1) == 15);


        algorithm::slam::Grid<uint16_t, algorithm::slam::GridLayout::eColumnMajor> columnMajorGrid;
        columnMajorGrid.resize(WIDTH, HEIGHT);
        columnMajorGrid.fill(0);

        columnMajorGrid.insert(0, 0, 20);
        REQUIRE(*columnMajorGrid.data() == 20);
        REQUIRE(columnMajorGrid.at(0, 0) == 20);
        columnMajorGrid.at(WIDTH / 2, HEIGHT / 2) = 5;
        REQUIRE(*(columnMajorGrid.data() + WIDTH / 2 * HEIGHT + HEIGHT / 2) == 5);
        REQUIRE(columnMajorGrid.at(WIDTH / 2, HEIGHT / 2) == 5);
        columnMajorGrid.insert(WIDTH - 1, HEIGHT - 1, 15);
        REQUIRE(*(columnMajorGrid.data() + (WIDTH - 1) * HEIGHT + (HEIGHT - 1)) == 15);
        REQUIRE(columnMajorGrid.at(WIDTH - 1, HEIGHT - 1) == 15);
    }

    SECTION("Clear Grid.")
    {
        constexpr size_t WIDTH = 20;
        constexpr size_t HEIGHT = 10;
        grid.resize(20, 10);

        grid.insert(0, 0, 20);
        grid.insert(WIDTH / 2, HEIGHT / 2, 5);
        grid.insert(WIDTH - 1, HEIGHT - 1, 15);
        grid.fill(0);
        REQUIRE(grid.at(0, 0) == 0);
        REQUIRE(grid.at(WIDTH / 2, HEIGHT / 2) == 0);
        REQUIRE(grid.at(WIDTH - 1, HEIGHT - 1) == 0);
    }

    SECTION("Insert another Grid.")
    {
        algorithm::slam::Grid<uint16_t> smallGrid{5, 5};
        smallGrid.insert(0, 0, 12);
        smallGrid.insert(4, 0, 11);
        smallGrid.insert(0, 4, 10);
        smallGrid.insert(4, 4, 9);

        grid.resize(20, 20);
        grid.fill(0);
        grid.insert(0, 0, 99);
        grid.insert(19, 19, 98);

        size_t x = 1;
        size_t y = 1;
        grid.insert(x, y, smallGrid);

        REQUIRE(grid.width() == 20);
        REQUIRE(grid.height() == 20);

        REQUIRE(grid.at(0, 0) == 99);
        REQUIRE(grid.at(x, y) == 12);
        REQUIRE(grid.at(x + 4, y) == 11);
        REQUIRE(grid.at(x, y + 4) == 10);
        REQUIRE(grid.at(x + 4, y + 4) == 9);
        REQUIRE(grid.at(19, 19) == 98);

        x = 6;
        y = 18;
        grid.insert(x, y, smallGrid);

        REQUIRE(grid.width() == 20);
        REQUIRE(grid.height() == y + 5);

        REQUIRE(grid.at(0, 0) == 99);
        REQUIRE(grid.at(x, y) == 12);
        REQUIRE(grid.at(x + 4, y) == 11);
        REQUIRE(grid.at(x, y + 4) == 10);
        REQUIRE(grid.at(x + 4, y + 4) == 9);
        REQUIRE(grid.at(19, 19) == 98);

        grid.resize(20, 20);
        x = 25;
        y = 0;
        grid.insert(x, y, smallGrid);

        REQUIRE(grid.width() == x + 5);
        REQUIRE(grid.height() == 20);

        REQUIRE(grid.at(0, 0) == 99);
        REQUIRE(grid.at(x, y) == 12);
        REQUIRE(grid.at(x + 4, y) == 11);
        REQUIRE(grid.at(x, y + 4) == 10);
        REQUIRE(grid.at(x + 4, y + 4) == 9);
        REQUIRE(grid.at(19, 19) == 98);
    }
}