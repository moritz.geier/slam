#include "RelationsReader.hpp"
#include "File.hpp"
#include "String.hpp"

// std
#include <cmath>
#include <string>

///////////////////////////////////////////////////////////////////////////
utils::RelationsReader::RelationsReader(const std::filesystem::path& log)
{
    logFile = openFile(log);
}

///////////////////////////////////////////////////////////////////////////
utils::RelationsReader::~RelationsReader()
{
    logFile.close();
}

///////////////////////////////////////////////////////////////////////////
std::optional<utils::Relation> utils::RelationsReader::nextStep()
{
    std::string line;
    std::optional<Relation> relation = std::nullopt;

    while (!relation.has_value() && std::getline(logFile, line))
    {
        relation = parseLine(line);
    }

    return relation;
}

///////////////////////////////////////////////////////////////////////////
std::vector<utils::Relation> utils::RelationsReader::allSteps(const std::filesystem::path& log, float& progress)
{
    auto logFile = openFile(log);
    std::string file = file::readAll(logFile);
    auto lines = string::split(file, "\n");

    const float progressIncrease = 1 / static_cast<float>(lines.size());
    std::vector<Relation> relations;
    relations.reserve(lines.size());
    for (const auto& line : lines)
    {
        auto step = parseLine(line);
        if (step.has_value())
            relations.push_back(step.value());
        progress += progressIncrease;
    }

    return relations;
}

///////////////////////////////////////////////////////////////////////////
std::vector<utils::Relation> utils::RelationsReader::allSteps(const std::filesystem::path& log)
{
    float temp = 0;
    return allSteps(log, temp);
}

///////////////////////////////////////////////////////////////////////////
std::ifstream utils::RelationsReader::openFile(const std::filesystem::path& log)
{
    if (log.extension() != ".relations")
        throw std::runtime_error{"[RelationsReader] Log file should have a .relations extension, file: " + log.string()};

    if (!std::filesystem::exists(log))
        throw std::runtime_error{"[RelationsReader] Log file " + log.string() + " does not exist."};

    std::ifstream logFile;
    logFile.open(log);
    if (!logFile.is_open())
        throw std::runtime_error{"[RelationsReader] Could not open file: " + log.string()};

    return logFile;
}

///////////////////////////////////////////////////////////////////////////
std::optional<utils::Relation> utils::RelationsReader::parseLine(const std::string& line)
{
    Relation relation;

    auto splitLine = string::split(line, " ");
    if (splitLine.size() == 8)
    {
        relation.idOne = splitLine[0];
        relation.idTwo = splitLine[1];

        const auto x = std::stof(splitLine[2]);
        const auto y = std::stof(splitLine[3]);
        relation.transformation.translation() = Eigen::Vector2f{x, y};
        const auto theta = std::stof(splitLine[7]);
        relation.transformation.linear() = Eigen::Matrix2f{Eigen::Rotation2Df{theta}};

        return relation;
    }

    return std::nullopt;
}