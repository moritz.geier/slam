#pragma once

// eigen
#include <Eigen/Dense>

// std
#include <vector>

///////////////////////////////////////////////////////////////////////////
namespace math
{
    ///////////////////////////////////////////////////////////////////////////
    Eigen::Matrix2f calculateCovariance(const std::vector<Eigen::Vector2f>& points)
    {
        Eigen::Vector2f sum;
        sum.setZero();
        for (const auto& date : points)
            sum += date;
        const Eigen::Vector2f mean = sum / points.size();

        sum.setZero();
        float covarianceSum = 0;
        for (const auto& date : points)
        {
            const auto difference = date - mean;
            sum += difference.cwiseProduct(difference);
            covarianceSum += difference.x() * difference.y();
        }

        const auto variance = sum / (points.size() - 1);
        const auto covariance = covarianceSum / (points.size() - 1);

        Eigen::Matrix2f result;
        result << variance.x(), covariance, covariance, variance.y();
        return result;
    }
}