// catch2
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

// std
#include <filesystem>
#include <format>
#include <fstream>
#include <string>

// utils
#include <utils/BMP.hpp>
#include <utils/Definitions.hpp>

///////////////////////////////////////////////////////////////////////////
namespace Catch
{
    template<>
    struct StringMaker<utils::definitions::RGB>
    {
        static std::string convert(const utils::definitions::RGB& rgb)
        {
            return std::format("{{r: {}, g: {}, b: {}}}", rgb.red, rgb.green, rgb.blue);
        }
    };
}

///////////////////////////////////////////////////////////////////////////
template<typename T>
void requireValueFromFile(std::ifstream& file, T expected)
{
    T actual;
    file.read(reinterpret_cast<char*>(&actual), sizeof(T));
    REQUIRE(actual == expected);
}

///////////////////////////////////////////////////////////////////////////
void requireBmpFile(const std::filesystem::path& bmp, const utils::BmpHeader& bmpHeader, const utils::DibHeader& dibHeader, const std::vector<utils::definitions::RGB>& expectedImageData)
{
    REQUIRE(std::filesystem::exists(bmp));

    std::ifstream bmpFile{bmp, std::ios_base::binary | std::ios_base::ate};
    REQUIRE(bmpFile.is_open());

    const size_t fileSize = bmpFile.tellg();
    REQUIRE(fileSize == bmpHeader.fileSize);
    bmpFile.seekg(0);

    requireValueFromFile(bmpFile, bmpHeader.magicNumber);
    requireValueFromFile(bmpFile, bmpHeader.fileSize);
    requireValueFromFile(bmpFile, bmpHeader.appSpecific1);
    requireValueFromFile(bmpFile, bmpHeader.appSpecific2);
    requireValueFromFile(bmpFile, bmpHeader.offsetBitmap);

    requireValueFromFile(bmpFile, dibHeader.headerSize);
    requireValueFromFile(bmpFile, dibHeader.width);
    requireValueFromFile(bmpFile, dibHeader.height);
    requireValueFromFile(bmpFile, dibHeader.colorPlanes);
    requireValueFromFile(bmpFile, dibHeader.bitsPerPixel);
    requireValueFromFile(bmpFile, dibHeader.compression);
    requireValueFromFile(bmpFile, dibHeader.imageSize);
    requireValueFromFile(bmpFile, dibHeader.horizontalResolution);
    requireValueFromFile(bmpFile, dibHeader.verticalResolution);
    requireValueFromFile(bmpFile, dibHeader.paletteSize);
    requireValueFromFile(bmpFile, dibHeader.importantColors);

    REQUIRE(bmpHeader.offsetBitmap == bmpFile.tellg());

    std::vector<utils::definitions::RGB> actualImageData;
    actualImageData.resize(expectedImageData.size());

    std::vector<uint8_t> actualImageRow;
    actualImageRow.resize(dibHeader.width * 3);
    const size_t padding = (24 * dibHeader.width + 31) / 32 * 4 - dibHeader.width * 3;

    for(size_t y = 0; y < dibHeader.height; y++)
    {
        bmpFile.read(reinterpret_cast<char*>(actualImageRow.data()), actualImageRow.size());
        const size_t yOffset = y * dibHeader.width;

        for(size_t x = 0; x < dibHeader.width; x++)
        {
            const size_t xOffset = x * 3;
            actualImageData[yOffset + x].red = actualImageRow[xOffset];
            actualImageData[yOffset + x].green = actualImageRow[xOffset + 1];
            actualImageData[yOffset + x].blue = actualImageRow[xOffset + 2];
        }

        bmpFile.seekg(padding, std::ios_base::cur);
    }

    REQUIRE_THAT(expectedImageData, Catch::Matchers::Equals(actualImageData));

    char c;
    bmpFile.read(&c, sizeof(c));
    REQUIRE(bmpFile.eof());
}

///////////////////////////////////////////////////////////////////////////
void requireBmp(const algorithm::slam::Grid<uint8_t>& grid, const std::filesystem::path& filePath)
{
    const size_t padding = (24 * grid.width() + 31) / 32 * 4 - grid.width() * 3;
    auto bmp = utils::BMP{grid};

    const auto& bmpHeader = bmp.getBmpHeader();
    REQUIRE(bmpHeader.magicNumber == 0x4D42);
    REQUIRE(bmpHeader.fileSize == utils::BmpHeader::size() + utils::DibHeader::size() + grid.size() * 3 + padding * grid.height());

    const auto& dibHeader = bmp.getDibHeader();
    REQUIRE(dibHeader.headerSize == utils::DibHeader::size());
    REQUIRE(dibHeader.width == grid.width());
    REQUIRE(dibHeader.height == grid.height());
    REQUIRE(dibHeader.colorPlanes == 1);
    REQUIRE(dibHeader.bitsPerPixel == 24);
    REQUIRE(dibHeader.compression == 0);
    REQUIRE(dibHeader.imageSize == grid.size() * 3);
    REQUIRE(dibHeader.horizontalResolution == 2835);
    REQUIRE(dibHeader.verticalResolution == 2835);
    REQUIRE(dibHeader.paletteSize == 0);
    REQUIRE(dibHeader.importantColors == 0);

    const auto& imageData = bmp.getImageData();
    REQUIRE(imageData.size() == grid.size());

    if (!grid.empty())
    {
        std::vector<utils::definitions::RGB> gridToImageData;
        gridToImageData.resize(grid.size());

        for (size_t y = 0; y < grid.height(); y++)
        {
            const size_t offset = y * grid.width();
            for (size_t x = 0; x < grid.width(); x++)
            {
                const auto& value = grid.at(x, y);
                gridToImageData[offset + x].red = value;
                gridToImageData[offset + x].green = value;
                gridToImageData[offset + x].blue = value;
            }
        }

        REQUIRE_THAT(imageData, Catch::Matchers::Equals(gridToImageData));
    }

    bmp.save(filePath);
    requireBmpFile(filePath, bmpHeader, dibHeader, imageData);
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Saving grid as BMP.", "[utils::BMP]" )
{
    std::filesystem::path filePath = std::filesystem::current_path() / "temp";
    std::filesystem::create_directories(filePath);

    algorithm::slam::Grid<uint8_t> grid;

    SECTION("Empty grid.")
    {
        std::string fileName = "empty_" + std::to_string(std::rand()) + ".bmp";
        filePath /= fileName;

        REQUIRE(grid.empty());
        requireBmp(grid, filePath);
    }

    SECTION("Grid orientation.")
    {
        std::string fileName = "orientation_" + std::to_string(std::rand()) + ".bmp";
        filePath /= fileName;

        grid.resize(4, 4);
        grid.fill(0);
        grid.insert(0, 0, 0x11);
        grid.insert(0, grid.height() - 1, 0x55);
        grid.insert(grid.width() - 1, 0, 0xAA);
        grid.insert(grid.width() - 1, grid.height() - 1, 0xFF);

        requireBmp(grid, filePath);
    }

    SECTION("Padding.")
    {
        std::string fileName = "padding_" + std::to_string(std::rand()) + ".bmp";
        filePath /= fileName;

        grid.resize(2, 2);
        grid.fill(0);
        grid.insert(0, 0, 0x00);
        grid.insert(0, grid.height() - 1, 0x55);
        grid.insert(grid.width() - 1, 0, 0xAA);
        grid.insert(grid.width() - 1, grid.height() - 1, 0xFF);

        requireBmp(grid, filePath);
    }

    SECTION("Big image.")
    {
        std::string fileName = "padding_" + std::to_string(std::rand()) + ".bmp";
        filePath /= fileName;

        grid.resize(935, 935);
        grid.fill(0x40);
        grid.insert(0, 0, 0x00);
        grid.insert(0, grid.height() - 1, 0x55);
        grid.insert(grid.width() - 1, 0, 0xAA);
        grid.insert(grid.width() - 1, grid.height() - 1, 0xFF);

        requireBmp(grid, filePath);
    }

    std::filesystem::remove_all(filePath);
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Editing BMP settings.", "[utils::BMP]" )
{
    std::filesystem::path filePath = std::filesystem::current_path() / "temp";
    std::filesystem::create_directories(filePath);

    utils::BMP bmp{algorithm::slam::Grid<uint8_t>{}};

    SECTION("Changing resolution.")
    {
        const std::string fileName = std::format("resolution_{}.bmp", std::to_string(std::rand()));
        filePath /= fileName;

        const auto& dibHeader = bmp.getDibHeader();
        REQUIRE(dibHeader.horizontalResolution == dibHeader.verticalResolution);
        REQUIRE(dibHeader.horizontalResolution == 2835);

        bmp.setResolution(100);
        REQUIRE(dibHeader.horizontalResolution == dibHeader.verticalResolution);
        REQUIRE(dibHeader.horizontalResolution == 100);

        bmp.save(filePath);

        std::ifstream bmpFile{filePath, std::ios_base::binary};
        REQUIRE(bmpFile.is_open());
        bmpFile.seekg(0x26);

        uint32_t horizontalResolution;
        bmpFile.read(reinterpret_cast<char*>(&horizontalResolution), sizeof(horizontalResolution));

        uint32_t vecticalResolution;
        bmpFile.read(reinterpret_cast<char*>(&vecticalResolution), sizeof(vecticalResolution));

        REQUIRE(horizontalResolution == vecticalResolution);
        REQUIRE(horizontalResolution == dibHeader.horizontalResolution);
    }

    std::filesystem::remove_all(filePath);
}