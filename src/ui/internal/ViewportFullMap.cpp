#include "ViewportFullMap.hpp"
#include "../Toggle.hpp"
#include "../ProgressBar.hpp"

// algorithm
#include <algorithm/scan_matching/Icp.hpp>
#include <algorithm/scan_matching/Open3dIcp.hpp>
//#include <algorithm/scan_matching/PclNdt.hpp>
#include <algorithm/nearest_neighbor/KdTree.hpp>
#include <algorithm/slam/BackendLs.hpp>
#include <algorithm/slam/FrontendSimple.hpp>
#include <algorithm/slam/Slam.hpp>
#include <algorithm/slam/ToGrid.hpp>

// cmake
#include <CMakeVariables.hpp>

// imgui
#include <imgui.h>

// std
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>

// utils
#include <utils/Definitions.hpp>
#include <utils/DrawPoseGraph.hpp>
#include <utils/RelationsReader.hpp>

///////////////////////////////////////////////////////////////////////////
namespace ui
{
    ///////////////////////////////////////////////////////////////////////////
    namespace
    {
        ///////////////////////////////////////////////////////////////////////////
        enum class ScanMatcherAlgorithm : uint8_t
        {
            eVanillaIcp = 0,
            eOpen3dIcp
        };

        ///////////////////////////////////////////////////////////////////////////
        struct AsyncSlamReturn
        {
            algorithm::slam::SlamGraph<utils::definitions::Timestep> graph;
            std::chrono::milliseconds time;
            Eigen::Vector3f errorMetric;
        };

        ///////////////////////////////////////////////////////////////////////////
        AsyncSlamReturn slamSolver(float& progress, const algorithm::slam::SlamGraph<utils::definitions::Timestep>& graph, const std::vector<utils::Relation>& relations, ScanMatcherAlgorithm scanMatcher, float scanMatchingThreshold, float loopClosureRadius, bool runScanMatchingForOdometry, bool runLoopClosure, bool runBackend)
        {
            algorithm::slam::SolveConfiguration<utils::definitions::Timestep> solveConfig;
            solveConfig.backend = algorithm::slam::backendLs<utils::definitions::Timestep>;
            solveConfig.frontend = algorithm::slam::frontendSimple<utils::definitions::Timestep>;
            solveConfig.loopClosureAt = 1;
            solveConfig.loopClosureAcceptanceThreshold = 0.0f;
            solveConfig.loopClosureRadius = loopClosureRadius;
            solveConfig.runScanMatchingForOdometry = runScanMatchingForOdometry;
            solveConfig.runLoopClosure = runLoopClosure;
            solveConfig.runBackend = runBackend;

            switch (scanMatcher)
            {
                case ScanMatcherAlgorithm::eVanillaIcp:
                {
                    using ScanMatcher = algorithm::Icp<utils::definitions::Timestep>;
                    ScanMatcher::Configuration scanMatcherConfig;
                    scanMatcherConfig.threshold = scanMatchingThreshold;
                    scanMatcherConfig.pNearestNeighborAlgorithm = std::make_unique<algorithm::KdTree<algorithm::DefaultSearch<2>>>();

                    solveConfig.pScanMatcher = std::make_unique<ScanMatcher>(scanMatcherConfig);
                    break;
                }
                case ScanMatcherAlgorithm::eOpen3dIcp:
                {
                    using ScanMatcher = algorithm::Open3dIcp;
                    ScanMatcher::Configuration scanMatcherConfig;
                    scanMatcherConfig.threshold = scanMatchingThreshold;

                    solveConfig.pScanMatcher = std::make_unique<ScanMatcher>(scanMatcherConfig);
                    break;
                }
            }

            const auto begin = std::chrono::steady_clock::now();
            const auto result = algorithm::slam::solve(graph, progress, solveConfig);
            const auto end = std::chrono::steady_clock::now();

            Eigen::Vector3f score{Eigen::Vector3f::Zero()};
            const auto& nodes = result.getNodes();
            score.setZero();
            std::ofstream errorsCsv{"errors.csv"};
            for (const auto& relation : relations)
            {
                const auto firstNode = std::find_if(nodes.begin(), nodes.end(), [&relation](const auto& node) { return node->get().id == relation.idOne; });
                if (firstNode == nodes.end())
                    continue;
                const auto secondNode = std::find_if(nodes.begin(), nodes.end(), [&relation](const auto& node) { return node->get().id == relation.idTwo; });
                if (secondNode == nodes.end())
                    continue;
                const auto actualTransformation = firstNode->get()->get().pose.inverse() * secondNode->get()->get().pose;
                const auto error = relation.transformation.inverse() * actualTransformation;
                
                const auto rotation = std::atan2(error(1, 0), error(0, 0));
                Eigen::Vector3f currentError;
                currentError(0) = error(0, 2) * error(0, 2);
                currentError(1) = error(1, 2) * error(1, 2);
                currentError(2) = rotation * rotation;
                score += currentError;
                errorsCsv << std::format("{}, {}, {},\n", currentError(0), currentError(1), currentError(2));
            }

            return AsyncSlamReturn{result, std::chrono::duration_cast<std::chrono::milliseconds>(end - begin), score};
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    bool fullMap(algorithm::slam::Grid<ViewportGridColorMode>& grid, algorithm::slam::SlamGraph<utils::definitions::Timestep>& graph, float scale, bool recalculateGrid, bool recalculateGraph)
    {
        static bool firstRunthrough = true;
        static std::chrono::milliseconds solveTime{0};
        static algorithm::slam::SlamGraph<utils::definitions::Timestep> originalGraph;
        static std::vector<utils::Relation> relations = utils::RelationsReader::allSteps(CMake::programRootDirectory / "resources" / "aces-building.relations");
        bool hasGridChanged = false;
        if (firstRunthrough || recalculateGraph)
        {
            grid.fill(getGridConfiguration(0).unknownSpace);
            originalGraph = graph;
            firstRunthrough = false;
            hasGridChanged = true;
        }

        if (ImGui::Button("Filter Graph."))
        {
            algorithm::slam::SlamGraph<utils::definitions::Timestep> newGraph;
            for (const auto& node : graph.getNodes())
            {
                const auto exists = std::find_if(relations.begin(), relations.end(), [&node](const auto& relation){ return node->get().id == relation.idOne || node->get().id == relation.idTwo; });
                if (exists != relations.end())
                    newGraph.addNode(node->get());
            }
            graph = newGraph;
        }

        static utils::PoseGraphResult poseGraphResult{std::numeric_limits<float>().infinity(), -std::numeric_limits<float>().infinity(), 0.0f};
        static bool lastShowPoseGraph = false;
        static bool showPoseGraph = false;
        ui::Toggle("Show pose graph", showPoseGraph);

        ImGui::Separator();

        static float scanMatchingThreshold = 0.5f;
        ImGui::SliderFloat("Scan Matching Threshold.", &scanMatchingThreshold, 0.1f, 50.0f, "%.1f");
        static float loopClosureRadius = 2.0f;
        ImGui::SliderFloat("Loop Closure Radius.", &loopClosureRadius, 0.1f, 50.0f, "%.1f");

        static int scanMatcher = static_cast<int>(ScanMatcherAlgorithm::eOpen3dIcp);
        ImGui::Combo("Scan Matching Algorithm", &scanMatcher, "Vanilla ICP\0Open3d ICP\0PCL NDT\0");
        static bool applyScanMatching = false;
        ui::Toggle("Apply Scanmatching odometry", applyScanMatching);
        static bool applyLoopClosure = false;
        ui::Toggle("Apply Loop closure", applyLoopClosure);
        static bool applyBackend = false;
        ui::Toggle("Apply Backend", applyBackend);

        bool renderMap = ImGui::Button("Render occupancy gird");
        ImGui::SameLine();
        bool resetGraph = ImGui::Button("Reset graph");

        ImGui::Separator();

        static Eigen::Vector3f score{Eigen::Vector3f::Zero()};

        ImGui::Text(std::format("Nodes: {}", graph.nodeSize()).c_str());
        ImGui::SameLine();
        ImGui::Text(std::format("Edges: {}", graph.edgeSize()).c_str());
        ImGui::Text(std::format("Accuracy Score [x^2, y^2, theta^2]: [{}, {}, {}]", score(0), score(1), score(2)).c_str());

        ImGui::Text(std::format("Solved in {:d}:{:02d}.{:04d} (min:s:ms)", std::chrono::duration_cast<std::chrono::minutes>(solveTime).count(), std::chrono::duration_cast<std::chrono::seconds>(solveTime).count() % 60, solveTime.count() % 1000).c_str());

        if (showPoseGraph)
        {
            if (ImGui::TreeNode("Loop closure info"))
            {
                ImGui::Text(std::format("Smallest RMSE: {}", poseGraphResult.minRmse).c_str());
                ImGui::Text(std::format("Biggest RMSE: {}", poseGraphResult.maxRmse).c_str());
                ImGui::Text(std::format("Average RMSE: {}", poseGraphResult.averageRmse).c_str());
                ImGui::TreePop();
            }
        }

        bool poseGraphToggleChanged = false;
        if (lastShowPoseGraph != showPoseGraph)
        {
            poseGraphToggleChanged = true;
            lastShowPoseGraph = showPoseGraph;
        }

        static utils::AsyncWithProgress<AsyncSlamReturn, const algorithm::slam::SlamGraph<utils::definitions::Timestep>&, const std::vector<utils::Relation>&, ScanMatcherAlgorithm, float, float, bool, bool, bool> asyncSlamSolver{slamSolver};
        if (asyncSlamSolver.isFinished())
        {
            auto result = asyncSlamSolver.getResult();
            graph = std::move(result.graph);
            solveTime = result.time;
            recalculateGrid = true;
            score = result.errorMetric;
        }

        if (renderMap || asyncSlamSolver.isRunning())
            ui::ProgressBar("SLAM Optimization", asyncSlamSolver, graph, relations, static_cast<ScanMatcherAlgorithm>(scanMatcher), scanMatchingThreshold, loopClosureRadius, applyScanMatching, applyLoopClosure, applyBackend);

        if (resetGraph)
        {
            graph = originalGraph;
            recalculateGrid = true;
        }

        static utils::AsyncWithProgress<algorithm::slam::Grid<ui::ViewportGridColorMode>, const algorithm::slam::SlamGraph<utils::definitions::Timestep>&, float> asyncGrid{[](float& progress, const auto& graph, float scale) {
            const auto config = getGridConfiguration(scale);
            const auto grid = algorithm::slam::toGrid(graph, config, progress);
            return grid;
        }};

        if (asyncGrid.isFinished())
        {
            grid = asyncGrid.getResult();
            hasGridChanged = true;
        }

        if (recalculateGrid || (poseGraphToggleChanged && !showPoseGraph) || asyncGrid.isRunning())
            ui::ProgressBar("Draw Occupancy Grid", asyncGrid, graph, scale);

        if (poseGraphToggleChanged || recalculateGraph || resetGraph || recalculateGrid)
        {
            if (showPoseGraph)
            {
                poseGraphResult = utils::poseGraphView(grid, graph, {getPathColor(), getLoopClosureColor(), scale});
                hasGridChanged = true;
            }
            else
            {
                recalculateGrid = true;
            }
        }

        return hasGridChanged;
    }
}