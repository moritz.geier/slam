#pragma once

// slam
#include <algorithm/slam/Grid.hpp>

// std
#include <vector>

#include "ViewportConfig.hpp"

namespace math
{
    template<typename N, typename E>
    class Graph;
}

namespace algorithm::slam
{
    struct EdgeValue;
}

namespace utils::definitions
{
    struct Timestep;
};

namespace ui
{
    bool replay(algorithm::slam::Grid<ViewportGridColorMode>& grid, const math::Graph<utils::definitions::Timestep, algorithm::slam::EdgeValue>& graph, float scale, bool recalculateGrid);
}