#pragma once

// std
#include <string>
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

#include "PhysicalDevice.hpp"

class Instance final
{
public:
    Instance(const std::string& name, const std::vector<const char*>& validationLayers);
    Instance(Instance&&);
    Instance& operator=(Instance&&);
    Instance(const Instance&) = delete;
    Instance& operator=(const Instance&) = delete;
    ~Instance();

    const vk::Instance& get() const;
    vk::Instance& get();

    const vk::UniqueInstance& operator->() const;
    vk::UniqueInstance& operator->();

private:
    vk::UniqueInstance instance;
};