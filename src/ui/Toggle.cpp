#include "Toggle.hpp"

// imgui
#include "imgui.h"

///////////////////////////////////////////////////////////////////////////
void ui::Toggle(const std::string& name, bool& value)
{
    ImVec2 position = ImGui::GetCursorScreenPos();
    ImDrawList* pDrawList = ImGui::GetWindowDrawList();
    const auto& style = ImGui::GetStyle();

    float height = ImGui::GetFrameHeight();
    float width = height * 1.55f;
    float radius = height * 0.50f;

    if (ImGui::InvisibleButton(name.c_str(), ImVec2(width, height)))
        value = !value;

    ImColor backgroundColor;
    if (ImGui::IsItemHovered())
        backgroundColor = value ? style.Colors[ImGuiCol_ButtonActive] : style.Colors[ImGuiCol_ButtonHovered];
    else
        backgroundColor = value ? style.Colors[ImGuiCol_ButtonActive] : style.Colors[ImGuiCol_Button];

    pDrawList->AddRectFilled(position, ImVec2(position.x + width, position.y + height), backgroundColor, height * 0.5f);

    const ImVec2 sliderPosition{value ? (position.x + width - radius) : (position.x + radius), position.y + radius};
    const ImColor toggleColor = style.Colors[ImGuiCol_SliderGrab];
    pDrawList->AddCircleFilled(sliderPosition, radius - 1.5f, toggleColor);

    ImGui::SameLine();
    ImGui::Text(name.c_str());
}