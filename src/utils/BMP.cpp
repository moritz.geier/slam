#include "BMP.hpp"
#include "Definitions.hpp"

// std
#include <format>
#include <string>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    template<typename T>
    inline void internalWriteTo(std::ostream& output, T value)
    {
        output.write(reinterpret_cast<const char*>(&value), sizeof(T));
    }

    ///////////////////////////////////////////////////////////////////////////
    void writeTo(std::ostream& output, const utils::definitions::RGB& rgb)
    {
        internalWriteTo(output, static_cast<uint8_t>(rgb.blue));
        internalWriteTo(output, static_cast<uint8_t>(rgb.green));
        internalWriteTo(output, static_cast<uint8_t>(rgb.red));
    }
}

///////////////////////////////////////////////////////////////////////////
void utils::BmpHeader::writeTo(std::ostream& output) const
{
    internalWriteTo(output, magicNumber);
    internalWriteTo(output, fileSize);
    internalWriteTo(output, appSpecific1);
    internalWriteTo(output, appSpecific2);
    internalWriteTo(output, offsetBitmap);
}

///////////////////////////////////////////////////////////////////////////
void utils::DibHeader::writeTo(std::ostream& output) const
{
    internalWriteTo(output, headerSize);
    internalWriteTo(output, width);
    internalWriteTo(output, height);
    internalWriteTo(output, colorPlanes);
    internalWriteTo(output, bitsPerPixel);
    internalWriteTo(output, compression);
    internalWriteTo(output, imageSize);
    internalWriteTo(output, horizontalResolution);
    internalWriteTo(output, verticalResolution);
    internalWriteTo(output, paletteSize);
    internalWriteTo(output, importantColors);
}

///////////////////////////////////////////////////////////////////////////
utils::BMP::BMP(const algorithm::slam::Grid<uint8_t>& grid)
{
    constexpr size_t OFFSET = BmpHeader::size() + DibHeader::size();
    const size_t size = grid.size() * definitions::RGB::size();
    const size_t rowSize = (definitions::RGB::size() * 8 * grid.width() + 31) / 32 * 4;

    bmpHeader.fileSize = static_cast<uint32_t>(OFFSET + rowSize * grid.height());
    bmpHeader.appSpecific1 = 0;
    bmpHeader.appSpecific2 = 0;
    bmpHeader.offsetBitmap = OFFSET;

    dibHeader.width = static_cast<uint32_t>(grid.width());
    dibHeader.height = static_cast<uint32_t>(grid.height());
    dibHeader.colorPlanes = 1;
    dibHeader.bitsPerPixel = static_cast<uint16_t>(definitions::RGB::size() * 8);
    dibHeader.compression = 0;
    dibHeader.imageSize = static_cast<uint32_t>(size);
    dibHeader.horizontalResolution = 2835; // 72 DPI
    dibHeader.verticalResolution = 2835; // 72 DPI
    dibHeader.paletteSize = 0;
    dibHeader.importantColors = 0;

    imageData.resize(grid.size());

    for (size_t y = 0; y < grid.height(); y++)
    {
        const size_t offset = y * grid.width();
        for (size_t x = 0; x < grid.width(); x++)
        {
            imageData[offset + x].red = grid.at(x, y);
            imageData[offset + x].green = grid.at(x, y);
            imageData[offset + x].blue = grid.at(x, y);
        }
    }
}

///////////////////////////////////////////////////////////////////////////
utils::BMP::BMP(const algorithm::slam::Grid<definitions::RGB>& grid)
{
    constexpr size_t OFFSET = BmpHeader::size() + DibHeader::size();
    const size_t size = grid.size() * definitions::RGB::size();
    const size_t rowSize = (definitions::RGB::size() * 8 * grid.width() + 31) / 32 * 4;

    bmpHeader.fileSize = static_cast<uint32_t>(OFFSET + rowSize * grid.height());
    bmpHeader.appSpecific1 = 0;
    bmpHeader.appSpecific2 = 0;
    bmpHeader.offsetBitmap = OFFSET;

    dibHeader.width = static_cast<uint32_t>(grid.width());
    dibHeader.height = static_cast<uint32_t>(grid.height());
    dibHeader.colorPlanes = 1;
    dibHeader.bitsPerPixel = static_cast<uint16_t>(definitions::RGB::size() * 8);
    dibHeader.compression = 0;
    dibHeader.imageSize = static_cast<uint32_t>(size);
    dibHeader.horizontalResolution = 2835; // 72 DPI
    dibHeader.verticalResolution = 2835; // 72 DPI
    dibHeader.paletteSize = 0;
    dibHeader.importantColors = 0;

    imageData.resize(grid.size());

    for (size_t y = 0; y < grid.height(); y++)
    {
        const size_t offset = y * grid.width();
        for (size_t x = 0; x < grid.width(); x++)
            imageData[offset + x] = grid.at(x, y);
    }
}

///////////////////////////////////////////////////////////////////////////
utils::BMP::BMP(const algorithm::slam::Grid<definitions::RGBA>& grid)
{
    constexpr size_t OFFSET = BmpHeader::size() + DibHeader::size();
    const size_t size = grid.size() * definitions::RGB::size();
    const size_t rowSize = (definitions::RGB::size() * 8 * grid.width() + 31) / 32 * 4;

    bmpHeader.fileSize = static_cast<uint32_t>(OFFSET + rowSize * grid.height());
    bmpHeader.appSpecific1 = 0;
    bmpHeader.appSpecific2 = 0;
    bmpHeader.offsetBitmap = OFFSET;

    dibHeader.width = static_cast<uint32_t>(grid.width());
    dibHeader.height = static_cast<uint32_t>(grid.height());
    dibHeader.colorPlanes = 1;
    dibHeader.bitsPerPixel = static_cast<uint16_t>(definitions::RGB::size() * 8);
    dibHeader.compression = 0;
    dibHeader.imageSize = static_cast<uint32_t>(size);
    dibHeader.horizontalResolution = 2835; // 72 DPI
    dibHeader.verticalResolution = 2835; // 72 DPI
    dibHeader.paletteSize = 0;
    dibHeader.importantColors = 0;

    imageData.resize(grid.size());

    for (size_t y = 0; y < grid.height(); y++)
    {
        const size_t offset = y * grid.width();
        for (size_t x = 0; x < grid.width(); x++)
        {
            const size_t finalOffset = offset + x;
            imageData[finalOffset].red = grid.at(x, y).red;
            imageData[finalOffset].green = grid.at(x, y).green;
            imageData[finalOffset].blue = grid.at(x, y).blue;
        }
    }
}

///////////////////////////////////////////////////////////////////////////
utils::BMP::~BMP()
{
}

///////////////////////////////////////////////////////////////////////////
const utils::BmpHeader& utils::BMP::getBmpHeader() const
{
    return bmpHeader;
}

///////////////////////////////////////////////////////////////////////////
const utils::DibHeader& utils::BMP::getDibHeader() const
{
    return dibHeader;
}

///////////////////////////////////////////////////////////////////////////
const std::vector<utils::definitions::RGB>& utils::BMP::getImageData() const
{
    return imageData;
}

void utils::BMP::setResolution(uint32_t resolution)
{
    dibHeader.horizontalResolution = resolution;
    dibHeader.verticalResolution = resolution;
}

///////////////////////////////////////////////////////////////////////////
void utils::BMP::save(const std::filesystem::path& bmp) const
{
    if (bmp.extension() != ".bmp")
        throw std::runtime_error{"[BMP] BMP file should have a .BMP extension, file: " + bmp.string()};

    if (std::filesystem::exists(bmp))
        std::filesystem::remove(bmp);

    std::ofstream bmpFile{bmp.string(), std::ios_base::binary};
    if (!bmpFile.is_open())
        throw std::runtime_error{"[BMP] Could not open file for saving image, file: " + bmp.string()};

    bmpHeader.writeTo(bmpFile);
    dibHeader.writeTo(bmpFile);
    const size_t padding = (8 * definitions::RGB::size() * dibHeader.width + 31) / 32 * 4 - dibHeader.width * definitions::RGB::size();
    const uint8_t paddingBuffer[] = {0, 0, 0, 0};
    for (size_t y = 0; y < dibHeader.height; y++)
    {
        const size_t offset = y * dibHeader.width;
        for (size_t x = 0; x < dibHeader.width; x++)
            writeTo(bmpFile, imageData[offset + x]);
        bmpFile.write(reinterpret_cast<const char*>(paddingBuffer), padding);
    }

    bmpFile.close();
}