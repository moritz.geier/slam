#pragma once

// vulkan
#include <vulkan/vulkan.hpp>

class LogicalDevice;

///////////////////////////////////////////////////////////////////////////
class DescriptorPool final
{
public:
    DescriptorPool(const LogicalDevice& device);
    DescriptorPool(DescriptorPool&&);
    DescriptorPool& operator=(DescriptorPool&&);
    DescriptorPool(const DescriptorPool&) = delete;
    DescriptorPool& operator=(const DescriptorPool&) = delete;
    ~DescriptorPool();

    const vk::DescriptorPool& get() const;
    vk::DescriptorPool& get();

private:
    vk::UniqueDescriptorPool descriptorPool;
};
