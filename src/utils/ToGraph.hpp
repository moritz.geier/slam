#pragma once

// math
#include <math/Graph.hpp>

///////////////////////////////////////////////////////////////////////////
namespace utils
{
    template<typename E, typename N>
    math::Graph<N, E> toGraph(const std::vector<N>& nodes)
    {
        if(nodes.empty())
            return {};

        math::Graph<N, E> graph;
        auto* pLastNode = &graph.addNode(nodes.front());
        for (auto pNode = nodes.begin() + 1; pNode != nodes.end(); pNode++)
        {
            auto& currentNode = graph.addNode(*pNode);
            graph.addEdge({}, currentNode, *pLastNode);
            pLastNode = &currentNode;
        }
        return graph;
    }
}