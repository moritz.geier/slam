#pragma once

// std
#include <algorithm>
#include <memory>
#include <ranges>

#include "IScanMatching.hpp"
#include "../nearest_neighbor/INearestNeighbor.hpp"

///////////////////////////////////////////////////////////////////////////
namespace algorithm
{
    ///////////////////////////////////////////////////////////////////////////
    template<utils::concepts::Timestep StepType>
    class Icp : public IScanMatching<StepType>
    {
    public:
        struct Configuration
        {
            std::unique_ptr<INearestNeighbors<DefaultSearch<IScanMatching<StepType>::DIMENSIONS>>> pNearestNeighborAlgorithm;
            float threshold;
        };

        Icp(Configuration& configuration);
        virtual ~Icp();

        IScanMatching<StepType>::AlignmentResult findAlignment(IScanMatching<StepType>::PointcloudType pointsToMatch, const IScanMatching<StepType>::PointcloudType& referencePoints, const IScanMatching<StepType>::TransformationType& initialAlignment) override;

    protected:
        using ConstPointcloudPointerType = std::vector<const Eigen::Vector<float, IScanMatching<StepType>::DIMENSIONS>*>;
        using PointcloudPointerType = std::vector<Eigen::Vector<float, IScanMatching<StepType>::DIMENSIONS>*>;
        std::unique_ptr<INearestNeighbors<DefaultSearch<IScanMatching<StepType>::DIMENSIONS>>> pNearestNeighborAlgorithm;
        float threshold;

        inline float nearestNeighborHelper(ConstPointcloudPointerType& nearestReferencePoints, PointcloudPointerType& validCurrentPoints, PointcloudPointerType& onlyTransformPoints, IScanMatching<StepType>::PointcloudType& currentPoints);
        inline virtual IScanMatching<StepType>::TransformationType icpLogicHelper(PointcloudPointerType& onlyTransform, PointcloudPointerType& currentPoints, const ConstPointcloudPointerType& referencePoints);
    };
}

///////////////////////////////////////////////////////////////////////////
template<utils::concepts::Timestep StepType>
algorithm::Icp<StepType>::Icp(Configuration& configuration)
: pNearestNeighborAlgorithm{std::move(configuration.pNearestNeighborAlgorithm)}
, threshold{configuration.threshold}
{
}

///////////////////////////////////////////////////////////////////////////
template<utils::concepts::Timestep StepType>
algorithm::Icp<StepType>::~Icp()
{
}

///////////////////////////////////////////////////////////////////////////
template<utils::concepts::Timestep StepType>
algorithm::IScanMatching<StepType>::AlignmentResult algorithm::Icp<StepType>::findAlignment(IScanMatching<StepType>::PointcloudType pointsToMatch, const IScanMatching<StepType>::PointcloudType& referencePoints, const IScanMatching<StepType>::TransformationType& initialAlignment)
{
    ConstPointcloudPointerType nearestReferencePoints;
    PointcloudPointerType validCurrentPoints;
    PointcloudPointerType onlyTransformPoints;

    nearestReferencePoints.reserve(pointsToMatch.size());
    validCurrentPoints.reserve(pointsToMatch.size());
    onlyTransformPoints.reserve(pointsToMatch.size());

    auto referencePointsView = referencePoints
        | std::views::transform([](const auto& point) { return DefaultSearch<IScanMatching<StepType>::DIMENSIONS>{point}; });
    pNearestNeighborAlgorithm->set({referencePointsView.begin(), referencePointsView.end()});

    std::transform(pointsToMatch.begin(), pointsToMatch.end(), pointsToMatch.begin(), [&initialAlignment](const auto& point) { return initialAlignment * point; });

    typename IScanMatching<StepType>::AlignmentResult result{};
    result.transformMatrix.setIdentity();
    result.error = nearestNeighborHelper(nearestReferencePoints, validCurrentPoints, onlyTransformPoints, pointsToMatch);
    float lastError = std::numeric_limits<float>().infinity();

    for (size_t i = 0; i < 20 && lastError - result.error > 1e-5; i++)
    {
        lastError = result.error;
        result.transformMatrix = icpLogicHelper(onlyTransformPoints, validCurrentPoints, nearestReferencePoints) * result.transformMatrix;
        result.error = nearestNeighborHelper(nearestReferencePoints, validCurrentPoints, onlyTransformPoints, pointsToMatch);
    }

    result.transformMatrix = initialAlignment * result.transformMatrix;
    return result;
}

///////////////////////////////////////////////////////////////////////////
template<utils::concepts::Timestep StepType>
float algorithm::Icp<StepType>::nearestNeighborHelper(ConstPointcloudPointerType& nearestReferencePoints, PointcloudPointerType& validCurrentPoints, PointcloudPointerType& onlyTransformPoints, IScanMatching<StepType>::PointcloudType& currentPoints)
{
    float error = 0;

    nearestReferencePoints.clear();
    validCurrentPoints.clear();
    onlyTransformPoints.clear();

    for (auto& point : currentPoints)
    {
        const auto result = pNearestNeighborAlgorithm->find(point);

        if (result.distance < threshold)
        {
            nearestReferencePoints.push_back(&result.pNeighbor->coordinate);
            validCurrentPoints.push_back(&point);
        }
        else
        {
            onlyTransformPoints.push_back(&point);
        }

        error += result.distance;
    }

    return error / currentPoints.size();
}

///////////////////////////////////////////////////////////////////////////
template<utils::concepts::Timestep StepType>
algorithm::IScanMatching<StepType>::TransformationType algorithm::Icp<StepType>::icpLogicHelper(PointcloudPointerType& onlyTransform, PointcloudPointerType& currentPoints, const ConstPointcloudPointerType& referencePoints)
{
    Eigen::Vector<float, IScanMatching<StepType>::DIMENSIONS> currentPointsMedian;
    currentPointsMedian.setZero();
    for (const auto* pPoint : currentPoints)
        currentPointsMedian += *pPoint;
    currentPointsMedian /= static_cast<float>(currentPoints.size());

    Eigen::Vector<float, IScanMatching<StepType>::DIMENSIONS> referencePointsMedian;
    referencePointsMedian.setZero();
    for (const auto* pPoint : referencePoints)
        referencePointsMedian += *pPoint;
    referencePointsMedian /= static_cast<float>(referencePoints.size());

    Eigen::Matrix<float, IScanMatching<StepType>::DIMENSIONS, IScanMatching<StepType>::DIMENSIONS> crossCorrelationMatrix;
    crossCorrelationMatrix.setZero();
    for (size_t i = 0; i < currentPoints.size(); i++)
    {
        const auto referencePointDelta = (*referencePoints[i] - referencePointsMedian);
        crossCorrelationMatrix += (*currentPoints[i] - currentPointsMedian) * referencePointDelta.transpose();
    }

    Eigen::JacobiSVD<Eigen::Matrix<float, IScanMatching<StepType>::DIMENSIONS, IScanMatching<StepType>::DIMENSIONS>> decomposingCrossCorrelationMatrix{crossCorrelationMatrix, Eigen::ComputeFullU | Eigen::ComputeFullV};
    const auto svdUMatrix = decomposingCrossCorrelationMatrix.matrixU();
    const auto svdVMatrix = decomposingCrossCorrelationMatrix.matrixV();
    const Eigen::Matrix<float, IScanMatching<StepType>::DIMENSIONS, IScanMatching<StepType>::DIMENSIONS> rotationMatrix = svdVMatrix * svdUMatrix.transpose();

    for (auto* pPoint : currentPoints)
        *pPoint = rotationMatrix * (*pPoint - currentPointsMedian) + referencePointsMedian;

    for (auto* pPoint : onlyTransform)
        *pPoint = rotationMatrix * (*pPoint - currentPointsMedian) + referencePointsMedian;

    const Eigen::Vector<float, IScanMatching<StepType>::DIMENSIONS> translationVector = referencePointsMedian - rotationMatrix * currentPointsMedian;

    typename IScanMatching<StepType>::TransformationType transformationMatrix;
    transformationMatrix.translation() = translationVector;
    transformationMatrix.linear() = rotationMatrix;
    return transformationMatrix;
}