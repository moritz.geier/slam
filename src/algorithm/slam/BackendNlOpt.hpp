#pragma once

// nlopt
#include <nlopt.hpp>

#include "SlamTypes.hpp"

namespace algorithm::slam
{
    ///////////////////////////////////////////////////////////////////////////
    template<utils::concepts::Timestep StepType>
    void backendNlOpt(SlamGraph<StepType>& graph, const SolveConfiguration<StepType>& config)
    {
        if (config.runBackend)
            return true;

        nlopt::opt opt{nlopt::LD_MMA, 3};
        (void)opt;
    }
}