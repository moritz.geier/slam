#include "CommandBuffer.hpp"

// imgui
#include <imgui.h>
#include <imgui_impl_vulkan.h>

// vulkan
#include <vulkan_backend/CommandPool.hpp>
#include <vulkan_backend/Framebuffer.hpp>
#include <vulkan_backend/LogicalDevice.hpp>
#include <vulkan_backend/RenderPass.hpp>
#include <vulkan_backend/Swapchain.hpp>

///////////////////////////////////////////////////////////////////////////
ui::CommandBuffer::CommandBuffer(vk::UniqueCommandBuffer&& buffer)
: ::CommandBuffer{std::move(buffer)}
{
}

///////////////////////////////////////////////////////////////////////////
ui::CommandBuffer::CommandBuffer(const vk::CommandBuffer& buffer)
: ::CommandBuffer{std::move(buffer)}
{
}

///////////////////////////////////////////////////////////////////////////
ui::CommandBuffer::CommandBuffer(CommandBuffer&& moveCommandBuffer)
: ::CommandBuffer{std::move(moveCommandBuffer)}
{
}

///////////////////////////////////////////////////////////////////////////
ui::CommandBuffer& ui::CommandBuffer::operator=(CommandBuffer&& moveCommandBuffer)
{
    if (&moveCommandBuffer != this)
        commandBuffer = std::move(moveCommandBuffer.commandBuffer);
    return *this;
}

///////////////////////////////////////////////////////////////////////////
ui::CommandBuffer::~CommandBuffer()
{
}

///////////////////////////////////////////////////////////////////////////
std::vector<ui::CommandBuffer> ui::CommandBuffer::allocate(const LogicalDevice& device, const CommandPool& commandPool, size_t count)
{
    vk::CommandBufferAllocateInfo allocateInfo{};
    allocateInfo.sType = vk::StructureType::eCommandBufferAllocateInfo;
    allocateInfo.commandPool  = commandPool.get();
    allocateInfo.level = vk::CommandBufferLevel::ePrimary;
    allocateInfo.commandBufferCount = count;

    auto vulkanBuffers = device->allocateCommandBuffersUnique(allocateInfo);

    std::vector<CommandBuffer> buffers;
    buffers.reserve(vulkanBuffers.size());
    for (auto& vulkanBuffer : vulkanBuffers)
        buffers.emplace_back(std::move(vulkanBuffer));

    return buffers;
}

///////////////////////////////////////////////////////////////////////////
void ui::CommandBuffer::record(const RenderPass& renderPass, const Framebuffer& framebuffer, const Swapchain& swapchain, std::array<float, 4> clearColor)
{
    const auto& extent = swapchain.getExtent();

    commandBuffer->reset();

    vk::CommandBufferBeginInfo beginInfo{};
    beginInfo.sType = vk::StructureType::eCommandBufferBeginInfo;

    commandBuffer->begin(beginInfo);

    const vk::ClearValue clearValue{{clearColor}};
    vk::RenderPassBeginInfo renderPassInfo{};
    renderPassInfo.sType = vk::StructureType::eRenderPassBeginInfo;
    renderPassInfo.renderPass = renderPass.get();
    renderPassInfo.framebuffer = framebuffer.get();
    renderPassInfo.renderArea.offset = vk::Offset2D{0, 0};
    renderPassInfo.renderArea.extent = extent;
    renderPassInfo.clearValueCount = 1;
    renderPassInfo.pClearValues = &clearValue;

    commandBuffer->beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);

    // Record dear imgui primitives into command buffer
    ImDrawData* drawData = ImGui::GetDrawData();
    ImGui_ImplVulkan_RenderDrawData(drawData, commandBuffer.get());

    commandBuffer->endRenderPass();
    commandBuffer->end();
}