#pragma once

// std
#include <fstream>
#include <string>

namespace utils::file
{
    /// @brief Reads the whole content of a file into a string.
    /// @param file File to be read.
    /// @param chunkSize Amount of characters that a read at once.
    /// @return A string containing all the content of the file.
    std::string readAll(std::ifstream& file, size_t chunkSize = 4096);
}