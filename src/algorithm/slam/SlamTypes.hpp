#pragma once

// algorithm
#include <algorithm/scan_matching/IScanMatching.hpp>

// math
#include <math/Graph.hpp>


// std
#include <functional>

// utils
#include <utils/Definitions.hpp>

///////////////////////////////////////////////////////////////////////////
namespace algorithm::slam
{
    ///////////////////////////////////////////////////////////////////////////
    enum class MeasurementType : uint8_t
    {
        eOdometry,
        eLoopClosure
    };

    ///////////////////////////////////////////////////////////////////////////
    struct EdgeValue
    {
        MeasurementType measurement;
        Eigen::AffineCompact2f relativeTransformation;
        size_t from;
        float rmse;
    };

    ///////////////////////////////////////////////////////////////////////////
    template<utils::concepts::Timestep StepType>
    using SlamGraph = math::Graph<StepType, EdgeValue>;

    ///////////////////////////////////////////////////////////////////////////
    template<utils::concepts::Timestep StepType>
    struct SolveConfiguration
    {
        std::function<SlamGraph<StepType>(const SlamGraph<StepType>&, const SolveConfiguration<StepType>&, float&)> frontend;
        std::function<void(SlamGraph<StepType>&, const SolveConfiguration<StepType>&)> backend;
        std::unique_ptr<::algorithm::IScanMatching<StepType>> pScanMatcher;
        size_t loopClosureAt;
        float loopClosureRadius;
        float loopClosureAcceptanceThreshold;
        bool runScanMatchingForOdometry;
        bool runLoopClosure;
        bool runBackend;
    };

    struct LoopClosureSearchValue
    {
        using const_iterator = const float*;

        Eigen::Vector2f coordinate;
        size_t id;

        size_t size() const
        {
            return coordinate.size();
        }
        
        const_iterator begin() const
        {
            return coordinate.data();
        }
        
        const_iterator end() const
        {
            return coordinate.data() + coordinate.size();
        }
    };

    ///////////////////////////////////////////////////////////////////////////
    struct LoopClosureSearchType
    {
        using Value = LoopClosureSearchValue;
        using Distance = float;
        using Size = size_t;

        Distance operator()(const Value& lhs, const Value& rhs) const
        {
            return (lhs.coordinate - rhs.coordinate).norm();
        }

        Distance operator()(const Distance& lhs, const Distance& rhs, const ::std::size_t& /*index*/) const
        {
            return std::abs(lhs - rhs);
        }
    };
}