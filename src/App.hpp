#pragma once

// render
#include <render_backend/Renderer.hpp>

// std
#include <stdint.h>
#include <string>
#include <vector>

// ui
#include <ui_backend/Gui.hpp>

// vulkan
#include <vulkan_backend/CommandPool.hpp>
#include <vulkan_backend/DescriptorPool.hpp>
#include <vulkan_backend/Fence.hpp>
#include <vulkan_backend/Instance.hpp>
#include <vulkan_backend/LogicalDevice.hpp>
#include <vulkan_backend/PhysicalDevice.hpp>
#include <vulkan_backend/Sampler.hpp>
#include <vulkan_backend/Semaphore.hpp>
#include <vulkan_backend/Surface.hpp>
#include <vulkan_backend/Window.hpp>

class App
{
public:
    App();
    ~App();
    void run();

private:
    struct Syncronisation
    {
        Fence inFlight;
        Semaphore imageAvailable;
        Semaphore renderFinished;

        Syncronisation(const LogicalDevice& device);
    };

    const std::string name = "SLAM Visulizer";
    const uint32_t height = 1280;
    const uint32_t width = 800;
    vk::Extent2D windowSize = {width, height};
    const size_t framesInFlight = 2;
    size_t currentFrame;
    bool shouldResize;

    Window window;
    Instance instance;
    Surface surface;
    PhysicalDevice physicalDevice;
    LogicalDevice logicalDevice;
    CommandPool commandPool;
    DescriptorPool descriptorPool;
    Sampler sampler;
    std::vector<Syncronisation> synchronisation;

    Gui gui;
    Renderer renderer;

    void recreateSwapchain();
};