#pragma once

// vulkan
#include <vulkan/vulkan.hpp>

class LogicalDevice;

///////////////////////////////////////////////////////////////////////////
class Semaphore
{
public:
    Semaphore(const LogicalDevice& logicalDevice);
    Semaphore(Semaphore&&);
    Semaphore& operator=(Semaphore&&);
    Semaphore(const Semaphore&) = delete;
    Semaphore& operator=(const Semaphore&) = delete;
    ~Semaphore();

    const vk::Semaphore& get() const;
    vk::Semaphore& get();

private:
    vk::UniqueSemaphore semaphore;
};
