#pragma once

// std
#include <filesystem>

// vulkan
#include <vulkan/vulkan.hpp>

class LogicalDevice;

class Shader final
{
public:
    Shader(const LogicalDevice& device, const std::filesystem::path& shader);
    Shader(Shader&&);
    Shader& operator=(Shader&&);
    Shader(const Shader&) = delete;
    Shader& operator=(const Shader&) = delete;
    ~Shader();

    const vk::ShaderModule& get() const;
    vk::ShaderModule& get();

    vk::ShaderStageFlagBits getType() const;

private:
    vk::UniqueShaderModule shaderModule;
    vk::ShaderStageFlagBits type;
};