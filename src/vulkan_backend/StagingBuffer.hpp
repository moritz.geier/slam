#pragma once

// std
#include <concepts>
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

#include "Buffer.hpp"
#include "LogicalDevice.hpp"
#include "PhysicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
template<class Object>
class StagingBuffer final : public Buffer<Object>
{
public:
    StagingBuffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t size);
    StagingBuffer(StagingBuffer<Object>&&);
    StagingBuffer<Object>& operator=(StagingBuffer<Object>&&);
    StagingBuffer(const StagingBuffer<Object>&) = delete;
    StagingBuffer<Object>& operator=(const StagingBuffer<Object>&) = delete;
    ~StagingBuffer() override;
};

///////////////////////////////////////////////////////////////////////////
template<class Object>
StagingBuffer<Object>::StagingBuffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t size)
: Buffer<Object>{logicalDevice, physicalDevice, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, size}
{
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
StagingBuffer<Object>::StagingBuffer(StagingBuffer<Object>&& moveBuffer)
: Buffer<Object>{std::move(moveBuffer)}
{
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
StagingBuffer<Object>& StagingBuffer<Object>::operator=(StagingBuffer<Object>&& moveBuffer)
{
    return Buffer<Object>::operator=(std::move(moveBuffer));
}

///////////////////////////////////////////////////////////////////////////
template<class Object>
StagingBuffer<Object>::~StagingBuffer()
{
}