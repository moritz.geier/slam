// algorithm
#include <algorithm/nearest_neighbor/KdTree.hpp>
#include <algorithm/scan_matching/Icp.hpp>
#include <algorithm/scan_matching/Open3dIcp.hpp>
#include <algorithm/scan_matching/PclNdt.hpp>
#include <algorithm/slam/Slam.hpp>
#include <algorithm/slam/FrontendSimple.hpp>
#include <algorithm/slam/BackendLs.hpp>

// cmake
#include <CMakeVariables.hpp>

// std
#include <chrono>
#include <format>
#include <fstream>
#include <iostream>
#include <string>

// utils
#include <utils/LogReader.hpp>
#include <utils/RelationsReader.hpp>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    int parse(int argc, char** argv, int index, int init)
    {
        if (index < argc)
        {
            try
            {
                init = std::stoi(std::string{argv[index]});
            }
            catch(std::exception&)
            {
                throw std::runtime_error{"Usage: \n\t benchmark [scanmatcher_id] [odometry] [loop_closure] [backend]\n- scanmatcher_id: int\n- odometry: [0, 1]\n- loop_closure: [0, 1]\n- backend: [0, 1]\n"};
            }
        }

        return init;
    }
}

///////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
    int scanMatcher = 2;
    int odometry = 1;
    int loopClosure = 0;
    int backend = 0;

    try
    {
        scanMatcher = parse(argc, argv, 1, scanMatcher);
        odometry = parse(argc, argv, 2, odometry);
        loopClosure = parse(argc, argv, 3, loopClosure);
        backend = parse(argc, argv, 4, backend);
    }
    catch(const std::exception& e)
    {
        std::cout << e.what();
        return -1;
    }
    

    algorithm::slam::SolveConfiguration<utils::definitions::Timestep> solveConfig;
    solveConfig.backend = algorithm::slam::backendLs<utils::definitions::Timestep>;
    solveConfig.frontend = algorithm::slam::frontendSimple<utils::definitions::Timestep>;
    solveConfig.loopClosureAt = 1;
    solveConfig.loopClosureAcceptanceThreshold = 0.0f;
    solveConfig.loopClosureRadius = 2.0f;
    solveConfig.runScanMatchingForOdometry = odometry;
    solveConfig.runLoopClosure = loopClosure;
    solveConfig.runBackend = backend;

    std::string name = "";

    switch (scanMatcher)
    {
        case 0:
        {
            using ScanMatcher = algorithm::Icp<utils::definitions::Timestep>;
            ScanMatcher::Configuration scanMatcherConfig;
            scanMatcherConfig.threshold = 0.5f;
            scanMatcherConfig.pNearestNeighborAlgorithm = std::make_unique<algorithm::KdTree<algorithm::DefaultSearch<2>>>();

            solveConfig.pScanMatcher = std::make_unique<ScanMatcher>(scanMatcherConfig);
            name = "vanilla_icp";
            break;
        }
        case 1:
        {
            using ScanMatcher = algorithm::Open3dIcp;
            ScanMatcher::Configuration scanMatcherConfig;
            scanMatcherConfig.threshold = 0.5f;

            solveConfig.pScanMatcher = std::make_unique<ScanMatcher>(scanMatcherConfig);
            name = "open3d_icp";
            break;
        }
        case 2:
        {
            using ScanMatcher = algorithm::PclNdt;
            ScanMatcher::Configuration scanMatcherConfig;
            scanMatcherConfig.threshold = 0.5f;

            solveConfig.pScanMatcher = std::make_unique<ScanMatcher>(scanMatcherConfig);
            name = "pcl_ndt";
            break;
        }
    }

    if (!solveConfig.runScanMatchingForOdometry && !solveConfig.runLoopClosure && !solveConfig.runBackend)
        name = "baseline";

    float progress = 0;
    const auto relations = utils::RelationsReader::allSteps(CMake::programRootDirectory / "resources" / "aces-building.relations");
    const auto steps = utils::LogReader::allSteps(CMake::programRootDirectory / "resources" / "aces-building.log");
    algorithm::slam::SlamGraph<utils::definitions::Timestep> graph;
    for (const auto& step : steps)
    {
        const auto exists = std::find_if(relations.begin(), relations.end(), [&step](const auto& relation){ return step.id == relation.idOne || step.id == relation.idTwo; });
        if (exists != relations.end())
            graph.addNode(step);
    }

    std::cout << "Starting benchmark...\n";

    const auto begin = std::chrono::steady_clock::now();
    const auto result = algorithm::slam::solve(graph, progress, solveConfig);
    const auto end = std::chrono::steady_clock::now();

    std::cout << "Time to solve: " << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count() << "." << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() % 1000 << "s\n";
    std::cout << "Nodes: " << result.nodeSize() << "\tEdges: " << result.edgeSize() <<  "\tLoop closures: " << (result.edgeSize() <= result.nodeSize() ? 0 : result.edgeSize() - 1 - result.nodeSize()) << "\n";

    Eigen::Vector3f score{Eigen::Vector3f::Zero()};
    const auto& nodes = result.getNodes();
    score.setZero();
    std::ofstream errorsCsv{std::format("errors_{}_{}_{}_{}.csv", name, solveConfig.runScanMatchingForOdometry, solveConfig.runLoopClosure, solveConfig.runBackend)};
    errorsCsv << "node, x, y, theta\n";
    for (const auto& relation : relations)
    {
        const auto firstNode = std::find_if(nodes.begin(), nodes.end(), [&relation](const auto& node) { return node->get().id == relation.idOne; });
        if (firstNode == nodes.end())
            continue;
        const auto secondNode = std::find_if(nodes.begin(), nodes.end(), [&relation](const auto& node) { return node->get().id == relation.idTwo; });
        if (secondNode == nodes.end())
            continue;
        const auto actualTransformation = firstNode->get()->get().pose.inverse() * secondNode->get()->get().pose;
        const auto error = relation.transformation.inverse() * actualTransformation;
        
        const auto rotation = std::atan2(error(1, 0), error(0, 0));
        Eigen::Vector3f currentError;
        currentError(0) = error(0, 2) * error(0, 2);
        currentError(1) = error(1, 2) * error(1, 2);
        currentError(2) = rotation * rotation;
        score += currentError;
        errorsCsv << std::format("{}, {}, {}, {}\n", (firstNode->get()->id() + secondNode->get()->id()) / 2.0f, currentError(0), currentError(1), currentError(2));
    }

    std::cout << std::format("Errors (x^2, y^2, theta^2): ({}, {}, {})\n", score(0), score(1), score(2));
}