#include "CommandPool.hpp"
#include "LogicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
CommandPool::CommandPool(const LogicalDevice& logicalDevice, uint32_t queueFamilyIndex)
{
    vk::CommandPoolCreateInfo poolInfo{};
    poolInfo.sType = vk::StructureType::eCommandPoolCreateInfo;
    poolInfo.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer;
    poolInfo.queueFamilyIndex = queueFamilyIndex;

    commandPool = logicalDevice->createCommandPoolUnique(poolInfo);
}

///////////////////////////////////////////////////////////////////////////
CommandPool::CommandPool(CommandPool&& moveCommandPool)
{
    commandPool = std::move(moveCommandPool.commandPool);
}

///////////////////////////////////////////////////////////////////////////
CommandPool& CommandPool::operator=(CommandPool&& moveCommandPool)
{
    if (&moveCommandPool != this)
        commandPool = std::move(moveCommandPool.commandPool);

    return *this;
}

///////////////////////////////////////////////////////////////////////////
CommandPool::~CommandPool()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::CommandPool& CommandPool::get() const
{
    return commandPool.get();
}

///////////////////////////////////////////////////////////////////////////
vk::CommandPool& CommandPool::get()
{
    return commandPool.get();
}
