// ----------------------------------------------------------------------------
// -                        Open3D: www.open3d.org                            -
// ----------------------------------------------------------------------------
// Copyright (c) 2018-2023 www.open3d.org
// SPDX-License-Identifier: MIT
// ----------------------------------------------------------------------------

#include "open3d/utility/CPUInfo.h"

#include <cassert>
#include <fstream>
#include <memory>
#include <set>
#include <string>
#include <thread>
#include <vector>

#include "open3d/utility/Helper.h"

namespace open3d {
namespace utility {

struct CPUInfo::Impl {
    int num_cores_;
    int num_threads_;
};

/// Returns the number of physical CPU cores.
static int PhysicalConcurrency() {
    return std::thread::hardware_concurrency();
}  // namespace utility

CPUInfo::CPUInfo() : impl_(new CPUInfo::Impl()) {
    impl_->num_cores_ = PhysicalConcurrency();
    impl_->num_threads_ = std::thread::hardware_concurrency();
}

CPUInfo& CPUInfo::GetInstance() {
    static CPUInfo instance;
    return instance;
}

int CPUInfo::NumCores() const { return impl_->num_cores_; }

int CPUInfo::NumThreads() const { return impl_->num_threads_; }

void CPUInfo::Print() const {
}

}  // namespace utility
}  // namespace open3d
