// catch2
#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

// slam
#include <algorithm/nearest_neighbor/KdTree.hpp>
#include <algorithm/nearest_neighbor/BruteForce.hpp>
#include <algorithm/scan_matching/Icp.hpp>

// std
#include <random>

// utils
#include <utils/Definitions.hpp>

namespace
{
    ///////////////////////////////////////////////////////////////////////////
    struct DataPoints
    {
        std::vector<Eigen::Vector2f> points;
        Eigen::AffineCompact2f transformationMatrix;
    };

    ///////////////////////////////////////////////////////////////////////////
    DataPoints createDataPoints(size_t count)
    {
        std::random_device randomDevice;
        std::mt19937 generator{randomDevice()};
        std::uniform_real_distribution<> distribution{-100, 100};

        std::vector<Eigen::Vector2f> referencePoints;
        referencePoints.resize(count);

        Eigen::AffineCompact2f transformationMatrix;
        transformationMatrix(0, 2) = distribution(generator);
        transformationMatrix(1, 2) = distribution(generator);
        transformationMatrix(0, 0) = 0;
        transformationMatrix(1, 0) = -1;
        transformationMatrix(0, 1) = 1;
        transformationMatrix(1, 1) = 0;

        for (auto& entry : referencePoints)
        {
            entry.x() = distribution(generator);
            entry.y() = distribution(generator);
        }

        return {referencePoints, transformationMatrix};
    }
}


///////////////////////////////////////////////////////////////////////////
TEST_CASE("Benchmarking different scan matching algorithm", "[!benchmark][scan_matching]")
{
    algorithm::Icp<utils::definitions::Timestep>::Configuration icpKdTreeConfig;
    icpKdTreeConfig.pNearestNeighborAlgorithm = std::make_unique<algorithm::KdTree<algorithm::DefaultSearch<2>>>();
    icpKdTreeConfig.threshold = 30.0f;

    algorithm::Icp<utils::definitions::Timestep>::Configuration icpBruteForceConfig;
    icpBruteForceConfig.pNearestNeighborAlgorithm = std::make_unique<algorithm::BruteForceNearestNeighbor<algorithm::DefaultSearch<2>>>();
    icpBruteForceConfig.threshold = 30.0f;

    algorithm::Icp<utils::definitions::Timestep> icpKdTree{icpKdTreeConfig};
    algorithm::Icp<utils::definitions::Timestep> icpBruteForce{icpBruteForceConfig};

    {
        const auto data{createDataPoints(10)};

        const auto resultKdTree = icpKdTree.findAlignment(data.points, data.points, data.transformationMatrix);
        const auto resultBruteForce = icpBruteForce.findAlignment(data.points, data.points, data.transformationMatrix);
        CHECK((resultKdTree.error < resultBruteForce.error + 0.01f && resultKdTree.error > resultBruteForce.error - 0.01f));
        CHECK(resultKdTree.transformMatrix.matrix() == resultBruteForce.transformMatrix.matrix());

        BENCHMARK("Icp with KDTree[10]")
        {
            return icpKdTree.findAlignment(data.points, data.points, data.transformationMatrix);
        };

        BENCHMARK("Icp with BruteForce[10]")
        {
            return icpBruteForce.findAlignment(data.points, data.points, data.transformationMatrix);
        };
    }

    {
        const auto data{createDataPoints(180)};

        const auto resultKdTree = icpKdTree.findAlignment(data.points, data.points, data.transformationMatrix);
        const auto resultBruteForce = icpBruteForce.findAlignment(data.points, data.points, data.transformationMatrix);
        CHECK((resultKdTree.error < resultBruteForce.error + 0.01f && resultKdTree.error > resultBruteForce.error - 0.01f));
        CHECK(resultKdTree.transformMatrix.matrix() == resultBruteForce.transformMatrix.matrix());

        BENCHMARK("Icp with KDTree[180]")
        {
            return icpKdTree.findAlignment(data.points, data.points, data.transformationMatrix);
        };

        BENCHMARK("Icp with BruteForce[180]")
        {
            return icpBruteForce.findAlignment(data.points, data.points, data.transformationMatrix);
        };
    }

    {
        const auto data{createDataPoints(1'000)};

        const auto resultKdTree = icpKdTree.findAlignment(data.points, data.points, data.transformationMatrix);
        const auto resultBruteForce = icpBruteForce.findAlignment(data.points, data.points, data.transformationMatrix);
        CHECK((resultKdTree.error < resultBruteForce.error + 0.01f && resultKdTree.error > resultBruteForce.error - 0.01f));
        CHECK(resultKdTree.transformMatrix.matrix() == resultBruteForce.transformMatrix.matrix());

        BENCHMARK("Icp with KDTree[1k]")
        {
            return icpKdTree.findAlignment(data.points, data.points, data.transformationMatrix);
        };

        BENCHMARK("Icp with BruteForce[1k]")
        {
            return icpBruteForce.findAlignment(data.points, data.points, data.transformationMatrix);
        };
    }
}