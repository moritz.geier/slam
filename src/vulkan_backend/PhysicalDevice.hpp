#pragma once

// std
#include <optional>
#include <set>
#include <unordered_set>
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

class Instance;
class Surface;


///////////////////////////////////////////////////////////////////////////
struct QueueFamilyIndices
{
    std::optional<uint32_t> graphics;
    std::optional<uint32_t> presentation;

    QueueFamilyIndices();

    bool isComplete() const;
    std::set<uint32_t> toSet() const;
};

///////////////////////////////////////////////////////////////////////////
struct SwapchainSupportDetails
{
    vk::SurfaceCapabilitiesKHR capabilities;
    std::vector<vk::SurfaceFormatKHR> formats;
    std::vector<vk::PresentModeKHR> presentModes;

    bool isAdequate() const;
};


///////////////////////////////////////////////////////////////////////////
class PhysicalDevice final
{
public:
    using Extensions = std::unordered_set<std::string>;

    PhysicalDevice(const Instance& instance, const Surface& surface, const Extensions& extensions);
    PhysicalDevice(PhysicalDevice&&);
    PhysicalDevice& operator=(PhysicalDevice&&);
    PhysicalDevice(const PhysicalDevice&) = delete;
    PhysicalDevice& operator=(const PhysicalDevice&) = delete;
    ~PhysicalDevice();

    const vk::PhysicalDevice& get() const;
    vk::PhysicalDevice& get();

    const vk::PhysicalDevice* operator->() const;
    vk::PhysicalDevice* operator->();

    QueueFamilyIndices getQueueFamilies(const Surface& surface) const;
    SwapchainSupportDetails getSwapchainSupport(const Surface& surface) const;
    uint32_t getMemoryTypeIndex(uint32_t filter, vk::MemoryPropertyFlags propertyToSelect) const;

private:
    vk::PhysicalDevice device;

    QueueFamilyIndices getQueueFamilies(const vk::PhysicalDevice& device, const Surface& surface) const;
    SwapchainSupportDetails getSwapchainSupport(const vk::PhysicalDevice& device, const Surface& surface) const;
    int scoreDevice(const vk::PhysicalDevice& device, const Surface& surface, const Extensions& extensions) const;
    bool areExtensionsSupported(const vk::PhysicalDevice& device, const Extensions& extensions) const;
};