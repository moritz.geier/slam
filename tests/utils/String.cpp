// catch2
#include <catch2/catch_test_macros.hpp>

// std
#include <string>
#include <vector>

// utils
#include <utils/String.hpp>

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Splitting a string with a delimiter.", "[utils::string]" )
{
    SECTION("Splitting empty string.")
    {
        const auto result = utils::string::split("", "");
        REQUIRE(result.empty());
    }

    const std::string testString = "Testing string splitting. Wish you good luck!";
    SECTION("Splitting string with empty delimiter.")
    {
        const auto result = utils::string::split(testString, "");
        REQUIRE(result.size() == 1);
        REQUIRE(result[0] == testString);
    }

    SECTION("Splitting string with a character as delimiter.")
    {
        const auto result = utils::string::split(testString, " ");
        REQUIRE(result.size() == 7);
        REQUIRE(result[0] == testString.substr(0, testString.find(" ")));
        REQUIRE(result[2] == "splitting.");
        REQUIRE(result[6] == testString.substr(testString.find_last_of(" ") + 1));
    }

    SECTION("Splitting string with a string as delimiter.")
    {
        const auto result = utils::string::split(testString, "Wish");
        REQUIRE(result.size() == 2);
        REQUIRE(result[0] == testString.substr(0, testString.find("Wish")));
        REQUIRE(result[1] == testString.substr(testString.find("Wish") + 4));
    }
}