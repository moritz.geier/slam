#include "DescriptorSetLayout.hpp"
#include "LogicalDevice.hpp"

// std
#include <vector>

///////////////////////////////////////////////////////////////////////////
DescriptorSetLayout::DescriptorSetLayout(const LogicalDevice& logicalDevice)
{
    vk::DescriptorSetLayoutBinding uniformBufferLayout{};
    uniformBufferLayout.binding = 0;
    uniformBufferLayout.descriptorType = vk::DescriptorType::eUniformBuffer;
    uniformBufferLayout.descriptorCount = 1;
    uniformBufferLayout.stageFlags = vk::ShaderStageFlagBits::eVertex;
    uniformBufferLayout.pImmutableSamplers = nullptr;

    vk::DescriptorSetLayoutBinding samplerLayout{};
    samplerLayout.binding = 1;
    samplerLayout.descriptorType = vk::DescriptorType::eCombinedImageSampler;
    samplerLayout.descriptorCount = 1;
    samplerLayout.stageFlags = vk::ShaderStageFlagBits::eFragment;
    samplerLayout.pImmutableSamplers = nullptr;

    std::vector<vk::DescriptorSetLayoutBinding> layoutBindings{uniformBufferLayout, samplerLayout};

    vk::DescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = createInfo.structureType;
    createInfo.bindingCount = layoutBindings.size();
    createInfo.pBindings = layoutBindings.data();

    descriptorSetLayout = logicalDevice->createDescriptorSetLayoutUnique(createInfo);
}

///////////////////////////////////////////////////////////////////////////
DescriptorSetLayout::DescriptorSetLayout(DescriptorSetLayout&& moveCommandPool)
{
    descriptorSetLayout = std::move(moveCommandPool.descriptorSetLayout);
}

///////////////////////////////////////////////////////////////////////////
DescriptorSetLayout& DescriptorSetLayout::operator=(DescriptorSetLayout&& moveCommandPool)
{
    if (&moveCommandPool != this)
        descriptorSetLayout = std::move(moveCommandPool.descriptorSetLayout);
    return *this;
}

///////////////////////////////////////////////////////////////////////////
DescriptorSetLayout::~DescriptorSetLayout()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::DescriptorSetLayout& DescriptorSetLayout::get() const
{
    return descriptorSetLayout.get();
}

///////////////////////////////////////////////////////////////////////////
vk::DescriptorSetLayout& DescriptorSetLayout::get()
{
    return descriptorSetLayout.get();
}
