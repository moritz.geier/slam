#include "PhysicalDevice.hpp"
#include "Instance.hpp"
#include "Surface.hpp"

// std
#include <map>

///////////////////////////////////////////////////////////////////////////
QueueFamilyIndices::QueueFamilyIndices()
{
    graphics = std::nullopt;
    presentation = std::nullopt;
}

///////////////////////////////////////////////////////////////////////////
bool QueueFamilyIndices::isComplete() const
{
    return graphics.has_value() && presentation.has_value();
}

///////////////////////////////////////////////////////////////////////////
std::set<uint32_t> QueueFamilyIndices::toSet() const
{
    if (isComplete())
        return {graphics.value(), presentation.value()};
    return {};
}

///////////////////////////////////////////////////////////////////////////
bool SwapchainSupportDetails::isAdequate() const
{
    return !(formats.empty() && presentModes.empty());
}

///////////////////////////////////////////////////////////////////////////
PhysicalDevice::PhysicalDevice(const Instance& instance, const Surface& surface, const Extensions& extensions)
{
    auto devices = instance->enumeratePhysicalDevices();
    std::multimap<int, vk::PhysicalDevice> deviceScores;

    for (const auto& device : devices)
    {
        int currentScore = scoreDevice(device, surface, extensions);

        if (currentScore >= 0)
            deviceScores.emplace(currentScore, device);
    }

    if (deviceScores.empty())
        throw std::runtime_error{"[PhysicalDevice] No suitable GPU could be found!"};

    device = deviceScores.rbegin()->second;
}

///////////////////////////////////////////////////////////////////////////
PhysicalDevice::PhysicalDevice(PhysicalDevice&& movePhysicalDevice)
{
    device = std::move(movePhysicalDevice.device);
}

///////////////////////////////////////////////////////////////////////////
PhysicalDevice& PhysicalDevice::operator=(PhysicalDevice&& movePhysicalDevice)
{
    if (&movePhysicalDevice != this)
        device = std::move(movePhysicalDevice.device);

    return *this;
}

///////////////////////////////////////////////////////////////////////////
PhysicalDevice::~PhysicalDevice()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::PhysicalDevice& PhysicalDevice::get() const
{
    return device;
}

///////////////////////////////////////////////////////////////////////////
vk::PhysicalDevice& PhysicalDevice::get()
{
    return device;
}

///////////////////////////////////////////////////////////////////////////
const vk::PhysicalDevice* PhysicalDevice::operator->() const
{
    return &device;
}

///////////////////////////////////////////////////////////////////////////
vk::PhysicalDevice* PhysicalDevice::operator->()
{
    return &device;
}

///////////////////////////////////////////////////////////////////////////
QueueFamilyIndices PhysicalDevice::getQueueFamilies(const Surface& surface) const
{
    return getQueueFamilies(device, surface);
}

///////////////////////////////////////////////////////////////////////////
SwapchainSupportDetails PhysicalDevice::getSwapchainSupport(const Surface& surface) const
{
    return getSwapchainSupport(device, surface);
}

///////////////////////////////////////////////////////////////////////////
QueueFamilyIndices PhysicalDevice::getQueueFamilies(const vk::PhysicalDevice& device, const Surface& surface) const
{
    QueueFamilyIndices indices{};

    auto properties = device.getQueueFamilyProperties();
    for (size_t i = 0; i < properties.size(); i++)
    {
        if (properties[i].queueFlags & vk::QueueFlagBits::eGraphics && !indices.graphics.has_value())
            indices.graphics = i;

        if (device.getSurfaceSupportKHR(i, surface.get()) != 0 && !indices.presentation.has_value())
            indices.presentation = i;
    }

    return indices;
}

///////////////////////////////////////////////////////////////////////////
SwapchainSupportDetails PhysicalDevice::getSwapchainSupport(const vk::PhysicalDevice& device, const Surface& surface) const
{
    SwapchainSupportDetails details;
    auto vulkanSurface = surface.get();

    details.capabilities = device.getSurfaceCapabilitiesKHR(vulkanSurface);
    details.formats = device.getSurfaceFormatsKHR(vulkanSurface);
    details.presentModes = device.getSurfacePresentModesKHR(vulkanSurface);

    return details;
}

///////////////////////////////////////////////////////////////////////////
uint32_t PhysicalDevice::getMemoryTypeIndex(uint32_t filter, vk::MemoryPropertyFlags propertyToSelect) const
{
    auto properties = device.getMemoryProperties();
    for (size_t i = 0; i < properties.memoryTypeCount; i++)
    {
        if ((filter & (1 << i)) != 0 && (properties.memoryTypes[i].propertyFlags & propertyToSelect) == propertyToSelect)
        {
            return i;
        }
    }

    throw std::runtime_error("[PhysicalDevice] Failed to find suitable memory type!");
}

///////////////////////////////////////////////////////////////////////////
int PhysicalDevice::scoreDevice(const vk::PhysicalDevice& device, const Surface& surface, const Extensions& extensions) const
{
    int score = 0;
    auto properties = device.getProperties();
    auto features = device.getFeatures();
    auto queueFamilies = getQueueFamilies(device, surface);
    auto swapchainSupport = getSwapchainSupport(device, surface);

    // Check first the extensions, some of the following check might fail otherwise
    if (!areExtensionsSupported(device, extensions))
        return -1;

    if (!features.geometryShader || !queueFamilies.isComplete() || !swapchainSupport.isAdequate())
        return -1;

    if (properties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu)
        score += 100;

    return score;
}

///////////////////////////////////////////////////////////////////////////
bool PhysicalDevice::areExtensionsSupported(const vk::PhysicalDevice& device, const Extensions& extensions) const
{
    size_t count = 0;
    for (const auto& extension : device.enumerateDeviceExtensionProperties())
    {
        if (extensions.find(extension.extensionName) != extensions.end())
            count++;
    }
    return count == extensions.size();
}