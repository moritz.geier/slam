#include "CommandBuffer.hpp"

// vulkan
#include <vulkan_backend/CommandPool.hpp>
#include <vulkan_backend/Framebuffer.hpp>
#include <vulkan_backend/LogicalDevice.hpp>
#include <vulkan_backend/RenderPass.hpp>

///////////////////////////////////////////////////////////////////////////
render::CommandBuffer::CommandBuffer(vk::UniqueCommandBuffer&& buffer)
: ::CommandBuffer{std::move(buffer)}
{
}

///////////////////////////////////////////////////////////////////////////
render::CommandBuffer::CommandBuffer(const vk::CommandBuffer& buffer)
: ::CommandBuffer{std::move(buffer)}
{
}

///////////////////////////////////////////////////////////////////////////
render::CommandBuffer::CommandBuffer(CommandBuffer&& moveCommandBuffer)
: ::CommandBuffer{std::move(moveCommandBuffer)}
{
}

///////////////////////////////////////////////////////////////////////////
render::CommandBuffer& render::CommandBuffer::operator=(CommandBuffer&& moveCommandBuffer)
{
    if (&moveCommandBuffer != this)
        commandBuffer = std::move(moveCommandBuffer.commandBuffer);
    return *this;
}

///////////////////////////////////////////////////////////////////////////
render::CommandBuffer::~CommandBuffer()
{
}

///////////////////////////////////////////////////////////////////////////
std::vector<render::CommandBuffer> render::CommandBuffer::allocate(const LogicalDevice& device, const CommandPool& commandPool, size_t count)
{
    vk::CommandBufferAllocateInfo allocateInfo{};
    allocateInfo.sType = vk::StructureType::eCommandBufferAllocateInfo;
    allocateInfo.commandPool  = commandPool.get();
    allocateInfo.level = vk::CommandBufferLevel::ePrimary;
    allocateInfo.commandBufferCount = count;

    auto vulkanBuffers = device->allocateCommandBuffersUnique(allocateInfo);

    std::vector<CommandBuffer> buffers;
    buffers.reserve(vulkanBuffers.size());
    for (auto& vulkanBuffer : vulkanBuffers)
        buffers.emplace_back(std::move(vulkanBuffer));

    return buffers;
}

///////////////////////////////////////////////////////////////////////////
void render::CommandBuffer::record(const RenderPass& renderPass, const Framebuffer& framebuffer, const Pipeline<TextureVertex>& pipeline, const DescriptorSet<TransformationBuffer>& descriptorSet, const VertexBuffer<TextureVertex>& vertexBuffer, const vk::Extent2D& extent, std::array<float, 4> clearColor)
{
    commandBuffer->reset();

    vk::CommandBufferBeginInfo beginInfo{};
    beginInfo.sType = vk::StructureType::eCommandBufferBeginInfo;

    commandBuffer->begin(beginInfo);

    const vk::ClearValue clearValue{{clearColor}};
    vk::RenderPassBeginInfo renderPassInfo{};
    renderPassInfo.sType = vk::StructureType::eRenderPassBeginInfo;
    renderPassInfo.renderPass = renderPass.get();
    renderPassInfo.framebuffer = framebuffer.get();
    renderPassInfo.renderArea.offset = vk::Offset2D{0, 0};
    renderPassInfo.renderArea.extent = extent;
    renderPassInfo.clearValueCount = 1;
    renderPassInfo.pClearValues = &clearValue;

    commandBuffer->beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
    commandBuffer->bindPipeline(vk::PipelineBindPoint::eGraphics, pipeline.get());

    const std::array<vk::DeviceSize, 1> offset{0};
    commandBuffer->bindVertexBuffers(0, 1, &vertexBuffer.get(), offset.data());

    commandBuffer->bindDescriptorSets(vk::PipelineBindPoint::eGraphics, pipeline.getLayout(), 0, 1, &descriptorSet.get(), 0, nullptr);

    vk::Viewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = static_cast<float>(extent.width);
    viewport.height = static_cast<float>(extent.height);
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    commandBuffer->setViewport(0, 1, &viewport);

    vk::Rect2D scissor{};
    scissor.offset = vk::Offset2D{0, 0};
    scissor.extent = extent;

    commandBuffer->setScissor(0, 1, &scissor);
    commandBuffer->draw(static_cast<uint32_t>(vertexBuffer.size()), 1, 0, 0);

    commandBuffer->endRenderPass();
    commandBuffer->end();
}