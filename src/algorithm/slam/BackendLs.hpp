#pragma once

// eigen
#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "SlamTypes.hpp"

namespace algorithm::slam
{
    ///////////////////////////////////////////////////////////////////////////
    namespace
    {
        ///////////////////////////////////////////////////////////////////////////
        void setBlock(Eigen::SparseMatrix<float>& matrix, size_t rowBegin, size_t colBegin, const Eigen::Matrix3f& block)
        {
            for (size_t currentCol = 0; currentCol < block.ColsAtCompileTime; currentCol++)
            {
                for (size_t currentRow = 0; currentRow < block.RowsAtCompileTime; currentRow++)
                    matrix.coeffRef(rowBegin + currentRow, colBegin + currentCol) = block(currentRow, currentCol);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    template<utils::concepts::Timestep StepType>
    void backendLs(SlamGraph<StepType>& graph, const SolveConfiguration<StepType>& config)
    {
        if (config.runBackend)
            return;

        const int size = static_cast<int>(3 * graph.nodeSize());
        Eigen::VectorXf bVector{size};
        bVector.setZero();
        Eigen::SparseMatrix<float> hMatrix{size, size};
        hMatrix.insert(0, 0) = 1;
        hMatrix.insert(1, 1) = 1;
        hMatrix.insert(2, 2) = 1;
        for (const auto& node : graph.getNodes())
        {
            const Eigen::Matrix3f informationMatrix = Eigen::Matrix3f::Identity();
            Eigen::Matrix3f jacobianNode;
            jacobianNode(2, 0) = 0;
            jacobianNode(2, 1) = 0;
            jacobianNode(2, 2) = -1;
            Eigen::Matrix3f jacobianConnection;
            jacobianConnection(2, 0) = 0;
            jacobianConnection(2, 1) = 0;
            jacobianConnection(2, 2) = 1;
            jacobianConnection(1, 2) = 0;
            jacobianConnection(0, 2) = 0;
            Eigen::Matrix2f derivativeOfNodeRotation;
            derivativeOfNodeRotation(0, 0) = node->get().pose(0, 1);
            derivativeOfNodeRotation(1, 0) = node->get().pose(0, 0);
            derivativeOfNodeRotation(0, 1) = -node->get().pose(0, 0);
            derivativeOfNodeRotation(1, 1) = node->get().pose(0, 1);

            for (const auto& connection : node->getConnections())
            {
                if (connection.edge->from != node->id())
                    continue;

                if (connection.node.expired())
                    continue;

                auto pConnectionNode = connection.node.lock();

                const Eigen::AffineCompact2f homogeneousError = connection.edge->relativeTransformation.inverse() * (node->get().pose.inverse() * pConnectionNode->get().pose);
                Eigen::Vector3f error;
                error.segment(0, 2) = homogeneousError.translation();
                error(2) = std::atan2(homogeneousError(1, 0), homogeneousError(0, 0));

                jacobianNode.block<2, 2>(0, 0) = -connection.edge->relativeTransformation.rotation().transpose() * node->get().pose.rotation().transpose();
                jacobianNode.block<2, 1>(0, 2) = connection.edge->relativeTransformation.rotation().transpose() * derivativeOfNodeRotation * (pConnectionNode->get().pose.translation() - node->get().pose.translation());
                jacobianConnection.block<2, 2>(0, 0) = connection.edge->relativeTransformation.rotation().transpose() * node->get().pose.rotation().transpose();

                const size_t nodeId = node->id() * 3;
                const size_t connectionId = pConnectionNode->id() * 3;
                bVector.segment<3>(nodeId) += error.transpose() * informationMatrix * jacobianNode;
                bVector.segment<3>(connectionId) += error.transpose() * informationMatrix * jacobianConnection;

                const auto nodeBlock = hMatrix.block(nodeId, nodeId, 3, 3) + jacobianNode.transpose() * informationMatrix * jacobianNode;
                const auto nodeConnectionBlock = hMatrix.block(nodeId, connectionId, 3, 3) + jacobianNode.transpose() * informationMatrix * jacobianConnection;
                const auto connectionNodeBlock = hMatrix.block(connectionId, nodeId, 3, 3) + jacobianConnection.transpose() * informationMatrix * jacobianNode;
                const auto connectionBlock = hMatrix.block(connectionId, connectionId, 3, 3) + jacobianConnection.transpose() * informationMatrix * jacobianConnection;

                setBlock(hMatrix, nodeId, nodeId, nodeBlock);
                setBlock(hMatrix, nodeId, connectionId, nodeConnectionBlock);
                setBlock(hMatrix, connectionId, nodeId, connectionNodeBlock);
                setBlock(hMatrix, connectionId, connectionId, connectionBlock);
            }
        }

        Eigen::SimplicialCholesky<Eigen::SparseMatrix<float>> cholskey{hMatrix};
        const Eigen::VectorXf optimization = cholskey.solve(-bVector);

        for (auto& node : graph.getNodes())
        {
            const size_t offset = 3 * node->id();
            Eigen::AffineCompact2f deltaTransform;
            deltaTransform.translation() = optimization.segment<2>(offset);
            deltaTransform.linear() = Eigen::Rotation2D{optimization(offset + 2)}.toRotationMatrix();
            node->get().pose = node->get().pose * deltaTransform;
        }
    }
}