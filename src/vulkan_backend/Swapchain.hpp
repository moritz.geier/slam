#pragma once

// std
#include <optional>

// vulkan
#include <vulkan/vulkan.hpp>

#include "Image.hpp"

class LogicalDevice;
class PhysicalDevice;
class Semaphore;
class Surface;
class Window;

class Swapchain final
{
public:
    Swapchain(const LogicalDevice& device, const PhysicalDevice& physicalDevice, const Surface& surface, const Window& window);
    Swapchain(Swapchain&&);
    Swapchain& operator=(Swapchain&&);
    Swapchain(const Swapchain&) = delete;
    Swapchain& operator=(const Swapchain&) = delete;
    ~Swapchain();

    const vk::SwapchainKHR& get() const;
    vk::SwapchainKHR& get();

    const vk::Extent2D& getExtent() const;
    vk::Extent2D& getExtent();

    const vk::Format& getFormat() const;
    vk::Format& getFormat();

    const Images& getImages() const;
    Images& getImages();

    std::optional<uint32_t> aquireNextImage(const Semaphore& signalAfter) const;
    void recreate(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const Surface& surface, const Window& window);

private:
    vk::UniqueSwapchainKHR swapchain;
    vk::Format format;
    vk::Extent2D extent;
    Images images;
    vk::Device device;

    void create(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const Surface& surface, const Window& window);
};