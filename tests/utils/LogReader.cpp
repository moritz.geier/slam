// catch2
#include <catch2/catch_test_macros.hpp>

// std
#include <chrono>
#include <filesystem>
#include <fstream>
#include <string>
#include <vector>

// utils
#include <utils/LogReader.hpp>

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Reading a log file, input variations.", "[utils::LogReader]")
{
    const uint32_t seed = static_cast<uint32_t>(std::chrono::system_clock::now().time_since_epoch().count());
    std::srand(seed);
    std::filesystem::path filePath = std::filesystem::current_path() / "temp";
    std::filesystem::create_directories(filePath);

    SECTION("Inputing empty string as path.")
    {
        REQUIRE_THROWS(utils::LogReader{""});
    }

    SECTION("Inputing file with wrong extension.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".txt";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output.close();

        REQUIRE_THROWS(utils::LogReader{filePath});
    }

    SECTION("Inputing non existent file.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;

        if (std::filesystem::exists(filePath))
            std::filesystem::remove(filePath);

        REQUIRE_THROWS(utils::LogReader{filePath});
    }

    SECTION("Inputing blocked file.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        std::filesystem::path filePath = std::filesystem::current_path() / "temp";
        filePath /= fileName;

        REQUIRE_THROWS(utils::LogReader{filePath});
    }

    SECTION("Inputing empty file.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output.close();

        REQUIRE_NOTHROW(utils::LogReader{filePath});
    }

    std::filesystem::remove_all(filePath);
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Reading a log file, retriving one entry.", "[utils::LogReader::nextStep]")
{
    const uint32_t seed = static_cast<uint32_t>(std::chrono::system_clock::now().time_since_epoch().count());
    std::srand(seed);
    std::filesystem::path filePath = std::filesystem::current_path() / "temp";
    std::filesystem::create_directories(filePath);

    SECTION("Parsing empty file.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output.close();

        utils::LogReader logReader{filePath};
        auto result = logReader.nextStep();
        REQUIRE(!result.has_value());
    }

    SECTION("Malformed file, no FLASER entry.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.179699 nohost 0.000000\n"
               << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.309700 nohost 0.130001\n"
               << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.429500 nohost 0.249801\n";
        output.close();

        utils::LogReader logReader{filePath};
        auto result = logReader.nextStep();
        REQUIRE(!result.has_value());
    }

    SECTION("Malformed file, malformed FLASER entry.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output << "FLASER 6 46.56 47.31 12.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.012727 -1.570796 70.289900 nohost 0.110201\n"
               << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.429500 nohost 0.249801\n"
               << "FLASER 10 46.56 47.31 12.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.012727 -1.570796 70.289900 nohost 0.310201\n"
               << "FLASER 6 46.56 47.31 12.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.012727 -1.570796 70.289900 nohost 0.010201\n";
        output.close();

        utils::LogReader logReader{filePath};
        auto result = logReader.nextStep();
        REQUIRE(result.has_value());
        result = logReader.nextStep();
        REQUIRE(!result.has_value());
    }

    SECTION("Correct parsing.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output << "FLASER 6 46.56 47.31 6.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.032727 -1.570796 70.289900 nohost 0.112501\n"
               << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.429500 nohost 0.249801\n"
               << "FLASER 6 46.56 47.31 15.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.012727 -1.570796 70.289900 nohost 0.310201\n"
               << "FLASER 6 46.56 47.31 12.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.042727 -1.570796 70.289900 nohost 0.400201\n";
        output.close();

        utils::LogReader logReader{filePath};
        auto result = logReader.nextStep();
        REQUIRE(result.has_value());
        REQUIRE(result.value().measurements.size() == 6);
        REQUIRE((result.value().timestamp > std::chrono::milliseconds(110) && result.value().timestamp < std::chrono::milliseconds(114)));

        result = logReader.nextStep();
        REQUIRE(result.has_value());
        REQUIRE(result.value().measurements.size() == 6);
        REQUIRE((result.value().pose.translation().y() > 0.0127 && result.value().pose.translation().y() < 0.0128));

        result = logReader.nextStep();
        REQUIRE(result.has_value());
        REQUIRE(result.value().measurements.size() == 6);
        const float measurementNorm = result.value().measurements[2].norm();
        REQUIRE((measurementNorm > 12.50 && measurementNorm < 12.52));

        result = logReader.nextStep();
        REQUIRE(!result.has_value());
    }

    std::filesystem::remove_all(filePath);
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Reading a log file, retriving all entries.", "[utils::LogReader::allSteps]")
{
    const uint32_t seed = static_cast<uint32_t>(std::chrono::system_clock::now().time_since_epoch().count());
    std::srand(seed);
    std::filesystem::path filePath = std::filesystem::current_path() / "temp";
    std::filesystem::create_directories(filePath);

    SECTION("Parsing empty file.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output.close();

        auto result = utils::LogReader::allSteps(filePath);
        REQUIRE(result.empty());
    }

    SECTION("Malformed file, no FLASER entry.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.179699 nohost 0.000000\n"
               << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.309700 nohost 0.130001\n"
               << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.429500 nohost 0.249801\n";
        output.close();

        auto result = utils::LogReader::allSteps(filePath);
        REQUIRE(result.empty());
    }

    SECTION("Malformed file, malformed FLASER entry.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output << "FLASER 6 46.56 47.31 12.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.012727 -1.570796 70.289900 nohost 0.110201\n"
               << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.429500 nohost 0.249801\n"
               << "FLASER 10 46.56 47.31 12.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.012727 -1.570796 70.289900 nohost 0.310201\n"
               << "FLASER 6 46.56 47.31 12.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.012727 -1.570796 70.289900 nohost 0.010201\n";
        output.close();

        auto result = utils::LogReader::allSteps(filePath);
        REQUIRE(result.size() == 1);
    }

    SECTION("Correct parsing.")
    {
        std::string fileName = "test" + std::to_string(std::rand()) + ".log";
        filePath /= fileName;
        std::ofstream output;
        output.open(filePath);
        output << "FLASER 6 46.56 47.31 6.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.032727 -1.570796 70.289900 nohost 0.112501\n"
               << "ODOM -0.000602 0.012727 -1.570796 0.000000 0.000000 0.000000 70.429500 nohost 0.249801\n"
               << "FLASER 6 46.56 47.31 15.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.012727 -1.570796 70.289900 nohost 0.310201\n"
               << "FLASER 6 46.56 47.31 12.51 14.71 12.16 12.26 -0.000602 0.012727 -1.570796 -0.000602 0.042727 -1.570796 70.289900 nohost 0.400201\n";
        output.close();

        auto result = utils::LogReader::allSteps(filePath);
        REQUIRE(result.size() == 3);
        REQUIRE(result[0].measurements.size() == 6);
        REQUIRE((result[0].timestamp > std::chrono::milliseconds(110) && result[0].timestamp < std::chrono::milliseconds(114)));

        REQUIRE(result[1].measurements.size() == 6);
        REQUIRE((result[1].pose.translation().y() > 0.0127 && result[1].pose.translation().y() < 0.0128));

        REQUIRE(result[2].measurements.size() == 6);
        const float measurementNorm = result[2].measurements[2].norm();
        REQUIRE((measurementNorm > 12.50 && measurementNorm < 12.52));
    }

    std::filesystem::remove_all(filePath);
}