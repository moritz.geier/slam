#pragma once

// glm
#include <glm/glm.hpp>

// vulkan
#include <vulkan/vulkan.hpp>

///////////////////////////////////////////////////////////////////////////
namespace render
{
    struct TextureVertex
    {
        glm::vec2 position;
        glm::vec2 textureCoordinate;

        ///////////////////////////////////////////////////////////////////////////
        static vk::VertexInputBindingDescription getBindingDescription()
        {
            vk::VertexInputBindingDescription bindingDescription{};
            bindingDescription.binding = 0;
            bindingDescription.stride = sizeof(TextureVertex);
            bindingDescription.inputRate = vk::VertexInputRate::eVertex;

            return bindingDescription;
        }

        ///////////////////////////////////////////////////////////////////////////
        static std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions()
        {
            std::vector<vk::VertexInputAttributeDescription> attributes;

            {
                vk::VertexInputAttributeDescription attribute{};
                attribute.binding = 0;
                attribute.location = 0;
                attribute.format = vk::Format::eR32G32Sfloat;
                attribute.offset = offsetof(TextureVertex, position);
                attributes.push_back(attribute);
            }

            {
                vk::VertexInputAttributeDescription attribute{};
                attribute.binding = 0;
                attribute.location = 1;
                attribute.format = vk::Format::eR32G32Sfloat;
                attribute.offset = offsetof(TextureVertex, textureCoordinate);
                attributes.push_back(attribute);
            }

            return attributes;
        }
    };
}