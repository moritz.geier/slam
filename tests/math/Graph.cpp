// catch2
#include <catch2/catch_test_macros.hpp>

// math
#include <math/Graph.hpp>

// std
#include <limits>

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Testing operations on a single node.", "[math::Graph::Node]")
{
    SECTION("Move constructor test.")
    {
        class Test
        {
        public:
            Test(int value)
            : value{value}
            {
            }

            Test(Test&& test)
            : value{test.value}
            {
                test.value = -1;
            }

        public:
            int value;
        };

        Test t{30};
        CHECK(t.value == 30);
        math::Graph<Test, int>::Node node{std::move(t), 0};
        CHECK(node->value == 30);
        CHECK(t.value == -1);

        math::Graph<Test, int>::Node moveNode{std::move(node)};
        CHECK(moveNode.id() == 0);
        CHECK(moveNode->value == 30);
        CHECK(node->value == -1);
    }

    SECTION("Requesting the id of a node.")
    {
        math::Graph<int, int>::Node nodeOne{1, 0};
        CHECK(nodeOne.id() == 0);

        math::Graph<int, int>::Node nodeTwo{1, 55};
        CHECK(nodeTwo.id() == 55);

        math::Graph<int, int>::Node nodeThree{1, std::numeric_limits<size_t>().max()};
        CHECK(nodeThree.id() == std::numeric_limits<size_t>().max());
    }

    SECTION("Getting and changing the value of a node.")
    {
        math::Graph<int, int>::Node node{0, 0};
        auto& nodeValue = node.get();
        CHECK(nodeValue == 0);

        nodeValue = 30;
        CHECK(30 == node.get());

        nodeValue = -30;
        CHECK(-30 == node.get());

        const math::Graph<int, int>::Node constNode{30, 0};
        CHECK(constNode.get() == 30);
    }

    SECTION("Getting and changing the value of a node via -> operator.")
    {
        struct Test
        {
            int a;
        };

        math::Graph<Test, int>::Node node{{0}, 0};
        auto& nodeValue = node.get();
        CHECK(nodeValue.a == node->a);
        CHECK(node->a == 0);

        node->a = 30;
        CHECK(30 == node.get().a);

        nodeValue.a = -30;
        CHECK(-30 == node->a);

        const math::Graph<Test, int>::Node constNode{{30}, 0};
        CHECK(constNode->a == 30);
    }
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Testing operations on edges.", "[math::Graph::Edge]")
{
    class Node : public math::Graph<int, int>::Node
    {
    public:
        Node(int value, size_t id)
        : math::Graph<int, int>::Node{value, id}
        {
        }

        virtual ~Node() override = default;

        void connect(const math::Graph<int, int>::Edge& edge)
        {
            math::Graph<int, int>::Node::connect(edge.edge, edge.node);
        }

        void disconnect(const math::Graph<int, int>::Node& node)
        {
            math::Graph<int, int>::Node::disconnect(node);
        }
    };

    auto nodeOne = std::make_shared<Node>(5, 0);
    auto nodeTwo = std::make_shared<Node>(5, 1);
    auto edgeValue = std::make_shared<int>(15);
    math::Graph<int, int>::Edge edge{edgeValue, nodeTwo};
    nodeOne->connect(edge);
    REQUIRE(nodeOne->getConnections().size() == 1);
    REQUIRE(nodeTwo->getConnections().size() == 0);
    CHECK(nodeOne->getConnections().front().edge == edgeValue);
    CHECK(nodeOne->getConnections().front().node.lock() == nodeTwo);

    nodeTwo->connect(edge);
    REQUIRE(nodeOne->getConnections().size() == 1);
    REQUIRE(nodeTwo->getConnections().size() == 1);
    CHECK(nodeTwo->getConnections().front().edge == edgeValue);
    CHECK(nodeTwo->getConnections().front().node.lock() == nodeTwo);

    math::Graph<int, int>::Edge loop{edgeValue, nodeOne};
    nodeOne->connect(loop);
    REQUIRE(nodeOne->getConnections().size() == 2);
    REQUIRE(nodeTwo->getConnections().size() == 1);
    CHECK(nodeOne->getConnections().back().edge == edgeValue);
    CHECK(nodeOne->getConnections().back().node.lock() == nodeOne);

    std::weak_ptr<int> edgeReference = edgeValue;
    edge.edge.reset();
    loop.edge.reset();
    edgeValue.reset();
    REQUIRE(!edgeReference.expired());
    nodeOne->disconnect(*nodeOne);
    REQUIRE(!edgeReference.expired());
    REQUIRE(nodeOne->getConnections().size() == 1);
    REQUIRE(nodeTwo->getConnections().size() == 1);
    CHECK(nodeOne->getConnections().front().edge == edgeReference.lock());
    CHECK(nodeOne->getConnections().front().node.lock() == nodeTwo);

    nodeOne->disconnect(*nodeTwo);
    REQUIRE(!edgeReference.expired());
    REQUIRE(nodeOne->getConnections().size() == 0);
    REQUIRE(nodeTwo->getConnections().size() == 1);

    nodeTwo->disconnect(*nodeTwo);
    REQUIRE(edgeReference.expired());
    REQUIRE(nodeOne->getConnections().size() == 0);
    REQUIRE(nodeTwo->getConnections().size() == 0);
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Testing graph datastructure.", "[math::Graph]")
{
    class Test
    {
    public:
        Test(int value)
        : value{value}
        {
        }

        Test(const Test& test)
        : value{test.value}
        {
        }

        Test& operator=(const Test& test)
        {
            if (&test != this)
                value = test.value;
            return *this;
        }

        Test(Test&& test)
        : value{test.value}
        {
            test.value = -1;
        }

        Test& operator=(Test&& test)
        {
            if (&test != this)
            {
                value = test.value;
                test.value = -1;
            }
            return *this;
        }

    public:
        int value;
    };

    SECTION("Add node to graph.")
    {
        math::Graph<Test, int> graph;

        const auto& firstNode = graph.addNode({5});
        REQUIRE(firstNode->value == 5);

        Test test{15};
        const auto& secondNode = graph.addNode(test);
        REQUIRE(secondNode->value == 15);
        REQUIRE(test.value == 15);

        const auto& thirdNode = graph.addNode(std::move(test));
        REQUIRE(thirdNode->value == 15);
        REQUIRE(test.value == -1);
    }

    SECTION("Add edge to graph.")
    {
        math::Graph<int, Test> graph;

        auto& nodeOne = graph.addNode(5);
        auto& nodeTwo = graph.addNode(10);

        graph.addEdge({5}, nodeOne, nodeTwo);
        REQUIRE(nodeOne.getConnections().size() == 1);
        REQUIRE(nodeTwo.getConnections().size() == 1);
        REQUIRE(nodeOne.getConnections().front().edge->value == 5);
        REQUIRE(nodeTwo.getConnections().front().edge->value == 5);
        REQUIRE(&nodeOne.getConnections().front().edge->value == &nodeTwo.getConnections().front().edge->value);
        REQUIRE(&nodeOne.getConnections().front().edge->value == &nodeTwo.getConnections().front().edge->value);
        REQUIRE(nodeOne.getConnections().front().node.lock().get() == &nodeTwo);
        REQUIRE(nodeTwo.getConnections().front().node.lock().get() == &nodeOne);

        Test test{15};
        graph.addEdge(test, nodeOne, nodeOne);
        REQUIRE(nodeOne.getConnections().size() == 2);
        REQUIRE(nodeOne.getConnections().back().edge->value == 15);
        REQUIRE(nodeOne.getConnections().back().node.lock().get() == &nodeOne);
        REQUIRE(test.value == 15);

        graph.addEdge(std::move(test), nodeTwo, nodeOne);
        REQUIRE(nodeOne.getConnections().size() == 2);
        REQUIRE(nodeTwo.getConnections().size() == 1);
        REQUIRE(nodeOne.getConnections().front().edge->value == 15);
        REQUIRE(nodeTwo.getConnections().front().edge->value == 15);
        REQUIRE(&nodeOne.getConnections().front().edge->value == &nodeTwo.getConnections().front().edge->value);
        REQUIRE(&nodeOne.getConnections().front().edge->value == &nodeTwo.getConnections().front().edge->value);
        REQUIRE(nodeOne.getConnections().front().node.lock().get() == &nodeTwo);
        REQUIRE(nodeTwo.getConnections().front().node.lock().get() == &nodeOne);
        REQUIRE(test.value == -1);
    }

    SECTION("Get sizes of graph and removing nodes.")
    {
        math::Graph<int, int> graph;
        REQUIRE(graph.nodeSize() == 0);
        REQUIRE(graph.edgeSize() == 0);

        auto& firstNode = graph.addNode(5);
        REQUIRE(graph.nodeSize() == 1);
        auto& secondNode = graph.addNode(10);
        REQUIRE(graph.nodeSize() == 2);
        graph.addEdge(15, firstNode, secondNode);
        REQUIRE(graph.edgeSize() == 1);
        graph.addEdge(2, firstNode, firstNode);
        REQUIRE(graph.edgeSize() == 2);
        graph.addEdge(2, secondNode, firstNode);
        REQUIRE(graph.edgeSize() == 2);
        graph.addEdge(2, secondNode, secondNode);
        REQUIRE(graph.edgeSize() == 3);

        graph.removeNode(firstNode);
        REQUIRE(graph.nodeSize() == 1);
        REQUIRE(graph.edgeSize() == 1);
        graph.removeEdge(secondNode, secondNode);
        REQUIRE(graph.nodeSize() == 1);
        REQUIRE(graph.edgeSize() == 0);
        graph.removeNode(secondNode.id());
        REQUIRE(graph.nodeSize() == 0);
        REQUIRE(graph.edgeSize() == 0);

        const auto& checkMaxId = graph.addNode(5);
        REQUIRE(checkMaxId.id() == 2);
        REQUIRE(graph.nextId() == 3);
    }
}