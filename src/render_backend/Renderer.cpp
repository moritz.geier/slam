#include "Renderer.hpp"
#include "CMakeVariables.hpp"

// glm
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// std
#include <optional>

// vulkan_backend
#include <vulkan_backend/CommandPool.hpp>
#include <vulkan_backend/Sampler.hpp>

namespace
{
    const std::vector<render::TextureVertex> planeVertecies = {
        {{-0.5, -0.5}, {1.0f, 0.0f}},
        {{0.5, -0.5}, {0.0f, 0.0f}},
        {{-0.5, 0.5}, {1.0f, 1.0f}},
        {{0.5, 0.5}, {0.0f, 1.0f}},
        {{-0.5, 0.5}, {1.0f, 1.0f}},
        {{0.5, -0.5}, {0.0f, 0.0f}}
    };
}

///////////////////////////////////////////////////////////////////////////
Renderer::Renderer(const PhysicalDevice& physicalDevice, const LogicalDevice& logicalDevice, const Sampler& sampler, const CommandPool& commandPool, const DescriptorPool& descriptorPool, size_t framesInFlight, const vk::Extent2D& extent)
// Initialization order is important!
: framesInFlight{framesInFlight}
, extent{extent}
, physicalDevice{physicalDevice}
, logicalDevice{logicalDevice}
, renderTargetImages{logicalDevice, physicalDevice, vk::Format::eR8G8B8A8Unorm, extent, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal, framesInFlight}
, renderTargetImageViews{logicalDevice, vk::Format::eR8G8B8A8Unorm, renderTargetImages}
, renderPass{logicalDevice, vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::eShaderReadOnlyOptimal}
, sprite{logicalDevice, physicalDevice, 1024, 1024}
, uniformBuffers{logicalDevice, physicalDevice, framesInFlight}
, descriptorSetLayout{logicalDevice}
, descriptorSets{logicalDevice, descriptorPool, descriptorSetLayout, uniformBuffers, sampler, sprite.get()}
, pipeline{logicalDevice, renderPass, descriptorSetLayout, createShaders(logicalDevice)}
, framebuffers{logicalDevice, renderPass, renderTargetImageViews, extent}
, commandBuffers{render::CommandBuffer::allocate(logicalDevice, commandPool, framesInFlight)}
, vertexBuffer{logicalDevice, physicalDevice, planeVertecies}
{
    vertexBuffer.upload();

    currentFrame = 0;
}

///////////////////////////////////////////////////////////////////////////
Renderer::~Renderer()
{
}

///////////////////////////////////////////////////////////////////////////
const ImageViews& Renderer::getImageViews() const
{
    return renderTargetImageViews;
}

///////////////////////////////////////////////////////////////////////////
ImageViews& Renderer::getImageViews()
{
    return renderTargetImageViews;
}

///////////////////////////////////////////////////////////////////////////
const CommandBuffer& Renderer::render(float x, float y, float z)
{
    constexpr float nearClipPlane = 0.01f;
    constexpr float farClipPlane = 100.0f;

    z = std::max(nearClipPlane * 2, z);

    auto* uniformBuffer = uniformBuffers[currentFrame].data();
    uniformBuffer->model = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
    uniformBuffer->view = glm::translate(glm::mat4(1), glm::vec3(-x, -y, -z));
    uniformBuffer->projection = glm::perspective(glm::radians(45.0f), extent.width / static_cast<float>(extent.height), nearClipPlane, farClipPlane);
    uniformBuffer->projection[1][1] *= -1;

    auto& currentCommandBuffer = commandBuffers[currentFrame];
    currentCommandBuffer.record(renderPass, framebuffers[currentFrame], pipeline, descriptorSets[currentFrame], vertexBuffer, extent, {0.0, 0.0, 0.0, 1.0});

    currentFrame = (currentFrame + 1) % framesInFlight;
    return currentCommandBuffer;
}

///////////////////////////////////////////////////////////////////////////
void Renderer::updateSprite(const algorithm::slam::Grid<utils::definitions::GrayScale>& grid, const CommandPool& pool)
{
    const ConvertToBGRA<utils::definitions::GrayScale> grayToBgra = [](const auto& value) { return render::Sprite::TextureDataType{value.value, value.value, value.value, 255}; };
    updateSprite(grid, pool, grayToBgra);
}

///////////////////////////////////////////////////////////////////////////
void Renderer::updateSprite(const algorithm::slam::Grid<utils::definitions::RGB>& grid, const CommandPool& pool)
{
    const ConvertToBGRA<utils::definitions::RGB> rgbToBgra = [](const auto& rgb) { return render::Sprite::TextureDataType{rgb.red, rgb.green, rgb.blue, 255}; };
    updateSprite(grid, pool, rgbToBgra);
}

///////////////////////////////////////////////////////////////////////////
void Renderer::updateSprite(const algorithm::slam::Grid<utils::definitions::RGBA>& grid, const CommandPool& pool)
{
    const ConvertToBGRA<utils::definitions::RGBA> rgbaToBgra = [](const auto& rgb) { return render::Sprite::TextureDataType{rgb.red, rgb.green, rgb.blue, rgb.alpha}; };
    updateSprite(grid, pool, rgbaToBgra);
}

///////////////////////////////////////////////////////////////////////////
void Renderer::recreate(size_t x, size_t y)
{
    logicalDevice.waitForIdle();
    extent = vk::Extent2D{static_cast<uint32_t>(x), static_cast<uint32_t>(y)};
    renderTargetImages = Images{logicalDevice, physicalDevice, vk::Format::eR8G8B8A8Unorm, extent, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal, framesInFlight};
    renderTargetImageViews = ImageViews{logicalDevice, vk::Format::eR8G8B8A8Unorm, renderTargetImages};
    framebuffers = Framebuffers{logicalDevice, renderPass, renderTargetImageViews, extent};
}

///////////////////////////////////////////////////////////////////////////
std::vector<Shader> Renderer::createShaders(const LogicalDevice& logicalDevice) const
{
    std::vector<Shader> shaders;
    shaders.push_back({logicalDevice, CMake::programRootDirectory / "build\\shaders\\shader.vert.spv"});
    shaders.push_back({logicalDevice, CMake::programRootDirectory / "build\\shaders\\shader.frag.spv"});
    return shaders;
}

///////////////////////////////////////////////////////////////////////////
template<typename T>
void Renderer::updateSprite(const algorithm::slam::Grid<T>& grid, const CommandPool& pool, const ConvertToBGRA<T>& convertFunction)
{
    auto* spriteData = sprite.data();

    vk::ImageSubresource subresource{};
    subresource.aspectMask = vk::ImageAspectFlagBits::eColor;
    const auto layout = logicalDevice->getImageSubresourceLayout(sprite.getImage().get(), subresource);

    const size_t rowPitch = layout.rowPitch / sizeof(render::Sprite::TextureDataType);
    const size_t layoutOffset = layout.offset / sizeof(render::Sprite::TextureDataType);

    for (size_t y = 0; y < grid.height(); y++)
    {
        for (size_t x = 0; x < grid.width(); x++)
        {
            const size_t offset = y * rowPitch + x + layoutOffset;
            spriteData[offset] = convertFunction(grid.at(x, y));
        }
    }

    sprite.upload(pool);
}