#pragma once

// eigen
#include <Eigen/Dense>

// utils
#include <utils/Definitions.hpp>

///////////////////////////////////////////////////////////////////////////
namespace algorithm
{
    ///////////////////////////////////////////////////////////////////////////
    template<typename Entry>
    concept NearestNeighborEntry = requires(Entry entry)
    {
        utils::concepts::AnyEigenVector<decltype(entry.coordinate)>;
    };

    ///////////////////////////////////////////////////////////////////////////
    template<int Dimensions>
    struct DefaultSearch
    {
        Eigen::Vector<float, Dimensions> coordinate;
    };

    ///////////////////////////////////////////////////////////////////////////
    template<NearestNeighborEntry EntryType>
    class INearestNeighbors
    {
    public:
        struct Result
        {
            const EntryType* pNeighbor;
            float distance;
        };

        virtual void set(const std::vector<EntryType>& referencePoints) = 0;
        virtual Result find(const Eigen::Vector<float, decltype(EntryType::coordinate)::RowsAtCompileTime>& target) const = 0;
    };
}
