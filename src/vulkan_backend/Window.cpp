#include "Window.hpp"

///////////////////////////////////////////////////////////////////////////
Window::Window(size_t height, size_t width, const std::string& name)
: height{height}
, width{width}
, shouldDestroy{true}
{
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    window = glfwCreateWindow(height, width, name.c_str(), nullptr, nullptr);
}

///////////////////////////////////////////////////////////////////////////
Window::Window(Window&& moveWindow)
{
    window = moveWindow.window;
    height = moveWindow.height;
    width = moveWindow.width;
    resizeCallback = std::move(moveWindow.resizeCallback);

    shouldDestroy = true;
    moveWindow.shouldDestroy = false;
}

///////////////////////////////////////////////////////////////////////////
Window& Window::operator=(Window&& moveWindow)
{
    if (&moveWindow != this)
    {
        window = moveWindow.window;
        height = moveWindow.height;
        width = moveWindow.width;
        resizeCallback = std::move(moveWindow.resizeCallback);

        shouldDestroy = true;
        moveWindow.shouldDestroy = false;
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Window::~Window()
{
    if (shouldDestroy)
    {
        glfwDestroyWindow(window);
        glfwTerminate();
    }
}

///////////////////////////////////////////////////////////////////////////
GLFWwindow* Window::get() const
{
    return window;
}

///////////////////////////////////////////////////////////////////////////
size_t Window::getHeight() const
{
    return height;
}

///////////////////////////////////////////////////////////////////////////
size_t Window::getWidth() const
{
    return width;
}

///////////////////////////////////////////////////////////////////////////
bool Window::isMinimized()
{
    int width;
    int height;

    glfwWaitEvents();

    glfwGetFramebufferSize(window, &width, &height);

    this->width = static_cast<size_t>(width);
    this->height = static_cast<size_t>(height);

    return width == 0 || height == 0;
}

///////////////////////////////////////////////////////////////////////////
bool Window::shouldClose() const
{
    return glfwWindowShouldClose(window) != 0;
}

///////////////////////////////////////////////////////////////////////////
void Window::pollEvents() const
{
    glfwPollEvents();
}

///////////////////////////////////////////////////////////////////////////
void Window::onResize(ResizeCallback callback)
{
    resizeCallback = callback;
    glfwSetWindowUserPointer(window, reinterpret_cast<void*>(this));
    glfwSetFramebufferSizeCallback(window, onResizeGLFWCallback);
}

///////////////////////////////////////////////////////////////////////////
void Window::onResizeGLFWCallback(GLFWwindow* glfwWindow, int width, int height)
{
    auto window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(glfwWindow));
    window->height = static_cast<size_t>(height);
    window->width = static_cast<size_t>(width);
    window->resizeCallback(*window, window->height, window->width);
}