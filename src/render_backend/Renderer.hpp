#pragma once

// slam
#include <algorithm/slam/Grid.hpp>

// std
#include <functional>
#include <vector>

// vulkan
#include <vulkan_backend/CommandBuffer.hpp>
#include <vulkan_backend/DescriptorSet.hpp>
#include <vulkan_backend/DescriptorSetLayout.hpp>
#include <vulkan_backend/Framebuffer.hpp>
#include <vulkan_backend/Image.hpp>
#include <vulkan_backend/ImageView.hpp>
#include <vulkan_backend/LogicalDevice.hpp>
#include <vulkan_backend/PhysicalDevice.hpp>
#include <vulkan_backend/Pipeline.hpp>
#include <vulkan_backend/RenderPass.hpp>
#include <vulkan_backend/Shader.hpp>
#include <vulkan_backend/UniformBuffer.hpp>
#include <vulkan_backend/VertexBuffer.hpp>

// utils
#include <utils/Definitions.hpp>

#include "CommandBuffer.hpp"
#include "Sprite.hpp"
#include "TextureVertex.hpp"
#include "TransformationBuffer.hpp"

class CommandPool;
class Sampler;

class Renderer final
{
public:
    Renderer(const PhysicalDevice& physicalDevice, const LogicalDevice& logicalDevice, const Sampler& sampler, const CommandPool& commandPool, const DescriptorPool& descriptorPool, size_t framesInFlight, const vk::Extent2D& extent);
    ~Renderer();

    const ImageViews& getImageViews() const;
    ImageViews& getImageViews();

    [[nodiscard]]
    const CommandBuffer& render(float x, float y, float z);
    void updateSprite(const algorithm::slam::Grid<utils::definitions::GrayScale>& grid, const CommandPool& pool);
    void updateSprite(const algorithm::slam::Grid<utils::definitions::RGB>& grid, const CommandPool& pool);
    void updateSprite(const algorithm::slam::Grid<utils::definitions::RGBA>& grid, const CommandPool& pool);

    void recreate(size_t x, size_t y);

private:
    const size_t framesInFlight;
    size_t currentFrame;
    vk::Extent2D extent;

    const PhysicalDevice& physicalDevice;
    const LogicalDevice& logicalDevice;

    Images renderTargetImages;
    ImageViews renderTargetImageViews;
    RenderPass renderPass;
    render::Sprite sprite;
    UniformBuffers<render::TransformationBuffer> uniformBuffers;
    DescriptorSetLayout descriptorSetLayout;
    DescriptorSets<render::TransformationBuffer> descriptorSets;
    Pipeline<render::TextureVertex> pipeline;
    Framebuffers framebuffers;
    std::vector<render::CommandBuffer> commandBuffers;
    VertexBuffer<render::TextureVertex> vertexBuffer;

    std::vector<Shader> createShaders(const LogicalDevice& logicalDevice) const;

    template<typename T>
    using ConvertToBGRA = std::function<render::Sprite::TextureDataType(const T&)>;
    template<typename T>
    void updateSprite(const algorithm::slam::Grid<T>& grid, const CommandPool& pool, const ConvertToBGRA<T>& convertFunction);
};