#pragma once

// std
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

class LogicalDevice;
class RenderPass;
class ImageView;
class ImageViews;

///////////////////////////////////////////////////////////////////////////
class Framebuffer final
{
public:
    Framebuffer(const LogicalDevice& logicalDevice, const RenderPass& renderPass, const ImageView& image, const vk::Extent2D& extent);
    Framebuffer(Framebuffer&&);
    Framebuffer& operator=(Framebuffer&&);
    Framebuffer(const Framebuffer&) = delete;
    Framebuffer& operator=(const Framebuffer&) = delete;
    ~Framebuffer();

    const vk::Framebuffer& get() const;
    vk::Framebuffer& get();

private:
    vk::UniqueFramebuffer framebuffer;
};

///////////////////////////////////////////////////////////////////////////
class Framebuffers final : public std::vector<Framebuffer>
{
public:
    Framebuffers(const LogicalDevice& device, const RenderPass& renderPass, const ImageViews& images, const vk::Extent2D& extent);
};