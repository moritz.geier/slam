#include "Viewport.hpp"
#include "ProgressBar.hpp"

// cereal
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>

// cmake
#include <CMakeVariables.hpp>

// imgui
#include <misc/cpp/imgui_stdlib.h>

// nfd
#include <nfd.hpp>

// slam
#include <algorithm/slam/SlamTypes.hpp>

// utils
#include <utils/LogReader.hpp>
#include <utils/ToGraph.hpp>
#include <utils/Serialization.hpp>

#include "internal/ViewportFullMap.hpp"
#include "internal/ViewportReplay.hpp"

///////////////////////////////////////////////////////////////////////////
namespace ui
{
    ///////////////////////////////////////////////////////////////////////////
    namespace
    {
        ///////////////////////////////////////////////////////////////////////////
        algorithm::slam::SlamGraph<utils::definitions::Timestep> parseFile(float& progress, const std::filesystem::path& filePath)
        {
            if (filePath.extension() == ".log")
            {
                try
                {
                    auto steps = utils::LogReader::allSteps(filePath, progress);
                    progress -= 0.02f;
                    return utils::toGraph<algorithm::slam::EdgeValue>(steps);
                }
                catch(std::runtime_error&)
                {
                }
            }
            else if (filePath.extension() == ".cereal")
            {
                std::ifstream fileStream{filePath, std::ios_base::binary};
                if (fileStream.is_open())
                {
                    algorithm::slam::SlamGraph<utils::definitions::Timestep> graph;
                    cereal::PortableBinaryInputArchive archive(fileStream);
                    archive(graph);
                    progress = 1.0f;
                    return graph;
                }
            }
            else if (filePath.extension() == ".json")
            {
                std::ifstream fileStream{filePath, std::ios_base::binary};
                if (fileStream.is_open())
                {
                    algorithm::slam::SlamGraph<utils::definitions::Timestep> graph;
                    cereal::JSONInputArchive archive(fileStream);
                    archive(graph);
                    progress = 1.0f;
                    return graph;
                }
            }
            else if (filePath.extension() == ".xml")
            {
                std::ifstream fileStream{filePath, std::ios_base::binary};
                if (fileStream.is_open())
                {
                    algorithm::slam::SlamGraph<utils::definitions::Timestep> graph;
                    cereal::XMLInputArchive archive(fileStream);
                    archive(graph);
                    progress = 1.0f;
                    return graph;
                }
            }

            return {};
        }

        ///////////////////////////////////////////////////////////////////////////
        void saveGraph(float& progress, const algorithm::slam::SlamGraph<utils::definitions::Timestep>& graph, const std::filesystem::path& filePath)
        {
            std::ofstream fileStream{filePath, std::ios_base::binary};
            if (filePath.extension() == ".cereal")
            {
                if (fileStream.is_open())
                {
                    cereal::PortableBinaryOutputArchive archive(fileStream);
                    archive(graph);
                    progress = 1.0f;
                }
            }
            else if (filePath.extension() == ".json")
            {
                if (fileStream.is_open())
                {
                    cereal::JSONOutputArchive archive(fileStream);
                    archive(graph);
                    progress = 1.0f;
                }
            }
            else if (filePath.extension() == ".xml")
            {
                if (fileStream.is_open())
                {
                    cereal::XMLOutputArchive archive(fileStream);
                    archive(graph);
                    progress = 1.0f;
                }
            }
        }
    }

    ViewportReturnInformation& Viewport(ImTextureID id, std::function<void (size_t, size_t)> callback)
    {
        static ViewportReturnInformation returnInfo{0.0f, 0.0f, 1.0f, false, {1000, 1000}};
        static std::filesystem::path filePath;
        static std::filesystem::path saveFilePath;
        bool fileChanged = false;
        bool saveFile = false;

        if (returnInfo.gridHasChanged)
            returnInfo.gridHasChanged = false;

        ImGui::Begin("Viewport");

        // Select file
        {
            static NFD::UniquePathU8 path;
            static bool didNFDFail = false;
            static const std::string defaultPath = (CMake::programRootDirectory / "resources").string();
            static std::string backupPath;

            if (didNFDFail)
                ImGui::InputText("File", &backupPath);

            fileChanged = ImGui::Button("Select File");
            if (fileChanged && !didNFDFail)
            {
                constexpr size_t amount = 4;
                const nfdfilteritem_t filter[amount] = {{"Log files", "log"}, {"SLAM Data", "cereal"}, {"SLAM Data JSON", "json"}, {"SLAM Data XML", "xml"}};

                nfdresult_t result;
                result = NFD::OpenDialog(path, filter, amount, defaultPath.c_str());

                if (result != nfdresult_t::NFD_OKAY)
                {
                    didNFDFail = true;
                    filePath.clear();
                    backupPath.reserve(255);
                    backupPath = defaultPath;
                }
                else if (path != nullptr)
                {
                    filePath.assign(path.get());
                }
            }
            else if (didNFDFail)
            {
                filePath.assign(backupPath);
            }

            ImGui::SameLine();
            bool saveGraph = ImGui::Button("Save Graph");
            if (saveGraph)
            {
                constexpr size_t amount = 3;
                const nfdfilteritem_t filter[amount] = {{"SLAM Data", "cereal"}, {"SLAM Data JSON", "json"}, {"SLAM Data XML", "xml"}};

                NFD::SaveDialog(path, filter, amount, defaultPath.c_str());

                if (path != nullptr)
                {
                    saveFile = true;
                    saveFilePath.assign(path.get());
                }
            }
            
            if (!filePath.empty())
            {
                const std::string stringPath = filePath.string();
                ImGui::Text(stringPath.c_str());
            }

            ImGui::Separator();
        }

        const bool disableViewer = filePath.empty();
        if (disableViewer)
            ImGui::BeginDisabled();

        // Grid control and mode selection
        {
            static algorithm::slam::SlamGraph<utils::definitions::Timestep> graph;
            static int currentItem = 0;
            static float inputScale = 0.1;
            static utils::AsyncWithProgress<algorithm::slam::SlamGraph<utils::definitions::Timestep>, const std::filesystem::path&> asyncFileLoader{parseFile};
            static utils::AsyncWithProgress<void, const algorithm::slam::SlamGraph<utils::definitions::Timestep>&, const std::filesystem::path&> asyncGraphSaver{saveGraph};

            if ((saveFile || asyncGraphSaver.isRunning()) && !asyncGraphSaver.isFinished())
                ui::ProgressBar("Saving file...", asyncGraphSaver, graph, saveFilePath);

            bool loadingFileFinished = false;
            if (asyncFileLoader.isFinished())
            {
                graph = asyncFileLoader.getResult();
                loadingFileFinished = true;
            }

            if (fileChanged || asyncFileLoader.isRunning())
                ui::ProgressBar("Loading file...", asyncFileLoader, filePath);

            bool selectionChanged = ImGui::Combo("Render modes", &currentItem, "Measurement\0Full map\0Map reconstruction\0");
            bool scaleChanged = ImGui::SliderFloat("Density", &inputScale, .01, 2, "%.2f m/px");
            float scale = 1 / inputScale;

            ImGui::Separator();

            if(graph.nodeSize() != 0)
            {
                const bool shouldUpdateGrid = (scaleChanged || loadingFileFinished);
                switch (currentItem)
                {
                case 1:
                    returnInfo.gridHasChanged = fullMap(returnInfo.grid, graph, scale, shouldUpdateGrid, loadingFileFinished);
                    break;
                case 2:
                case 0: // fall through
                default:
                    returnInfo.gridHasChanged = replay(returnInfo.grid, graph, scale, selectionChanged || shouldUpdateGrid);
                    break;
            }
            }
        }

        // Displaying and resizing viewport
        {
            static ImVec2 viewportSize{0, 0};
            ImVec2 newViewportSize = ImGui::GetContentRegionAvail();

            if (newViewportSize.x != viewportSize.x || newViewportSize.y != viewportSize.y)
                callback(newViewportSize.x, newViewportSize.y);
            else
                ImGui::Image(id, viewportSize);

            viewportSize = newViewportSize;
        }

        // Mouse control
        if (ImGui::IsItemHovered())
        {
            const auto viewportPosition = ImGui::GetItemRectMin();
            const auto viewportSize = ImGui::GetItemRectSize();
            const auto& io = ImGui::GetIO();
            const auto& delta = io.MouseDelta;

            ImGui::SetMouseCursor(ImGuiMouseCursor_Hand);

            float mouseSpeed = 0.001;
            float wheelSpeed = 0.1;
            if (ImGui::IsKeyDown(ImGuiKey_ModShift))
            {
                mouseSpeed = 0.0001;
                wheelSpeed = 0.01;
            }

            if (ImGui::IsMouseReleased(ImGuiMouseButton_Right))
            {
                returnInfo.x = 0;
                returnInfo.y = 0;
                returnInfo.z = 1;
            }


            if (ImGui::IsMouseDown(ImGuiMouseButton_Left))
            {
                returnInfo.x -= delta.x * mouseSpeed * returnInfo.z;
                returnInfo.y += delta.y * mouseSpeed * returnInfo.z;
            }

            if (io.MouseWheel != 0)
            {
                const float mouseWheel = io.MouseWheel * wheelSpeed;
                const float clampedMouseWheel = std::clamp(mouseWheel, -1.0f, 1.0f);
                returnInfo.x += (io.MousePos.x - viewportPosition.x - viewportSize.x / 2) / viewportSize.x * clampedMouseWheel;
                returnInfo.y -= (io.MousePos.y - viewportPosition.y - viewportSize.y / 2) / viewportSize.y * clampedMouseWheel;
                returnInfo.z = std::clamp(returnInfo.z - mouseWheel, 0.01f, 1000.0f);
            }
        }

        if (disableViewer)
            ImGui::EndDisabled();

        ImGui::End();

        return returnInfo;
    }
}
