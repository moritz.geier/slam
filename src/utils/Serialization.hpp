#pragma once

// algorithm
#include <algorithm/slam/SlamTypes.hpp>

// cereal
#include <cereal/types/chrono.hpp>
#include <cereal/types/vector.hpp>

// std
#include <stdint.h>

#include "Definitions.hpp"

namespace cereal
{
    ///////////////////////////////////////////////////////////////////////////
    template<typename Archive, int Dimensions>
    void serialize(Archive& archive, Eigen::Vector<float, Dimensions>& vector)
    {
        if (Dimensions > 0)
        {
            for (auto* pData = vector.data(); pData < vector.data() + Dimensions; pData++)
                archive(*pData);
        }
        else
        {
            throw std::runtime_error{"[cereal::serialize] Dynamic eigen vectors are currently not implemented to be serialized."};
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    template<typename Archive, int Dimensions>
    void serialize(Archive& archive, Eigen::Transform<float, Dimensions, Eigen::AffineCompact>& matrix)
    {
        if (Dimensions > 0)
        {
            constexpr size_t size = Dimensions * (Dimensions + 1);
            for (auto* pData = matrix.data(); pData < matrix.data() + size; pData++)
                archive(*pData);
        }
        else
        {
            throw std::runtime_error{"[cereal::serialize] Dynamic eigen vectors are currently not implemented to be serialized."};
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    template<typename Archive>
    void serialize(Archive& archive, utils::definitions::Timestep& step)
    {
        archive(step.measurements, step.pose, step.timestamp);
    }

    ///////////////////////////////////////////////////////////////////////////
    template<typename Archive>
    void serialize(Archive& archive, algorithm::slam::EdgeValue& value)
    {
        archive(value.measurement, value.relativeTransformation, value.from, value.rmse);
    }
}