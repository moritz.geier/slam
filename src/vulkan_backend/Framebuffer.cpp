#include "Framebuffer.hpp"
#include "LogicalDevice.hpp"
#include "RenderPass.hpp"
#include "ImageView.hpp"

///////////////////////////////////////////////////////////////////////////
Framebuffer::Framebuffer(const LogicalDevice& logicalDevice, const RenderPass& renderPass, const ImageView& image, const vk::Extent2D& extent)
{
    vk::FramebufferCreateInfo framebufferInfo{};
    framebufferInfo.sType = vk::StructureType::eFramebufferCreateInfo;
    framebufferInfo.renderPass = renderPass.get();
    framebufferInfo.attachmentCount = 1;
    framebufferInfo.pAttachments = &image.get();
    framebufferInfo.width = extent.width;
    framebufferInfo.height = extent.height;
    framebufferInfo.layers = 1;

    framebuffer = logicalDevice->createFramebufferUnique(framebufferInfo);
}

///////////////////////////////////////////////////////////////////////////
Framebuffer::Framebuffer(Framebuffer&& moveFramebuffer)
{
    framebuffer = std::move(moveFramebuffer.framebuffer);
}

///////////////////////////////////////////////////////////////////////////
Framebuffer& Framebuffer::operator=(Framebuffer&& moveFramebuffer)
{
    if (&moveFramebuffer != this)
        framebuffer = std::move(moveFramebuffer.framebuffer);

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Framebuffer::~Framebuffer()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::Framebuffer& Framebuffer::get() const
{
    return framebuffer.get();
}

///////////////////////////////////////////////////////////////////////////
vk::Framebuffer& Framebuffer::get()
{
    return framebuffer.get();
}

///////////////////////////////////////////////////////////////////////////
Framebuffers::Framebuffers(const LogicalDevice& device, const RenderPass& renderPass, const ImageViews& images, const vk::Extent2D& extent)
: std::vector<Framebuffer>{}
{
    reserve(images.size());

    for (const auto& image : images)
        emplace_back(device, renderPass, image, extent);
}