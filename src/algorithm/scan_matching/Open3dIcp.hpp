#pragma once

// std
#include <algorithm>
#include <memory>

// utils
#include <utils/Definitions.hpp>

#include "IScanMatching.hpp"

///////////////////////////////////////////////////////////////////////////
namespace algorithm
{
    ///////////////////////////////////////////////////////////////////////////
    class Open3dIcp : public IScanMatching<utils::definitions::Timestep>
    {
    private:
        using StepType = utils::definitions::Timestep;
    public:
        struct Configuration
        {
            float threshold;
        };

        Open3dIcp(const Configuration& configuration);
        virtual ~Open3dIcp();

        /// @brief Aligns two pointcloud and returns how you have to transform `pointsToMatch` to align them with `referencePoints`.
        /// @param pointsToMatch
        /// @param referencePoints
        /// @param initialAlignment
        /// @return
        IScanMatching<StepType>::AlignmentResult findAlignment(IScanMatching<StepType>::PointcloudType pointsToMatch, const IScanMatching<StepType>::PointcloudType& referencePoints, const IScanMatching<StepType>::TransformationType& initialAlignment) override;

    protected:
        float threshold;
    };
}