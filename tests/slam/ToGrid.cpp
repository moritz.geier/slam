// catch2
#include <catch2/catch_test_macros.hpp>

// cmake
#include <CMakeVariables.hpp>

// math
#include <math/Graph.hpp>

// slam
#include <algorithm/slam/ToGrid.hpp>

// std
#include <filesystem>

// utils
#include <utils/BMP.hpp>
#include <utils/LogReader.hpp>

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Converting a LogReaderStep into a grid.", "[slam::toGrid(const Step&)]" )
{
    const auto baseDirectory{std::filesystem::current_path() / "to_grid"};
    std::filesystem::create_directories(baseDirectory);

    utils::LogReader logReader{CMake::programRootDirectory / "resources/aces-building.log"};
    auto firstStep = logReader.nextStep();
    REQUIRE(firstStep.has_value());

    algorithm::slam::ToGridConfiguration<uint8_t> config;
    config.freeSpace = 255;
    config.unknownSpace = 127;
    config.occupiedSpace = 0;
    config.size = 1000;
    config.scale = 10;

    SECTION("Single Step.")
    {

        auto grid = algorithm::slam::toGrid<uint8_t>(firstStep.value(), config);
        utils::BMP bmp{grid};
        bmp.setResolution(500);
        bmp.save(baseDirectory / "togrid.bmp");
    }

    SECTION("Single line.")
    {
        utils::definitions::Timestep step;
        step.measurements.emplace_back(5, 5);

        auto grid = algorithm::slam::toGrid<uint8_t>(step, config);
        utils::BMP bmp{grid};
        bmp.setResolution(500);
        bmp.save(baseDirectory / "togrid_single_line.bmp");
    }
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Converting a graph into a grid.", "[slam::toGrid(const Graph&)]" )
{
    struct Timestep
    {
        std::vector<utils::definitions::Point2D<float>> measurements;
        Eigen::AffineCompact2f pose;
    };

    const auto baseDirectory{std::filesystem::current_path() / "to_grid"};
    std::filesystem::create_directories(baseDirectory);

    algorithm::slam::ToGridConfiguration<uint8_t> config;
    config.freeSpace = 255;
    config.unknownSpace = 127;
    config.occupiedSpace = 0;
    config.size = 1000;
    config.scale = 10;

    math::Graph<Timestep, void*> graph;

    SECTION("Straight graph.")
    {
        auto& firstNode = graph.addNode({{{5, 5}}, {}});
        firstNode->pose.setIdentity();
        auto& secondNode = graph.addNode({{{5, 5}}, {}});
        secondNode->pose.setIdentity();
        secondNode->pose(1, 2) = 3.0f;
        graph.addEdge(nullptr, firstNode, secondNode);

        auto grid = algorithm::slam::toGrid<uint8_t>(graph, config);
        utils::BMP bmp{grid};
        bmp.setResolution(500);
        bmp.save(baseDirectory / "togrid_straight_graph.bmp");
    }

    SECTION("Cyclic graph.")
    {
        auto& firstNode = graph.addNode({{{5, 5}}, {}});
        firstNode->pose.setIdentity();
        auto& secondNode = graph.addNode({{{5, 5}}, {}});
        secondNode->pose.setIdentity();
        secondNode->pose(1, 2) = 3.0f;
        auto& thirdNode = graph.addNode({{{5, 5}}, {}});
        thirdNode->pose.setIdentity();
        thirdNode->pose(1, 2) = 6.0f;

        graph.addEdge(nullptr, firstNode, secondNode);
        graph.addEdge(nullptr, thirdNode, secondNode);
        graph.addEdge(nullptr, firstNode, thirdNode);

        auto grid = algorithm::slam::toGrid<uint8_t>(graph, config);
        utils::BMP bmp{grid};
        bmp.setResolution(500);
        bmp.save(baseDirectory / "togrid_cyclic_graph.bmp");
    }

    SECTION("Loop graph.")
    {
        auto& firstNode = graph.addNode({{{5, 5}}, {}});
        firstNode->pose.setIdentity();
        auto& secondNode = graph.addNode({{{5, 5}}, {}});
        secondNode->pose.setIdentity();
        secondNode->pose(1, 2) = 3.0f;
        auto& thirdNode = graph.addNode({{{5, 5}}, {}});
        thirdNode->pose.setIdentity();
        thirdNode->pose(1, 2) = 6.0f;

        graph.addEdge(nullptr, firstNode, secondNode);
        graph.addEdge(nullptr, firstNode, firstNode);
        graph.addEdge(nullptr, thirdNode, secondNode);
        graph.addEdge(nullptr, thirdNode, thirdNode);

        auto grid = algorithm::slam::toGrid<uint8_t>(graph, config);
        utils::BMP bmp{grid};
        bmp.setResolution(500);
        bmp.save(baseDirectory / "togrid_loop_graph.bmp");
    }
}