# SLAM

This is a student project, done as part of a bachelor thesis.

## Installation
First, we will install all dependencies, you'll need
- [CMake](https://cmake.org/download/)
- [Ninja](https://github.com/ninja-build/ninja/releases) or an equivalent build system
- A compiler with good C++20 support, our recommendation is GCC 13 either via [MSYS2](https://www.msys2.org/) for Windows or your packet manager of choice for Linux
- [Vulkan LunarG SDK](https://vulkan.lunarg.com/sdk/home), during installation, select to also install the GLM headers.

Then clone the repo.
```
git clone --recursive https://gitlab.com/moritz.geier/slam.git
```

To build the program you can have to navigate into the folder of the git repo and execute the following commands.
Note: If you are on Windows, you might have to add the path to the binary directory of Ninja and the compiler to `Environment Variables -> PATH`
```
mkdir build
cd build
cmake -GNinja ..
cmake --build .
```

We can now run the application.
```
./src/slam_visulazier
```

## Troubleshooting
First of all, you can run our test suite to detect errors.
```
./tests/test
```
If they failed, please post them into an issue and detail your problems and setup.