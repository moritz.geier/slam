#include "ViewportReplay.hpp"

// imgui
#include <imgui.h>

// math
#include <math/Covariance.hpp>
#include <math/Graph.hpp>

// slam
#include <algorithm/slam/ToGrid.hpp>
#include <algorithm/slam/SlamTypes.hpp>

// utils
#include <utils/Definitions.hpp>

///////////////////////////////////////////////////////////////////////////
namespace ui
{
    namespace
    {
        const utils::definitions::Timestep& atIndex(const algorithm::slam::SlamGraph<utils::definitions::Timestep>::NodeContainer& nodes, size_t index)
        {
            auto pCurrent = nodes.begin();
            while(index != 0)
            {
                pCurrent++;
                index--;
            }
            return (*pCurrent)->get();
        }
    }
    ///////////////////////////////////////////////////////////////////////////
    bool replay(algorithm::slam::Grid<ViewportGridColorMode>& grid, const algorithm::slam::SlamGraph<utils::definitions::Timestep>& graph, float scale, bool recalculateGrid)
    {
        static size_t timestep = 1;
        static const std::string playText = "Play";
        static const std::string pauseText = "Pause";
        static std::string playMovementText{playText};
        static bool shouldPlayMovement = false;
        static Eigen::Matrix2f covariance = Eigen::Matrix2f::Zero();

        if (shouldPlayMovement && timestep < graph.nodeSize())
        {
            timestep++;
        }
        else
        {
            shouldPlayMovement = false;
            playMovementText = playText;
        }

        if (ImGui::Button(playMovementText.c_str()))
        {
            shouldPlayMovement = !shouldPlayMovement;
            playMovementText = shouldPlayMovement ? pauseText : playText;
        }

        ImGui::SameLine();
        ImGui::SliderInt("Timeframe", reinterpret_cast<int*>(&timestep), 1, graph.nodeSize() - 1);

        ImGui::Text("Covariance Matrix: ");
        ImGui::SameLine();

        constexpr auto covarianceFlags =  ImGuiTableFlags_BordersOuterV | ImGuiTableFlags_SizingFixedSame | ImGuiTableFlags_NoHostExtendX;
        if (ImGui::BeginTable("Covariance", 2, covarianceFlags))
        {
            for (const auto row : covariance.rowwise())
            {
                for (const auto element : row)
                {
                    ImGui::TableNextColumn();
                    ImGui::Text(std::to_string(element).c_str());
                }
            }
            ImGui::EndTable();
        }

        bool hasGridChanged = false;
        static size_t lastTimestep = 0;
        if (lastTimestep != timestep || recalculateGrid)
        {
            const auto config = getGridConfiguration(scale);

            lastTimestep = timestep;
            const auto& stepValue = atIndex(graph.getNodes(), timestep);
            grid = algorithm::slam::toGrid(stepValue, config);

            covariance = math::calculateCovariance(stepValue.measurements);
            hasGridChanged = true;
        }
        return hasGridChanged;
    }
}