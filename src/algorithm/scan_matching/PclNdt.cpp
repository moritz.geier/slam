#include "PclNdt.hpp"

// pcl
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/registration/ndt_2d.h>


///////////////////////////////////////////////////////////////////////////
algorithm::PclNdt::PclNdt(const Configuration& configuration)
: threshold{configuration.threshold}
{
}

///////////////////////////////////////////////////////////////////////////
algorithm::PclNdt::~PclNdt()
{
}

///////////////////////////////////////////////////////////////////////////
algorithm::IScanMatching<utils::definitions::Timestep>::AlignmentResult algorithm::PclNdt::findAlignment(IScanMatching<StepType>::PointcloudType pointsToMatch, const IScanMatching<StepType>::PointcloudType& referencePoints, const IScanMatching<StepType>::TransformationType& initialAlignment)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr source{new pcl::PointCloud<pcl::PointXYZ>};
    source->reserve(pointsToMatch.size());
    for (const auto& point : pointsToMatch)
        source->push_back({point(0), point(1), 0.0f});

    pcl::PointCloud<pcl::PointXYZ>::Ptr target{new pcl::PointCloud<pcl::PointXYZ>};
    target->reserve(referencePoints.size());
    for (const auto& point : referencePoints)
        target->push_back({point(0), point(1), 0.0f});

    Eigen::Matrix4f initialAlignment3d;
    initialAlignment3d.setIdentity();

    pcl::NormalDistributionsTransform2D<pcl::PointXYZ, pcl::PointXYZ> ndt;
    ndt.setInputSource(target);
    ndt.setInputTarget(source);
    ndt.align(*source, initialAlignment3d);
    Eigen::Matrix4f transformation = ndt.getFinalTransformation();

    algorithm::IScanMatching<StepType>::AlignmentResult result;
    result.transformMatrix.matrix().block<2, 2>(0, 0) = transformation.block<2, 2>(0, 0);
    result.transformMatrix.matrix().block<2, 1>(0, 2) = transformation.block<2, 1>(0, 3);
    result.error = static_cast<float>(ndt.getFitnessScore());
    return result;
}
