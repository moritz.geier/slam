#pragma once

// std
#include <limits>
#include <memory>

namespace algorithm::slam
{
    enum class GridLayout : bool
    {
        eRowMajor,
        eColumnMajor
    };

    template<class T, GridLayout Layout = GridLayout::eRowMajor>
    class Grid
    {
    public:
        Grid();
        Grid(size_t width, size_t height);
        Grid(Grid<T, Layout>&& grid);
        Grid& operator=(Grid<T, Layout>&& grid);
        Grid(const Grid<T, Layout>& grid);
        Grid& operator=(const Grid<T, Layout>& grid);
        ~Grid();

        inline size_t size() const;
        inline size_t width() const;
        inline size_t height() const;
        inline size_t empty() const;

        inline const T& at(size_t x, size_t y) const;
        inline T& at(size_t x, size_t y);

        void resize(size_t width, size_t height);
        /// @brief Fill grid with a certain value.
        /// @param value To which each cell should be set.
        void fill(const T& value);
        inline void insert(const size_t x, const size_t y, const T& value);
        inline void insertSafe(const size_t x, const size_t y, const T& value);
        void insert(size_t x, size_t y, const Grid& grid);

        const T* data() const;
        T* data();

    private:
        size_t _width;
        size_t _height;
        std::unique_ptr<T[]> _data;
    };
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
algorithm::slam::Grid<T, Layout>::Grid()
: _width{0}
, _height{0}
, _data{}
{
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
algorithm::slam::Grid<T, Layout>::Grid(size_t width, size_t height)
: _width{width}
, _height{height}
, _data{std::make_unique<T[]>(width * height)}
{
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
algorithm::slam::Grid<T, Layout>::Grid(Grid<T, Layout>&& grid)
: _width{grid._width}
, _height{grid._height}
, _data{std::move(grid._data)}
{
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
algorithm::slam::Grid<T, Layout>& algorithm::slam::Grid<T, Layout>::operator=(Grid<T, Layout>&& grid)
{
    if(this != &grid)
    {
        _width = grid._width;
        _height = grid._height;
        _data = std::move(grid._data);
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
algorithm::slam::Grid<T, Layout>::Grid(const Grid<T, Layout>& grid)
: _width{grid._width}
, _height{grid._height}
, _data{std::make_unique<T[]>(_width * _height)}
{
    for(size_t i = 0; i < grid.size(); i++)
        _data[i] = grid._data[i];
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
algorithm::slam::Grid<T, Layout>& algorithm::slam::Grid<T, Layout>::operator=(const Grid<T, Layout>& grid)
{
    if(this != &grid)
    {
        _width = grid._width;
        _height = grid._height;
        _data = std::make_unique<T[]>(_width * _height);
        for(size_t i = 0; i < grid.size(); i++)
            _data[i] = grid._data[i];
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
algorithm::slam::Grid<T, Layout>::~Grid()
{
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
size_t algorithm::slam::Grid<T, Layout>::size() const
{
    return _width * _height;
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
size_t algorithm::slam::Grid<T, Layout>::width() const
{
    return _width;
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
size_t algorithm::slam::Grid<T, Layout>::height() const
{
    return _height;
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
size_t algorithm::slam::Grid<T, Layout>::empty() const
{
    return _width == 0 || _height == 0;
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
const T& algorithm::slam::Grid<T, Layout>::at(size_t x, size_t y) const
{
    if constexpr (Layout == GridLayout::eColumnMajor)
        return _data[x * _height + y];
    else
        return _data[y * _width + x];
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
T& algorithm::slam::Grid<T, Layout>::at(size_t x, size_t y)
{
    if constexpr (Layout == GridLayout::eColumnMajor)
        return _data[x * _height + y];
    else
        return _data[y * _width + x];
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
void algorithm::slam::Grid<T, Layout>::resize(size_t width, size_t height)
{
    if (width == _width && height == _height)
        return;

    std::unique_ptr<T[]> newData{std::make_unique<T[]>(height * width)};

    const size_t elementsToCopyHeight = std::min(_height, height);
    const size_t elementsToCopyWidth = std::min(_width, width);

    if constexpr (Layout == GridLayout::eColumnMajor)
    {
        for(size_t x = 0; x < elementsToCopyWidth; x++)
        {
            for(size_t y = 0; y < elementsToCopyHeight; y++)
                newData[x * height + y] = _data[x * _height + y];

            for(size_t y = _height; y < height; y++)
                newData[x * height + y] = T{};
        }
    }
    else
    {
        for(size_t y = 0; y < elementsToCopyHeight; y++)
        {
            for(size_t x = 0; x < elementsToCopyWidth; x++)
                newData[x + y * width] = _data[x + y * _width];

            for(size_t x = _width; x < width; x++)
                newData[x + y * width] = T{};
        }
    }

    _data = std::move(newData);

    _width = width;
    _height = height;
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
void algorithm::slam::Grid<T, Layout>::fill(const T& value)
{
    std::fill(_data.get(), _data.get() + _width * _height, value);
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
void algorithm::slam::Grid<T, Layout>::insert(const size_t x, const size_t y, const T& value)
{
    T& element = at(x, y);
    element = value;
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
void algorithm::slam::Grid<T, Layout>::insertSafe(const size_t x, const size_t y, const T& value)
{
    if (x >= _width || y >= _height)
        return;

    insert(x, y, value);
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
void algorithm::slam::Grid<T, Layout>::insert(size_t x, size_t y, const Grid& grid)
{
    if(grid.height() + y > _height || grid.width() + x > _width)
        resize(std::max(grid.width() + x, _width), std::max(grid.height() + y, _height));

    if constexpr (Layout == GridLayout::eColumnMajor)
    {
        for (size_t currentX = 0; currentX < grid.width(); currentX++)
        {
            for (size_t currentY = 0; currentY < grid.height(); currentY++)
            {
                insert(x + currentX, y + currentY, grid.at(currentX, currentY));
            }
        }
    }
    else
    {
        for (size_t currentY = 0; currentY < grid.height(); currentY++)
        {
            for (size_t currentX = 0; currentX < grid.width(); currentX++)
            {
                insert(x + currentX, y + currentY, grid.at(currentX, currentY));
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
const T* algorithm::slam::Grid<T, Layout>::data() const
{
    return _data.get();
}

///////////////////////////////////////////////////////////////////////////
template<class T, algorithm::slam::GridLayout Layout>
T* algorithm::slam::Grid<T, Layout>::data()
{
    return _data.get();
}