#include "Instance.hpp"
#include "CMakeVariables.hpp"

// glfw
#include <GLFW/glfw3.h>

///////////////////////////////////////////////////////////////////////////
Instance::Instance(const std::string& name, const std::vector<const char*>& validationLayers)
{
    vk::ApplicationInfo appInfo{};
    appInfo.sType = vk::StructureType::eApplicationInfo;
    appInfo.pApplicationName = name.c_str();
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engien";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    vk::InstanceCreateInfo createInfo{};
    createInfo.sType = vk::StructureType::eInstanceCreateInfo;
    createInfo.pApplicationInfo = &appInfo;
    createInfo.enabledExtensionCount = glfwExtensionCount;
    createInfo.ppEnabledExtensionNames = glfwExtensions;

    if constexpr (CMake::isDebug)
    {
        createInfo.enabledLayerCount = validationLayers.size();
        createInfo.ppEnabledLayerNames = validationLayers.data();
    }
    else
    {
        createInfo.enabledLayerCount = 0;
    }

    instance = vk::createInstanceUnique(createInfo);
}

///////////////////////////////////////////////////////////////////////////
Instance::Instance(Instance&& moveCommandPool)
{
    instance = std::move(moveCommandPool.instance);
}

///////////////////////////////////////////////////////////////////////////
Instance& Instance::operator=(Instance&& moveCommandPool)
{
    if (&moveCommandPool != this)
        instance = std::move(moveCommandPool.instance);

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Instance::~Instance()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::Instance& Instance::get() const
{
    return instance.get();
}

///////////////////////////////////////////////////////////////////////////
vk::Instance& Instance::get()
{
    return instance.get();
}

///////////////////////////////////////////////////////////////////////////
const vk::UniqueInstance& Instance::operator->() const
{
    return instance;
}

///////////////////////////////////////////////////////////////////////////
vk::UniqueInstance& Instance::operator->()
{
    return instance;
}