#include "LogReader.hpp"
#include "File.hpp"
#include "String.hpp"

// std
#include <cmath>
#include <numbers>
#include <string>

///////////////////////////////////////////////////////////////////////////
utils::LogReader::LogReader(const std::filesystem::path& log)
: lastTimestamp{0}
{
    logFile = openFile(log);
}

///////////////////////////////////////////////////////////////////////////
utils::LogReader::~LogReader()
{
    logFile.close();
}

///////////////////////////////////////////////////////////////////////////
std::optional<utils::definitions::Timestep> utils::LogReader::nextStep()
{
    std::string line;
    std::optional<definitions::Timestep> step = std::nullopt;

    while (!step.has_value() && std::getline(logFile, line))
    {
        step = parseLine(line, lastTimestamp);
    }

    return step;
}

///////////////////////////////////////////////////////////////////////////
std::vector<utils::definitions::Timestep> utils::LogReader::allSteps(const std::filesystem::path& log, float& progress)
{
    auto logFile = openFile(log);
    std::string file = file::readAll(logFile);
    auto lines = string::split(file, "\n");
    std::chrono::microseconds lastTimestamp{0};

    const float progressIncrease = 1 / static_cast<float>(lines.size());
    std::vector<definitions::Timestep> steps;
    steps.reserve(lines.size());
    for (const auto& line : lines)
    {
        auto step = parseLine(line, lastTimestamp);
        if (step.has_value())
            steps.push_back(step.value());
        progress += progressIncrease;
    }

    return steps;
}

///////////////////////////////////////////////////////////////////////////
std::vector<utils::definitions::Timestep> utils::LogReader::allSteps(const std::filesystem::path& log)
{
    float temp = 0;
    return allSteps(log, temp);
}

///////////////////////////////////////////////////////////////////////////
std::ifstream utils::LogReader::openFile(const std::filesystem::path& log)
{
    if (log.extension() != ".log")
        throw std::runtime_error{"[LogReader] Log file should have a .log extension, file: " + log.string()};

    if (!std::filesystem::exists(log))
        throw std::runtime_error{"[LogReader] Log file " + log.string() + " does not exist."};

    std::ifstream logFile;
    logFile.open(log);
    if (!logFile.is_open())
        throw std::runtime_error{"[LogReader] Could not open file: " + log.string()};

    return logFile;
}

///////////////////////////////////////////////////////////////////////////
std::optional<utils::definitions::Timestep> utils::LogReader::parseLine(const std::string& line, std::chrono::microseconds& lastTimestamp)
{
    definitions::Timestep step;

    if (line.starts_with("FLASER"))
    {
        auto splitLine = string::split(line, " ");

        if(splitLine.size() < 2)
            return std::nullopt;

        const size_t measurementsCount = std::stoul(splitLine[1]);
        const size_t entryCount = splitLine.size();
        if (entryCount != measurementsCount + 11)
            return std::nullopt;

        auto timestampSeconds = std::chrono::duration<float>(std::stof(splitLine[entryCount - 1]));
        step.timestamp = std::chrono::duration_cast<std::chrono::microseconds>(timestampSeconds);
        if(lastTimestamp >= step.timestamp)
            return std::nullopt;
        lastTimestamp = step.timestamp;
        step.id = splitLine[entryCount - 3];

        const auto x = std::stof(splitLine[entryCount - 6]);
        const auto y = std::stof(splitLine[entryCount - 5]);
        step.pose.translation() = Eigen::Vector2f{x, y};
        const auto theta = std::stof(splitLine[entryCount - 4]);
        step.pose.linear() = Eigen::Matrix2f{Eigen::Rotation2Df{theta}};

        // Needed since the first measurement isn't at 0 deg but rather at -90 deg
        const float offset = static_cast<float>(measurementsCount == 360 ? -std::numbers::pi : -std::numbers::pi / 2);
        const float thetaIncrement = static_cast<float>(measurementsCount == 360 ? 2 * std::numbers::pi / measurementsCount : std::numbers::pi / measurementsCount);
        step.measurements.reserve(measurementsCount);
        for (size_t i = 0; i < measurementsCount; i++)
        {
            const float measurement = std::stof(splitLine[i + 2]);
            if (measurement >= 50)
                continue;

            const float xDelta = measurement * std::cos(offset + thetaIncrement * i);
            const float yDelta = measurement * std::sin(offset + thetaIncrement * i);
            step.measurements.emplace_back(xDelta, yDelta);
        }

        return step;
    }

    return std::nullopt;
}