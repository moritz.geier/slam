// catch2
#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

// slam
#include <algorithm/nearest_neighbor/KdTree.hpp>
#include <algorithm/nearest_neighbor/BruteForce.hpp>

// std
#include <random>

namespace
{
    ///////////////////////////////////////////////////////////////////////////
    auto createDataPoints(size_t count)
    {
        std::vector<algorithm::DefaultSearch<2>> data;
        std::random_device randomDevice;
        std::mt19937 generator{randomDevice()};
        std::uniform_real_distribution<> distribution{-100, 100};
        data.resize(count);

        for (auto& entry : data)
        {
            entry.coordinate.x() = distribution(generator);
            entry.coordinate.y() = distribution(generator);
        }

        return data;
    }
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Benchmarking different nearest neighbor algorithm", "[!benchmark][nearest_neighbor]")
{
    {
        const Eigen::Vector2f input{{10.0f, 5.0f}};
        const auto data{createDataPoints(10)};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;
        algorithm::BruteForceNearestNeighbor<algorithm::DefaultSearch<2>> bruteForce;

        BENCHMARK("Build KDTree[10]")
        {
            return tree.set(data);
        };

        BENCHMARK("Build BruteForce[10]")
        {
            return bruteForce.set(data);
        };

        const auto nearestKdTree = tree.find(input);
        const auto nearestBruteForce = bruteForce.find(input);
        CHECK(nearestBruteForce.pNeighbor->coordinate == nearestKdTree.pNeighbor->coordinate);

        BENCHMARK("KDTree[10]")
        {
            return tree.find(input);
        };

        BENCHMARK("BruteForce[10]")
        {
            return bruteForce.find(input);
        };
    }

    {
        const Eigen::Vector2f input{{10.0f, 5.0f}};
        const auto data{createDataPoints(180)};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;
        algorithm::BruteForceNearestNeighbor<algorithm::DefaultSearch<2>> bruteForce;

        BENCHMARK("Build KDTree[180]")
        {
            return tree.set(data);
        };

        BENCHMARK("Build BruteForce[180]")
        {
            return bruteForce.set(data);
        };

        const auto nearestKdTree = tree.find(input);
        const auto nearestBruteForce = bruteForce.find(input);
        CHECK(nearestBruteForce.pNeighbor->coordinate == nearestKdTree.pNeighbor->coordinate);

        BENCHMARK("KDTree[180]")
        {
            return tree.find(input);;
        };

        BENCHMARK("BruteForce[180]")
        {
            return bruteForce.find(input);;
        };
    }

    {
        const Eigen::Vector2f input{{10.0f, 5.0f}};
        const auto data{createDataPoints(1'000)};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;
        algorithm::BruteForceNearestNeighbor<algorithm::DefaultSearch<2>> bruteForce;

        BENCHMARK("Build KDTree[1k]")
        {
            return tree.set(data);
        };

        BENCHMARK("Build BruteForce[1k]")
        {
            return bruteForce.set(data);
        };

        const auto nearestKdTree = tree.find(input);
        const auto nearestBruteForce = bruteForce.find(input);
        CHECK(nearestBruteForce.pNeighbor->coordinate == nearestKdTree.pNeighbor->coordinate);

        BENCHMARK("KDTree[1k]")
        {
            return tree.find(input);;
        };

        BENCHMARK("BruteForce[1k]")
        {
            return bruteForce.find(input);;
        };
    }

    {
        const Eigen::Vector2f input{{10.0f, 5.0f}};
        const auto data{createDataPoints(1'000'000)};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;
        algorithm::BruteForceNearestNeighbor<algorithm::DefaultSearch<2>> bruteForce;

        tree.set(data);
        bruteForce.set(data);

        const auto nearestKdTree = tree.find(input);
        const auto nearestBruteForce = bruteForce.find(input);
        CHECK(nearestBruteForce.pNeighbor->coordinate == nearestKdTree.pNeighbor->coordinate);

        BENCHMARK("KDTree[1M]")
        {
            return tree.find(input);
        };

        // On Xeon X5660 was brute force by mean factor ~25 slower than KDTree
    }
}