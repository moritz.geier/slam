#include "LogicalDevice.hpp"
#include "CommandBuffer.hpp"
#include "Fence.hpp"
#include "PhysicalDevice.hpp"
#include "Semaphore.hpp"
#include "Surface.hpp"

///////////////////////////////////////////////////////////////////////////
LogicalDevice::LogicalDevice(const PhysicalDevice& physicalDevice, const Surface& surface, const std::vector<const char*>& extensions)
: queuePriorities{1.0}
{
    auto queueFamilies = physicalDevice.getQueueFamilies(surface);

    if (!queueFamilies.isComplete())
        throw std::runtime_error{"[LogicalDevice] Selected physical device does not poses all necessary queue family"};

    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos{};
    for (auto queueFamilyIndex : queueFamilies.toSet())
    {
        vk::DeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = vk::StructureType::eDeviceQueueCreateInfo;
        queueCreateInfo.queueFamilyIndex = queueFamilyIndex;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriorities;

        queueCreateInfos.push_back(queueCreateInfo);
    }

    vk::PhysicalDeviceFeatures deviceFeatures{};

    vk::DeviceCreateInfo createInfo{};
    createInfo.sType = vk::StructureType::eDeviceCreateInfo;
    createInfo.pQueueCreateInfos = queueCreateInfos.data();
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pEnabledFeatures = &deviceFeatures;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();

    device = physicalDevice->createDeviceUnique(createInfo);
    graphicsQueue = device->getQueue(queueFamilies.graphics.value(), 0);
    presentationQueue = device->getQueue(queueFamilies.presentation.value(), 0);
}

///////////////////////////////////////////////////////////////////////////
LogicalDevice::LogicalDevice(LogicalDevice&& moveLogicalDevice)
{
    device = std::move(moveLogicalDevice.device);
    queuePriorities = moveLogicalDevice.queuePriorities;
    graphicsQueue = std::move(moveLogicalDevice.graphicsQueue);
    presentationQueue = std::move(moveLogicalDevice.presentationQueue);
}

///////////////////////////////////////////////////////////////////////////
LogicalDevice& LogicalDevice::operator=(LogicalDevice&& moveLogicalDevice)
{
    if (&moveLogicalDevice != this)
    {
        device = std::move(moveLogicalDevice.device);
        queuePriorities = moveLogicalDevice.queuePriorities;
        graphicsQueue = std::move(moveLogicalDevice.graphicsQueue);
        presentationQueue = std::move(moveLogicalDevice.presentationQueue);
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
LogicalDevice::~LogicalDevice()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::Device& LogicalDevice::get() const
{
    return device.get();
}

///////////////////////////////////////////////////////////////////////////
vk::Device& LogicalDevice::get()
{
    return device.get();
}

///////////////////////////////////////////////////////////////////////////
const vk::UniqueDevice& LogicalDevice::operator->() const
{
    return device;
}

///////////////////////////////////////////////////////////////////////////
vk::UniqueDevice& LogicalDevice::operator->()
{
    return device;
}

///////////////////////////////////////////////////////////////////////////
const vk::Queue& LogicalDevice::getGraphicsQueue() const
{
    return graphicsQueue;
}

///////////////////////////////////////////////////////////////////////////
vk::Queue& LogicalDevice::getGraphicsQueue()
{
    return graphicsQueue;
}

///////////////////////////////////////////////////////////////////////////
void LogicalDevice::submit(const std::vector<vk::CommandBuffer>& buffers, const Fence& setAfter, const Semaphore& waitBefore, const Semaphore& signalAfter) const
{
    std::vector<vk::PipelineStageFlags> waitStages{vk::PipelineStageFlagBits::eColorAttachmentOutput};

    vk::SubmitInfo submitInfo{};
    submitInfo.sType = vk::StructureType::eSubmitInfo;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &waitBefore.get();
    submitInfo.pWaitDstStageMask = waitStages.data();
    submitInfo.commandBufferCount = static_cast<uint32_t>(buffers.size());
    submitInfo.pCommandBuffers = buffers.data();
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &signalAfter.get();

    vk::Result result = graphicsQueue.submit(1, &submitInfo, setAfter.get());
    if (result != vk::Result::eSuccess)
        throw std::runtime_error{"[LogicalDevice] Failed to submit draw command buffer!"};
}

///////////////////////////////////////////////////////////////////////////
bool LogicalDevice::present(const uint32_t imageIndex, const std::vector<vk::SwapchainKHR>& swapchains, const Semaphore& waitBefore) const
{
    vk::PresentInfoKHR presentInfo{};
    presentInfo.sType = vk::StructureType::ePresentInfoKHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &waitBefore.get();
    presentInfo.swapchainCount = static_cast<uint32_t>(swapchains.size());
    presentInfo.pSwapchains = swapchains.data();
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr;

    vk::Result result = presentationQueue.presentKHR(&presentInfo);

    if (result == vk::Result::eSuboptimalKHR || result == vk::Result::eErrorOutOfDateKHR)
        return false;
    if (result != vk::Result::eSuccess)
        throw std::runtime_error{"[LogicalDevice] Failed to present frame!"};
    return true;
}

///////////////////////////////////////////////////////////////////////////
void LogicalDevice::waitForIdle() const
{
    device->waitIdle();
}