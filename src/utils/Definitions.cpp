#include "Definitions.hpp"

bool utils::definitions::GrayScale::operator==(const GrayScale& grayScale) const
{
    return value == grayScale.value;
}

///////////////////////////////////////////////////////////////////////////
utils::definitions::GrayScale utils::definitions::GrayScale::operator+(const GrayScale& grayScale) const
{
    int newValue = value + static_cast<int>(grayScale.value);
    const int cappedValue = std::min(newValue, 255);
    return {static_cast<uint8_t>(cappedValue)};
}

///////////////////////////////////////////////////////////////////////////
utils::definitions::GrayScale utils::definitions::GrayScale::operator-(const GrayScale& grayScale) const
{
    const int newValue = value - static_cast<int>(grayScale.value);
    const int cappedValue = std::max(newValue, 0);
    return {static_cast<uint8_t>(cappedValue)};
}

///////////////////////////////////////////////////////////////////////////
utils::definitions::GrayScale utils::definitions::GrayScale::operator*(float intensity) const
{
    const auto scaledValue = std::abs(value * intensity);
    return {static_cast<uint8_t>(scaledValue)};
}

///////////////////////////////////////////////////////////////////////////
bool utils::definitions::RGB::operator==(const RGB& rgb) const
{
    return red == rgb.red && green == rgb.green && blue == rgb.blue;
}

///////////////////////////////////////////////////////////////////////////
utils::definitions::RGB utils::definitions::RGB::operator+(const RGB& rgb) const
{
    constexpr float weight = 0.5f;
    uint16_t newRed = red + rgb.red;
    uint16_t newGreen = green + rgb.green;
    uint16_t newBlue = blue + rgb.blue;
    return {static_cast<uint8_t>(newRed * weight), static_cast<uint8_t>(newGreen * weight), static_cast<uint8_t>(newBlue * weight)};
}

///////////////////////////////////////////////////////////////////////////
utils::definitions::RGB utils::definitions::RGB::operator-(const RGB& rgb) const
{
    constexpr float weight = 0.5f;
    int newRed = std::max(static_cast<int>(red) - static_cast<int>(rgb.red), 0);
    int newGreen = std::max(static_cast<int>(green) - static_cast<int>(rgb.green), 0);
    int newBlue = std::max(static_cast<int>(blue) - static_cast<int>(rgb.blue), 0);
    return {static_cast<uint8_t>(newRed * weight), static_cast<uint8_t>(newGreen * weight), static_cast<uint8_t>(newBlue * weight)};
}

///////////////////////////////////////////////////////////////////////////
utils::definitions::RGB utils::definitions::RGB::operator*(float intensity) const
{
    const RGB background{255, 255, 255};
    const float invertedIntensity = 1 - intensity;
    uint8_t newRed = static_cast<uint8_t>(invertedIntensity * background.red + (intensity * red));
    uint8_t newGreen = static_cast<uint8_t>(invertedIntensity * background.green + (intensity * green));
    uint8_t newBlue = static_cast<uint8_t>(invertedIntensity * background.blue + (intensity * blue));
    return {newRed, newGreen, newBlue};
}

///////////////////////////////////////////////////////////////////////////
bool utils::definitions::RGBA::operator==(const RGBA& rgba) const
{
    return red == rgba.red && green == rgba.green && blue == rgba.blue && alpha == rgba.alpha;
}

///////////////////////////////////////////////////////////////////////////
utils::definitions::RGBA utils::definitions::RGBA::operator+(const RGBA& foreground) const
{
    const float foregroundAlpha = foreground.alpha / 255.0f;
    const float backgroundAlpha = alpha / 255.0f;

    const float newAlpha = 1 - (1 - foregroundAlpha) * (1 - backgroundAlpha);

    const float foregroundAlphaScale = foregroundAlpha / newAlpha;
    const float backgroundAlphaScale = backgroundAlpha * (1 - foregroundAlpha) / newAlpha;

    float newRed = foreground.red * foregroundAlphaScale + red * backgroundAlphaScale;
    float newGreen = foreground.green * foregroundAlphaScale + green * backgroundAlphaScale;
    float newBlue = foreground.blue * foregroundAlphaScale + blue * backgroundAlphaScale;

    return {static_cast<uint8_t>(newRed), static_cast<uint8_t>(newGreen), static_cast<uint8_t>(newBlue), static_cast<uint8_t>(newAlpha * 255)};
}

///////////////////////////////////////////////////////////////////////////
utils::definitions::RGBA utils::definitions::RGBA::operator-(const RGBA& foreground) const
{
    const float foregroundAlpha = foreground.alpha / 255.0f;
    const float backgroundAlpha = alpha / 255.0f;

    const float newAlpha = 1 - (1 - foregroundAlpha) * (1 - backgroundAlpha);

    const float foregroundAlphaScale = foregroundAlpha / newAlpha;
    const float backgroundAlphaScale = backgroundAlpha * (1 - foregroundAlpha) / newAlpha;

    float newRed = foreground.red * foregroundAlphaScale - red * backgroundAlphaScale;
    float newGreen = foreground.green * foregroundAlphaScale - green * backgroundAlphaScale;
    float newBlue = foreground.blue * foregroundAlphaScale - blue * backgroundAlphaScale;

    newRed = std::max(newRed, 0.0f);
    newGreen = std::max(newGreen, 0.0f);
    newBlue = std::max(newBlue, 0.0f);

    return {static_cast<uint8_t>(newRed), static_cast<uint8_t>(newGreen), static_cast<uint8_t>(newBlue), static_cast<uint8_t>(newAlpha * 255)};
}

///////////////////////////////////////////////////////////////////////////
utils::definitions::RGBA utils::definitions::RGBA::operator*(float intensity) const
{
    return {red, green, blue, static_cast<uint8_t>(intensity * 255)};
}