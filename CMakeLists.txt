cmake_minimum_required(VERSION 3.10)

project(slam_visulazier CXX)

find_package(Vulkan REQUIRED)

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# Set build type and set build type variable
if (NOT CMAKE_BUILD_TYPE)
    message("Build Type not set, defaulting to Debug...")
    set(CMAKE_BUILD_TYPE Debug)
endif()

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    set(IS_DEBUG true)
else()
    set(IS_DEBUG false)
endif()

# GLFW
set(GLFW_DIR ${CMAKE_SOURCE_DIR}/libraries/glfw) # Set this to point to an up-to-date GLFW repo
option(GLFW_BUILD_EXAMPLES "Build the GLFW example programs" OFF)
option(GLFW_BUILD_TESTS "Build the GLFW test programs" OFF)
option(GLFW_BUILD_DOCS "Build the GLFW documentation" OFF)
option(GLFW_INSTALL "Generate installation target" OFF)
option(GLFW_DOCUMENT_INTERNALS "Include internals in documentation" OFF)
add_subdirectory(${GLFW_DIR} binary_dir EXCLUDE_FROM_ALL)
include_directories(${GLFW_DIR}/include)

# ImGui
set(IMGUI_DIR ${CMAKE_SOURCE_DIR}/libraries/imgui)
include_directories(${IMGUI_DIR} ${IMGUI_DIR}/backends)

set(
IMGUI_SOURCES
    ${IMGUI_DIR}/imgui.cpp
    ${IMGUI_DIR}/imgui_draw.cpp
    ${IMGUI_DIR}/imgui_demo.cpp
    ${IMGUI_DIR}/imgui_tables.cpp
    ${IMGUI_DIR}/imgui_widgets.cpp
    ${IMGUI_DIR}/misc/cpp/imgui_stdlib.cpp
)

set(
IMGUI_BACKEND
    ${IMGUI_DIR}/backends/imgui_impl_glfw.cpp
    ${IMGUI_DIR}/backends/imgui_impl_vulkan.cpp
)

add_library(imgui ${IMGUI_SOURCES})
add_library(imgui_backend ${IMGUI_BACKEND})
target_link_libraries(imgui_backend glfw Vulkan::Vulkan)

# Catch2
add_subdirectory(${CMAKE_SOURCE_DIR}/libraries/Catch2)

# Native File Dialog
add_subdirectory(${CMAKE_SOURCE_DIR}/libraries/nativefiledialog-extended)

# Eigen
add_compile_definitions(-DEIGEN_DONT_VECTORIZE)
add_subdirectory(${CMAKE_SOURCE_DIR}/libraries/eigen)
include_directories(${CMAKE_SOURCE_DIR}/libraries/eigen)
find_package(Eigen3 REQUIRED)

# OpenMP
find_package(OpenMP)
if (OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

# cereal
include_directories(${CMAKE_SOURCE_DIR}/libraries/cereal/include)

# rl
add_definitions(-D_USE_MATH_DEFINES)
find_package(RL COMPONENTS MATH REQUIRED)
link_libraries(${RL_LIBRARIES})

# nlopt
find_package(nlopt REQUIRED)
link_libraries(${NLOPT_LIBRARIES})

# flann
include_directories(${CMAKE_SOURCE_DIR}/libraries/flann/src/cpp)

# PCL
find_package(PCL 1.14 REQUIRED registration)

# Application
if (NOT CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    add_compile_options(-Wall -Wextra -Werror)
else()
    add_compile_options(/bigobj /D_SILENCE_ALL_CXX23_DEPRECATION_WARNINGS)
endif()

set(SLAM_CWD ${CMAKE_SOURCE_DIR})
configure_file(CMakeVariables.hpp.in app_config/CMakeVariables.hpp)

include_directories(${CMAKE_BINARY_DIR}/app_config ${CMAKE_SOURCE_DIR}/src)

add_subdirectory(shaders)
add_subdirectory(src)

# Testing
add_subdirectory(tests)