#pragma once

// vulkan
#include <vulkan/vulkan.hpp>

class Instance;
class Window;

class Surface final
{
public:
    Surface(const Instance& instance, const Window& window);
    Surface(Surface&&);
    Surface& operator=(Surface&&);
    Surface(const Surface&) = delete;
    Surface& operator=(const Surface&) = delete;
    ~Surface();

    const vk::SurfaceKHR& get() const;
    vk::SurfaceKHR& get();

private:
    vk::UniqueSurfaceKHR surface;
    vk::Instance instance;
};