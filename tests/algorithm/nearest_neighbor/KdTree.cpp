// catch2
#include <catch2/catch_test_macros.hpp>

// slam
#include <algorithm/nearest_neighbor/KdTree.hpp>
#include <algorithm/slam/Grid.hpp>

// std
#include <algorithm>
#include <filesystem>
#include <format>
#include <fstream>

// utils
#include <utils/BMP.hpp>

///////////////////////////////////////////////////////////////////////////
namespace
{
    ///////////////////////////////////////////////////////////////////////////
    #define CHECK_ENTRY(actual, expected, input) CHECK(actual.pNeighbor->coordinate == expected); CHECK(actual.distance == (input - expected).norm());

    ///////////////////////////////////////////////////////////////////////////
    const std::vector<algorithm::DefaultSearch<2>>& getSmallPointcloudOne()
    {
        static const std::vector<algorithm::DefaultSearch<2>> pointcloud{
            {{0.0f, 0.0f}}, {{-4.43f, 1.36f}}, {{-2.91f, 4.26f}}, {{-1.73f, 2.8f}}, {{-6.83f, 4.8f}}, {{-5.17f, -5.3f}}, {{-4.37f, -3.6f}}, {{-8.59f, -4.66f}}, {{-2.0f, -1.65f}},
            {{-1.67f, -6.12f}}, {{-8.93f, -3.42f}}, {{-8.79f, -5.4f}}, {{-7.23f, 3.38f}}, {{-7.29f, 5.5f}}, {{-1.33f, 1.88f}}, {{-1.33f, 1.88f}}, {{-1.49f, 4.46f}}, {{4.43f, 1.36f}},
            {{2.91f, 4.26f}}, {{1.73f, 2.8f}}, {{6.83f, 4.8f}}, {{5.17f, -5.3f}}, {{4.37f, -3.6f}}, {{8.59f, -4.66f}}, {{2.0f, -1.65f}}, {{1.67f, -6.12f}}, {{8.93f, -3.42f}},
            {{8.79f, -5.4f}}, {{7.23f, 3.38f}}, {{7.29f, 5.5f}}, {{1.33f, 1.88f}}, {{1.33f, 1.88f}}, {{1.49f, 4.46f}}};
        return pointcloud;
    }

    ///////////////////////////////////////////////////////////////////////////
    const std::vector<algorithm::DefaultSearch<2>>& getSmallPointcloudTwo()
    {
        static const std::vector<algorithm::DefaultSearch<2>> pointcloud{
            {{0.0f, 0.0f}}, {{-4.43f, 1.36f}}, {{-2.91f, 4.26f}}, {{-1.73f, 2.8f}}, {{-6.83f, 4.8f}}, {{-5.17f, -5.3f}}, {{-4.37f, -3.6f}}, {{-8.59f, -4.66f}}, {{-4.37f, -1.65f}},
            {{-1.67f, -6.12f}}, {{-8.93f, -3.42f}}, {{-8.79f, -5.4f}}, {{-7.23f, 3.38f}}, {{-7.29f, 5.5f}}, {{-1.33f, 1.88f}}, {{-1.33f, 1.88f}}, {{-1.49f, 4.46f}}, {{4.43f, 1.36f}},
            {{2.91f, 4.26f}}, {{1.73f, 2.8f}}, {{6.83f, 4.8f}}, {{5.17f, -5.3f}}, {{4.37f, -3.6f}}, {{8.59f, -4.66f}}, {{2.0f, -1.65f}}, {{1.67f, -6.12f}}, {{8.93f, -3.42f}},
            {{8.79f, -5.4f}}, {{7.23f, 3.38f}}, {{7.29f, 5.5f}}, {{1.33f, 1.88f}}, {{1.33f, 1.88f}}, {{1.49f, 4.46f}}};
        return pointcloud;
    }

    ///////////////////////////////////////////////////////////////////////////
    const std::vector<algorithm::DefaultSearch<2>>& getBigPointcloud()
    {
        static const std::vector<algorithm::DefaultSearch<2>> pointcloud{
            {{-88.1211, -13.1871}}, {{-32.0647, -36.2571}}, {{87.1214, 35.9308}}, {{34.0165, 38.422}}, {{1.92705, 95.371}}, {{56.8015, -51.5602}}, {{74.7991, 89.7478}}, {{-40.2447, -98.1588}},
            {{-12.3933, 3.45726}}, {{20.7614, -22.3531}}, {{-51.2279, -93.3468}}, {{-67.8699, 34.5125}}, {{42.3486, 77.7514}}, {{98.2604, -3.99091}}, {{-41.8597, 56.2028}}, {{-38.0837, 11.9689}},
            {{-43.6283, -73.3363}}, {{-68.3526, 15.7759}}, {{63.5617, 11.8706}}, {{88.8379, 12.4079}}, {{-78.0971, 20.2568}}, {{26.6026, -62.618}}, {{-75.5415, 79.0287}}, {{-61.0049, -87.2615}},
            {{89.7732, 38.9346}}, {{-46.8544, -79.332}}, {{11.0774, -77.445}}, {{79.4193, -29.3389}}, {{-54.4624, -28.2555}}, {{-12.2628, -54.0608}}, {{92.5891, 6.71188}}, {{-43.9541, 97.2957}},
            {{79.2031, -81.7499}}, {{-7.98156, -57.0241}}, {{62.2683, 14.2177}}, {{-2.83163, 24.0032}}, {{-45.4462, -93.7554}}, {{-23.6903, 62.9096}}, {{-95.8341, -52.3924}}, {{-63.3464, -37.0142}},
            {{80.2069, 78.351}}, {{-17.1072, -88.5669}}, {{35.457, 80.5761}}, {{-95.2186, -47.9947}}, {{-67.4074, -62.6163}}, {{87.2108, -30.6795}}, {{-92.2109, -53.8985}}, {{96.4297, -21.4592}},
            {{60.4328, -44.6284}}, {{39.1659, -11.8338}}, {{66.6606, 70.1727}}, {{-96.8171, 72.2735}}, {{-37.193, 92.5009}}, {{85.8486, 77.2804}}, {{-85.861, -88.5045}}, {{35.4815, 7.43947}},
            {{-70.1703, 82.2016}}, {{-15.521, 41.7433}}, {{-86.2663, 23.2743}}, {{-45.0829, -46.196}}, {{-99.4757, -74.4361}}, {{-60.8769, -52.0402}}, {{48.1975, 78.6066}}, {{-26.0573, -68.2078}},
            {{-15.582, 69.6115}}, {{-34.2321, 47.9252}}, {{-26.8492, 7.09667}}, {{37.8582, 81.3507}}, {{3.57662, 43.3788}}, {{-73.0109, -12.3228}}, {{72.8576, -64.7444}}, {{-4.98409, 42.9283}},
            {{82.3538, -20.97}}, {{37.2305, -73.6083}}, {{-90.4896, -15.3527}}, {{80.1683, -32.8578}}, {{60.8357, 46.7525}}, {{-10.8451, -20.969}}, {{-49.3321, 87.779}}, {{-53.5803, -35.5895}},
            {{-6.68278, 53.1325}}, {{-60.4718, -10.9676}}, {{-83.4752, -40.8617}}, {{68.0263, 24.6344}}, {{-96.6748, -95.8103}}, {{40.0334, 25.0897}}, {{-65.5281, 84.5559}}, {{21.1504, 56.7906}},
            {{84.6936, -75.2281}}, {{-16.8365, -21.5766}}, {{-72.1508, 27.1859}}, {{81.7217, 30.7119}}, {{-79.8121, -60.8908}}, {{-14.3297, 26.8178}}, {{4.34197, -92.9699}}, {{-36.6361, 13.2294}},
            {{-42.3886, 0.914465}}, {{-54.2257, -63.6524}}, {{-57.1773, -15.9743}}, {{43.6173, -77.1743}}, {{-84.2116, 98.6561}}, {{-25.9805, -61.5564}}, {{-47.2104, -4.11405}}, {{-81.3876, -87.0473}},
            {{-35.1536, 25.9591}}, {{-22.97, 2.81608}}, {{-55.2488, -54.3068}}, {{10.829, -28.128}}, {{94.3398, 29.3785}}, {{9.3589, 88.0812}}, {{-44.748, -4.11521}}, {{40.4792, -67.7684}}, 
            {{-15.9584, 55.833}}, {{81.5602, 74.8479}}, {{-21.61, -63.3761}}, {{-98.5004, 27.0822}}, {{54.0157, 78.7418}}, {{-81.675, -87.496}}, {{-0.181374, 16.952}}, {{-60.3223, 54.3448}}, 
            {{20.1258, 80.5076}}, {{46.4819, 94.0062}}, {{-21.896, -85.6302}}, {{-31.7496, 98.3737}}, {{54.4163, 85.5182}}, {{56.8115, 60.3263}}, {{76.4171, 2.04912}}, {{89.9786, -22.6394}}, 
            {{-62.4327, 77.869}}, {{57.4268, 93.0624}}, {{7.64893, -88.2191}}, {{38.7886, 72.7174}}, {{72.3316, 33.6263}}, {{-47.7507, 56.6067}}, {{-76.1797, -33.732}}, {{50.3861, 96.8003}}, 
            {{45.5635, -38.3429}}, {{-16.1665, -64.4837}}, {{-14.7851, -40.7536}}, {{77.7031, -28.7088}}, {{19.3888, -72.7968}}, {{-44.0415, 6.23114}}, {{-36.6887, -18.1545}}, {{-50.7361, 93.1645}}, 
            {{-39.6877, 12.1113}}, {{-34.6222, 10.6769}}, {{-64.7986, 10.7716}}, {{92.299, -17.9808}}, {{32.138, 15.154}}, {{75.4932, 9.90504}}, {{-27.3055, -80.2509}}, {{-52.1096, 88.0055}}, 
            {{74.7412, -56.0089}}, {{85.0421, 87.2096}}, {{17.4848, 79.8463}}, {{96.8329, 94.6158}}, {{23.0138, -32.5209}}, {{27.2835, 10.1857}}, {{-47.9768, 18.7095}}, {{-13.9491, -94.6413}}, 
            {{-99.2113, 77.3164}}, {{-2.89506, -77.1315}}, {{-54.3811, -31.5326}}, {{10.0669, -20.3993}}, {{82.5512, 5.90833}}, {{95.2753, -40.6183}}, {{64.3265, 65.3209}}, {{-11.9066, 89.5239}}, 
            {{94.799, -14.628}}, {{62.5471, 32.5822}}, {{86.7664, -50.4678}}, {{23.5659, -8.62495}}, {{-6.4257, -43.7303}}, {{73.1238, -95.2027}}, {{-25.4601, -14.4839}}, {{-69.9437, 22.2565}}, 
            {{11.2456, 35.778}}, {{48.988, 75.2232}}, {{48.3584, -88.2027}}, {{70.4035, 44.8447}}};
        return pointcloud;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Red cross is the target, blue point the expected result and the green cross the actual result
    void savePointsToBMP(const std::vector<algorithm::DefaultSearch<2>>& points, const Eigen::Vector2f& target, const Eigen::Vector2f& expectedResult, const Eigen::Vector2f& actualResult, const std::filesystem::path& path)
    {
        const auto xMaxIt = std::max_element(points.begin(), points.end(), [](const auto& first, const auto& second) { return first.coordinate.x() < second.coordinate.x(); });
        const auto yMaxIt = std::max_element(points.begin(), points.end(), [](const auto& first, const auto& second) { return first.coordinate.y() < second.coordinate.y(); });
        const auto xMinIt = std::min_element(points.begin(), points.end(), [](const auto& first, const auto& second) { return first.coordinate.x() < second.coordinate.x(); });
        const auto yMinIt = std::min_element(points.begin(), points.end(), [](const auto& first, const auto& second) { return first.coordinate.y() < second.coordinate.y(); });
        const float scale = 10.0f;
        const size_t xDelta = static_cast<size_t>((xMaxIt->coordinate.x() - xMinIt->coordinate.x() + 2) * scale);
        const size_t yDelta = static_cast<size_t>((yMaxIt->coordinate.y() - yMinIt->coordinate.y() + 2) * scale);
        const Eigen::Vector2f center{xDelta / 2, yDelta / 2};
        algorithm::slam::Grid<utils::definitions::RGB> grid{xDelta, yDelta};
        grid.fill({255, 255, 255});

        for (const auto& point : points)
        {
            const auto newPoint = point.coordinate * scale + center;
            grid.insertSafe(static_cast<size_t>(newPoint.x()), static_cast<size_t>(newPoint.y()), {0, 0, 0});
        }

        const auto newTarget = target * scale + center;
        grid.insertSafe(static_cast<size_t>(newTarget.x() - 1), static_cast<size_t>(newTarget.y()), {0, 0, 255});
        grid.insertSafe(static_cast<size_t>(newTarget.x() + 1), static_cast<size_t>(newTarget.y()), {0, 0, 255});
        grid.insertSafe(static_cast<size_t>(newTarget.x() ), static_cast<size_t>(newTarget.y() - 1), {0, 0, 255});
        grid.insertSafe(static_cast<size_t>(newTarget.x()), static_cast<size_t>(newTarget.y() + 1), {0, 0, 255});

        const auto newExpectedResult = expectedResult * scale + center;
        grid.insertSafe(static_cast<size_t>(newExpectedResult.x()), static_cast<size_t>(newExpectedResult.y()), {255, 0, 0});

        const auto newActualResult = actualResult * scale + center;
        grid.insertSafe(static_cast<size_t>(newActualResult.x() - 1), static_cast<size_t>(newActualResult.y()), {0, 255, 0});
        grid.insertSafe(static_cast<size_t>(newActualResult.x() + 1), static_cast<size_t>(newActualResult.y()), {0, 255, 0});
        grid.insertSafe(static_cast<size_t>(newActualResult.x() ), static_cast<size_t>(newActualResult.y() - 1), {0, 255, 0});
        grid.insertSafe(static_cast<size_t>(newActualResult.x()), static_cast<size_t>(newActualResult.y() + 1), {0, 255, 0});

        utils::BMP bmp{grid};
        bmp.save(path);
    }

    ///////////////////////////////////////////////////////////////////////////
    void generateGraph(const algorithm::KdTree<algorithm::DefaultSearch<2>>::Node* node, const Eigen::Vector2f& expected, const Eigen::Vector2f& actual, std::ofstream& file, size_t id)
    {
        const size_t childId = 2 * id;
        if (node->smallerChild != nullptr)
            file << std::format("    {} -- {};\n", id, childId + 1);

        if (node->biggerChild != nullptr)
            file << std::format("    {} -- {};\n", id, childId + 2);

        file << std::format("    {} [label=\"({},{})\"];\n", id, node->value.coordinate.x(), node->value.coordinate.y());

        if (expected == node->value.coordinate && actual == node->value.coordinate)
            file << std::format("    {} [color=green]", id);
        else if(expected == node->value.coordinate)
            file << std::format("    {} [color=yellow]", id);
        else if(actual == node->value.coordinate)
            file << std::format("    {} [color=red]", id);


        if (node->smallerChild != nullptr)
            generateGraph(node->smallerChild.get(), expected, actual, file, childId + 1);

        if (node->biggerChild != nullptr)
            generateGraph(node->biggerChild.get(), expected, actual, file, childId + 2);
    }

    ///////////////////////////////////////////////////////////////////////////
    void saveTreeAsDot(const algorithm::KdTree<algorithm::DefaultSearch<2>>& tree, const Eigen::Vector2f& expected, const Eigen::Vector2f& actual, const std::filesystem::path& filename)
    {
        std::ofstream file;
        file.open(filename);

        if (file.is_open())
        {
            file << "graph kdtree {\n";
            generateGraph(tree.data(), expected, actual, file, 0);
            file << "}";
            file.close();
        }
    }
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Finding nearest neighbor through a KD Tree.", "[algorithm::KdTree]")
{
    const std::filesystem::path baseDirectory{std::filesystem::current_path() / "kdtree"};
    std::filesystem::create_directories(baseDirectory);

    SECTION("Empty tree")
    {
        std::vector<algorithm::DefaultSearch<2>> data{};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        const auto* pTreeData = tree.data();
        REQUIRE(pTreeData == nullptr);

        const auto result = tree.find({0.0f, 0.0f});
        REQUIRE(result.pNeighbor == nullptr);
    }

    SECTION("Simple Tree")
    {
        std::vector<algorithm::DefaultSearch<2>> data{{{0.0f, 0.0f}}, {{5.5f, 10.0f}}, {{-5.0f, 20.5f}}};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        const auto* pTreeData = tree.data();
        REQUIRE(pTreeData->value.coordinate.x() == 0.0f);
        REQUIRE(pTreeData->value.coordinate.y() == 0.0f);
        REQUIRE(pTreeData->smallerChild->value.coordinate.x() == -5.0f);
        REQUIRE(pTreeData->biggerChild->value.coordinate.x() == 5.5f);
        REQUIRE(pTreeData->smallerChild->biggerChild == nullptr);
        REQUIRE(pTreeData->biggerChild->smallerChild == nullptr);

        {
            const Eigen::Vector2f input{0.0f, 2.0f};
            const auto actualResult = tree.find(input);
            const Eigen::Vector2f expectedResult{0.0f, 0.0f};
            CHECK_ENTRY(actualResult, expectedResult, input);
        }

        {
            const Eigen::Vector2f input{-15.0f, 10.0f};
            const auto actualResult = tree.find(input);
            const Eigen::Vector2f expectedResult{-5.0f, 20.5f};
            CHECK_ENTRY(actualResult, expectedResult, input);
        }
    }

    SECTION("Even number of entries")
    {
        std::vector<algorithm::DefaultSearch<2>> data{{{0.0f, 0.0f}}, {{5.5f, 10.0f}}, {{-5.0f, 20.5f}}, {{15.0f, 2.5f}}};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        const auto* pTreeData = tree.data();
        REQUIRE(pTreeData->value.coordinate.x() == 5.5f);
        REQUIRE(pTreeData->value.coordinate.y() == 10.0f);
        REQUIRE(pTreeData->smallerChild->value.coordinate.x() == -5.0f);
        REQUIRE(pTreeData->biggerChild->value.coordinate.x() == 15.0f);
        REQUIRE(pTreeData->smallerChild->smallerChild != nullptr);
        REQUIRE(pTreeData->smallerChild->biggerChild == nullptr);
        REQUIRE(pTreeData->smallerChild->smallerChild->value.coordinate.y() == 0.0f);
        REQUIRE(pTreeData->smallerChild->smallerChild->smallerChild == nullptr);
        REQUIRE(pTreeData->biggerChild->smallerChild == nullptr);
    }

    SECTION("Multiple entries")
    {
        std::vector<algorithm::DefaultSearch<2>> data{{{0.0f, 0.0f}}, {{5.5f, 10.0f}}, {{-5.0f, 20.5f}}, {{15.0f, 2.5f}}, {{0.0f, 0.0f}}, {{2.0f, 3.0f}}};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        const auto* pTreeData = tree.data();
        REQUIRE(pTreeData->value.coordinate.x() == 2.0f);
        REQUIRE(pTreeData->value.coordinate.y() == 3.0f);
        REQUIRE(pTreeData->smallerChild->value.coordinate.x() == 0.0f);
        REQUIRE(pTreeData->biggerChild->value.coordinate.x() == 5.5f);
        REQUIRE(pTreeData->smallerChild->smallerChild != nullptr);
        REQUIRE(pTreeData->smallerChild->biggerChild != nullptr);
        REQUIRE(pTreeData->smallerChild->smallerChild->value.coordinate.y() == 0.0f);
        REQUIRE(pTreeData->smallerChild->smallerChild->smallerChild == nullptr);
        REQUIRE(pTreeData->biggerChild->smallerChild != nullptr);
        REQUIRE(pTreeData->biggerChild->smallerChild->value.coordinate.x() == 15.0f);

        {
            const Eigen::Vector2f input{15.0f, 3.0f};
            const auto actualResult = tree.find(input);
            const Eigen::Vector2f expectedResult{15.0f, 2.5f};
            CHECK_ENTRY(actualResult, expectedResult, input);
        }
    }

    SECTION("Complex pointcloud one")
    {
        const auto& data = getSmallPointcloudOne();
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{-5.75f, -2.48f};
            const auto actualResult = tree.find(input);
            const Eigen::Vector2f expectedResult{-4.37f, -3.6f};
            CHECK_ENTRY(actualResult, expectedResult, input);

            savePointsToBMP(data, input, expectedResult, actualResult.pNeighbor->coordinate, baseDirectory / "kdtree_complex_pointcloud_one.bmp");
        }
    }

    SECTION("Complex pointcloud two")
    {
        const auto& data = getSmallPointcloudTwo();
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{-5.75f, -2.48f};
            const auto actualResult = tree.find(input);
            const Eigen::Vector2f expectedResult{-4.37f, -1.65f};
            CHECK_ENTRY(actualResult, expectedResult, input);

            savePointsToBMP(data, input, expectedResult, actualResult.pNeighbor->coordinate, baseDirectory / "kdtree_complex_pointcloud_two.bmp");
        }
    }

    SECTION("180 Entry test")
    {
        const auto& data = getBigPointcloud();
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        const Eigen::Vector2f input{10.0f, 5.0f};
        const auto actualResult = tree.find(input);
        const Eigen::Vector2f expectedResult{-0.181374, 16.952};
        CHECK_ENTRY(actualResult, expectedResult, input);

        saveTreeAsDot(tree, expectedResult, actualResult.pNeighbor->coordinate, baseDirectory / "kd_tree180.dot");
        savePointsToBMP(data, input, expectedResult, actualResult.pNeighbor->coordinate, baseDirectory / "kdtree_180_datapoints.bmp");
    }
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Finding k nearest neighbor through a KD Tree.", "[algorithm::KdTree::findKNearest]")
{
    const std::filesystem::path baseDirectory{std::filesystem::current_path() / "kdtree"};
    std::filesystem::create_directories(baseDirectory);

    SECTION("Empty tree")
    {
        std::vector<algorithm::DefaultSearch<2>> data{};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        const auto* pTreeData = tree.data();
        REQUIRE(pTreeData == nullptr);

        const auto result = tree.find({0.0f, 0.0f}, 5);
        REQUIRE(result.empty());
    }

    SECTION("Simple Tree")
    {
        std::vector<algorithm::DefaultSearch<2>> data{{{0.0f, 0.0f}}, {{5.5f, 10.0f}}, {{-5.0f, 20.5f}}};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{0.0f, 2.0f};
            const auto actualResults = tree.find(input, 3);
            const Eigen::Vector2f expectedResult0{0.0f, 0.0f};
            const Eigen::Vector2f expectedResult1{5.5f, 10.0f};
            const Eigen::Vector2f expectedResult2{-5.0f, 20.5f};
            REQUIRE(actualResults.size() == 3);
            CHECK_ENTRY(actualResults[0], expectedResult0, input);
            CHECK_ENTRY(actualResults[1], expectedResult1, input);
            CHECK_ENTRY(actualResults[2], expectedResult2, input);
        }

        {
            const Eigen::Vector2f input{-15.0f, 10.0f};
            const auto actualResults = tree.find(input, 4);
            const Eigen::Vector2f expectedResult{-5.0f, 20.5f};
            REQUIRE(actualResults.size() == 3);
            CHECK_ENTRY(actualResults[0], expectedResult, input);
        }
    }

    SECTION("Multiple entries")
    {
        std::vector<algorithm::DefaultSearch<2>> data{{{0.0f, 0.0f}}, {{5.5f, 10.0f}}, {{-5.0f, 20.5f}}, {{15.0f, 2.5f}}, {{0.0f, 0.0f}}, {{2.0f, 3.0f}}};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{15.0f, 3.0f};

            {
                const auto actualResult = tree.find(input, 0);
                REQUIRE(actualResult.empty());
            }
            
            {
                const auto actualResults = tree.find(input, 2);
                const Eigen::Vector2f expectedResult0{15.0f, 2.5f};
                const Eigen::Vector2f expectedResult1{5.5f, 10.0f};

                REQUIRE(actualResults.size() == 2);
                CHECK_ENTRY(actualResults[0], expectedResult0, input);
                CHECK_ENTRY(actualResults[1], expectedResult1, input);
            }
        }
    }

    SECTION("Complex pointcloud one")
    {
        const auto& data = getSmallPointcloudOne();
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{-5.75f, -2.48f};
            const auto actualResults = tree.find(input, 3);
            const Eigen::Vector2f expectedResult0{-4.37f, -3.6f};
            const Eigen::Vector2f expectedResult1{-5.17f, -5.3f};
            const Eigen::Vector2f expectedResult2{-8.93f, -3.42f};
            REQUIRE(actualResults.size() == 3);
            CHECK_ENTRY(actualResults[0], expectedResult0, input);
            CHECK_ENTRY(actualResults[1], expectedResult1, input);
            CHECK_ENTRY(actualResults[2], expectedResult2, input);
        }
    }

    SECTION("Complex pointcloud two")
    {
        const auto& data = getSmallPointcloudTwo();
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{-5.75f, -2.48f};
            const auto actualResults = tree.find(input, 3);
            const Eigen::Vector2f expectedResult0{-4.37f, -1.65f};
            const Eigen::Vector2f expectedResult1{-4.37f, -3.6f};
            const Eigen::Vector2f expectedResult2{-5.17f, -5.3f};
            REQUIRE(actualResults.size() == 3);
            CHECK_ENTRY(actualResults[0], expectedResult0, input);
            CHECK_ENTRY(actualResults[1], expectedResult1, input);
            CHECK_ENTRY(actualResults[2], expectedResult2, input);
        }
    }

    SECTION("180 Entry test")
    {
        const auto& data = getBigPointcloud();
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        const Eigen::Vector2f input{10.0f, 5.0f};
        const auto actualResults = tree.find(input, 10);
        const Eigen::Vector2f expectedResult0{-0.181374, 16.952};
        const Eigen::Vector2f expectedResult1{27.2835, 10.1857};
        const Eigen::Vector2f expectedResult2{23.5659, -8.62495};
        const Eigen::Vector2f expectedResult3{-12.3933, 3.45726};
        const Eigen::Vector2f expectedResult4{-2.83163, 24.0032};
        const Eigen::Vector2f expectedResult5{32.138, 15.154};
        const Eigen::Vector2f expectedResult6{10.0669, -20.3993};
        const Eigen::Vector2f expectedResult7{35.4815, 7.43947};
        const Eigen::Vector2f expectedResult8{20.7614, -22.3531};
        const Eigen::Vector2f expectedResult9{11.2456, 35.778};
        REQUIRE(actualResults.size() == 10);
        CHECK_ENTRY(actualResults[0], expectedResult0, input);
        CHECK_ENTRY(actualResults[1], expectedResult1, input);
        CHECK_ENTRY(actualResults[2], expectedResult2, input);
        CHECK_ENTRY(actualResults[3], expectedResult3, input);
        CHECK_ENTRY(actualResults[4], expectedResult4, input);
        CHECK_ENTRY(actualResults[5], expectedResult5, input);
        CHECK_ENTRY(actualResults[6], expectedResult6, input);
        CHECK_ENTRY(actualResults[7], expectedResult7, input);
        CHECK_ENTRY(actualResults[8], expectedResult8, input);
        CHECK_ENTRY(actualResults[9], expectedResult9, input);
    }
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Finding neighbors within a radius through a KD Tree.", "[algorithm::KdTree::findInRadius]")
{
    const std::filesystem::path baseDirectory{std::filesystem::current_path() / "kdtree"};
    std::filesystem::create_directories(baseDirectory);

    SECTION("Empty tree")
    {
        std::vector<algorithm::DefaultSearch<2>> data{};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        const auto result = tree.findInRadius({0.0f, 0.0f}, 5);
        REQUIRE(result.empty());
    }

    SECTION("Simple Tree")
    {
        std::vector<algorithm::DefaultSearch<2>> data{{{0.0f, 0.0f}}, {{5.5f, 10.0f}}, {{-5.0f, 20.5f}}};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{0.0f, 2.0f};
            const auto actualResults = tree.findInRadius(input, 50.0f);
            const Eigen::Vector2f expectedResult0{0.0f, 0.0f};
            const Eigen::Vector2f expectedResult1{5.5f, 10.0f};
            const Eigen::Vector2f expectedResult2{-5.0f, 20.5f};
            REQUIRE(actualResults.size() == 3);
            CHECK_ENTRY(actualResults[0], expectedResult0, input);
            CHECK_ENTRY(actualResults[1], expectedResult1, input);
            CHECK_ENTRY(actualResults[2], expectedResult2, input);
        }

        {
            const Eigen::Vector2f input{-15.0f, 10.0f};
            const auto actualResults = tree.findInRadius(input, 15.0f);
            const Eigen::Vector2f expectedResult{-5.0f, 20.5f};
            REQUIRE(actualResults.size() == 1);
            CHECK_ENTRY(actualResults[0], expectedResult, input);
        }
    }

    SECTION("Multiple entries")
    {
        std::vector<algorithm::DefaultSearch<2>> data{{{0.0f, 0.0f}}, {{5.5f, 10.0f}}, {{-5.0f, 20.5f}}, {{15.0f, 2.5f}}, {{0.0f, 0.0f}}, {{2.0f, 3.0f}}};
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{15.0f, 3.0f};

            {
                const auto actualResult = tree.findInRadius(input, 0.0f);
                REQUIRE(actualResult.empty());
            }

            {
                const auto actualResult = tree.findInRadius(input, -20.0f);
                REQUIRE(actualResult.empty());
            }

            {
                const auto actualResult = tree.findInRadius(input, 0.1f);
                REQUIRE(actualResult.empty());
            }
            
            {
                const Eigen::Vector2f expectedResult0{15.0f, 2.5f};
                const Eigen::Vector2f expectedResult1{5.5f, 10.0f};
                const auto actualResults = tree.findInRadius(input, (input - expectedResult1).norm());

                REQUIRE(actualResults.size() == 2);
                CHECK_ENTRY(actualResults[0], expectedResult0, input);
                CHECK_ENTRY(actualResults[1], expectedResult1, input);
            }
        }
    }

    SECTION("Complex pointcloud one")
    {
        const auto& data = getSmallPointcloudOne();
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{-5.75f, -2.48f};
            const Eigen::Vector2f expectedResult0{-4.37f, -3.6f};
            const Eigen::Vector2f expectedResult1{-5.17f, -5.3f};
            const Eigen::Vector2f expectedResult2{-8.93f, -3.42f};
            const auto actualResults = tree.findInRadius(input, (input - expectedResult2).norm());
            REQUIRE(actualResults.size() == 3);
            CHECK_ENTRY(actualResults[0], expectedResult0, input);
            CHECK_ENTRY(actualResults[1], expectedResult1, input);
            CHECK_ENTRY(actualResults[2], expectedResult2, input);
        }
    }

    SECTION("Complex pointcloud two")
    {
        const auto& data = getSmallPointcloudTwo();
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        {
            const Eigen::Vector2f input{-5.75f, -2.48f};
            const Eigen::Vector2f expectedResult0{-4.37f, -1.65f};
            const Eigen::Vector2f expectedResult1{-4.37f, -3.6f};
            const Eigen::Vector2f expectedResult2{-5.17f, -5.3f};
            const auto actualResults = tree.findInRadius(input, (input - expectedResult2).norm());
            REQUIRE(actualResults.size() == 3);
            CHECK_ENTRY(actualResults[0], expectedResult0, input);
            CHECK_ENTRY(actualResults[1], expectedResult1, input);
            CHECK_ENTRY(actualResults[2], expectedResult2, input);
        }
    }

    SECTION("180 Entry test")
    {
        const auto& data = getBigPointcloud();
        algorithm::KdTree<algorithm::DefaultSearch<2>> tree;

        tree.set(data);

        const Eigen::Vector2f input{10.0f, 5.0f};
        const Eigen::Vector2f expectedResult0{-0.181374, 16.952};
        const Eigen::Vector2f expectedResult1{27.2835, 10.1857};
        const Eigen::Vector2f expectedResult2{23.5659, -8.62495};
        const Eigen::Vector2f expectedResult3{-12.3933, 3.45726};
        const Eigen::Vector2f expectedResult4{-2.83163, 24.0032};
        const Eigen::Vector2f expectedResult5{32.138, 15.154};
        const Eigen::Vector2f expectedResult6{10.0669, -20.3993};
        const Eigen::Vector2f expectedResult7{35.4815, 7.43947};
        const Eigen::Vector2f expectedResult8{20.7614, -22.3531};
        const Eigen::Vector2f expectedResult9{11.2456, 35.778};
        const auto actualResults = tree.findInRadius(input, (input - expectedResult9).norm());
        REQUIRE(actualResults.size() == 10);
        CHECK_ENTRY(actualResults[0], expectedResult0, input);
        CHECK_ENTRY(actualResults[1], expectedResult1, input);
        CHECK_ENTRY(actualResults[2], expectedResult2, input);
        CHECK_ENTRY(actualResults[3], expectedResult3, input);
        CHECK_ENTRY(actualResults[4], expectedResult4, input);
        CHECK_ENTRY(actualResults[5], expectedResult5, input);
        CHECK_ENTRY(actualResults[6], expectedResult6, input);
        CHECK_ENTRY(actualResults[7], expectedResult7, input);
        CHECK_ENTRY(actualResults[8], expectedResult8, input);
        CHECK_ENTRY(actualResults[9], expectedResult9, input);
    }
}