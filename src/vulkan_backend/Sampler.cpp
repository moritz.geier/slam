#include "Sampler.hpp"
#include "LogicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
Sampler::Sampler(const LogicalDevice& logicalDevice)
{
    vk::SamplerCreateInfo samplerInfo{};
    samplerInfo.sType = vk::StructureType::eSamplerCreateInfo;
    samplerInfo.magFilter = vk::Filter::eNearest;
    samplerInfo.minFilter = vk::Filter::eNearest;
    samplerInfo.addressModeU = vk::SamplerAddressMode::eRepeat;
    samplerInfo.addressModeV = vk::SamplerAddressMode::eRepeat;
    samplerInfo.addressModeW = vk::SamplerAddressMode::eRepeat;
    samplerInfo.anisotropyEnable = VK_FALSE;
    samplerInfo.maxAnisotropy = 0;
    samplerInfo.borderColor = vk::BorderColor::eIntOpaqueBlack;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = vk::CompareOp::eAlways;
    samplerInfo.mipmapMode = vk::SamplerMipmapMode::eLinear;
    samplerInfo.mipLodBias = 0.0;
    samplerInfo.minLod = 0.0;
    samplerInfo.maxLod = 0.0;

    sampler = logicalDevice->createSamplerUnique(samplerInfo);
}

///////////////////////////////////////////////////////////////////////////
Sampler::Sampler(Sampler&& moveSampler)
{
    sampler = std::move(moveSampler.sampler);
}

///////////////////////////////////////////////////////////////////////////
Sampler& Sampler::operator=(Sampler&& moveSampler)
{
    if (&moveSampler != this)
        sampler = std::move(moveSampler.sampler);

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Sampler::~Sampler()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::Sampler& Sampler::get() const
{
    return sampler.get();
}

///////////////////////////////////////////////////////////////////////////
vk::Sampler& Sampler::get()
{
    return sampler.get();
}
