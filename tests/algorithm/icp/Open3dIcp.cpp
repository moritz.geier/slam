// catch2
#include <catch2/catch_test_macros.hpp>

// slam
#include <algorithm/scan_matching/Open3dIcp.hpp>
#include <algorithm/slam/Grid.hpp>

// std
#include <filesystem>
#include <vector>

// utils
#include <utils/BMP.hpp>
#include <utils/Definitions.hpp>

///////////////////////////////////////////////////////////////////////////
// Red pixel define the goal area, blue the start, green the actual result
TEST_CASE("Using ICP to align two point clouds.", "[algorithm::Open3dIcp]")
{
    const auto baseDirectory = std::filesystem::current_path() / "icp";
    std::filesystem::create_directories(baseDirectory);

    algorithm::Open3dIcp::Configuration icpConfiguration;
    icpConfiguration.threshold = 50.0f;

    algorithm::Open3dIcp icp{icpConfiguration};

    SECTION("Horizontal parallel lines.")
    {
        const std::vector<Eigen::Vector2f> referencePoints{{5.0f, 2.0f}, {11.0f, 2.0f}, {13.0f, 2.0f}};
        std::vector<Eigen::Vector2f> currentPoints{{5.0f, 5.0f}, {11.0f, 5.0f}, {13.0f, 5.0f}};
        Eigen::Vector2f robotPosition{0.0f, 0.0f};

        algorithm::slam::Grid<utils::definitions::RGB> grid{100, 100};
        grid.fill({0, 0, 0});

        for (const auto& point : referencePoints)
            grid.insert(static_cast<size_t>(point.x()), static_cast<size_t>(point.y()), {0, 0, 255});

        for (const auto& point : currentPoints)
            grid.insert(static_cast<size_t>(point.x()), static_cast<size_t>(point.y()), {255, 0, 0});

        Eigen::AffineCompact2f transfrom;
        transfrom.setIdentity();
        const auto result = icp.findAlignment(currentPoints, referencePoints, transfrom);

        for (const auto& point : currentPoints)
        {
            const auto newPoint = result.transformMatrix * point;
            grid.insertSafe(static_cast<size_t>(newPoint.x()), static_cast<size_t>(newPoint.y()), {0, 255, 0});
        }

        utils::BMP bmp{grid};
        bmp.setResolution(500);
        bmp.save(baseDirectory / "open3d_icp_horizontal_line.bmp");
    }

    SECTION("Vertical parallel lines.")
    {
        const std::vector<Eigen::Vector2f> referencePoints{{5.0f, 2.0f}, {5.0f, 10.0f}, {5.0f, 18.0f}};
        std::vector<Eigen::Vector2f> currentPoints{{7.0f, 3.0f}, {7.0f, 13.0f}, {7.0f, 19.0f}};
        Eigen::Vector2f robotPosition{0.0f, 0.0f};

        algorithm::slam::Grid<utils::definitions::RGB> grid{100, 100};
        grid.fill({0, 0, 0});

        for (const auto& point : referencePoints)
            grid.insert(static_cast<size_t>(point.x()), static_cast<size_t>(point.y()), {0, 0, 255});

        for (const auto& point : currentPoints)
            grid.insert(static_cast<size_t>(point.x()), static_cast<size_t>(point.y()), {255, 0, 0});

        Eigen::AffineCompact2f transfrom;
        transfrom.setIdentity();
        const auto result = icp.findAlignment(currentPoints, referencePoints, transfrom);

        for (const auto& point : currentPoints)
        {
            const auto newPoint = result.transformMatrix * point;
            grid.insertSafe(static_cast<size_t>(newPoint.x()), static_cast<size_t>(newPoint.y()), {0, 255, 0});
        }

        utils::BMP bmp{grid};
        bmp.setResolution(500);
        bmp.save(baseDirectory / "open3d_icp_vertical_line.bmp");
    }

    SECTION("Translated curve.")
    {
        const std::vector<Eigen::Vector2f> referencePoints{{13.0f, 46.0f}, {16.0f, 40.0f}, {21.0f, 33.0f}, {26.0f, 31.0f}, {33.0f, 34.0f}, {39.0f, 48.0f}, {44.0f, 57.0f}, {50.0f, 61.0f}, {57.0f, 63.0f}, {62.0f, 64.0f}, {73.0f, 60.0f}};
        std::vector<Eigen::Vector2f> currentPoints{{34.0f, 19.0f}, {38.0f, 10.0f}, {43.0f, 5.0f}, {47.0f, 4.0f}, {57.0f, 11.0f}, {60.0f, 23.0f}, {71.0f, 34.0f}, {83.0f, 37.0f}, {94.0f, 33.0f}};
        Eigen::Vector2f robotPosition{91.0f, 18.0f};

        algorithm::slam::Grid<utils::definitions::RGB> grid{100, 100};
        grid.fill({0, 0, 0});

        for (const auto& point : referencePoints)
            grid.insert(static_cast<size_t>(point.x()), static_cast<size_t>(point.y()), {0, 0, 255});

        for (const auto& point : currentPoints)
            grid.insert(static_cast<size_t>(point.x()), static_cast<size_t>(point.y()), {255, 0, 0});
        grid.insert(static_cast<size_t>(robotPosition.x()), static_cast<size_t>(robotPosition.y()), {255, 0, 0});

        Eigen::AffineCompact2f transfrom;
        transfrom.setIdentity();
        const auto result = icp.findAlignment(currentPoints, referencePoints, transfrom);

        for (const auto& point : currentPoints)
        {
            const auto newPoint = result.transformMatrix * point;
            grid.insertSafe(static_cast<size_t>(newPoint.x()), static_cast<size_t>(newPoint.y()), {0, 255, 0});
        }
        const auto newPoint = result.transformMatrix * robotPosition;
        grid.insertSafe(static_cast<size_t>(newPoint.x()), static_cast<size_t>(newPoint.y()), {0, 255, 0});

        utils::BMP bmp{grid};
        bmp.setResolution(500);
        bmp.save(baseDirectory / "open3d_icp_translated_curve.bmp");
    }

    SECTION("Rotated and translated curve.")
    {
        const std::vector<Eigen::Vector2f> referencePoints{{13.0f, 46.0f}, {16.0f, 40.0f}, {21.0f, 33.0f}, {26.0f, 31.0f}, {33.0f, 34.0f}, {39.0f, 48.0f}, {44.0f, 57.0f}, {50.0f, 61.0f}, {57.0f, 63.0f}, {62.0f, 64.0f}, {73.0f, 60.0f}};
        std::vector<Eigen::Vector2f> currentPoints{{45.0f, 18.0f}, {57.0f, 12.0f}, {66.0f, 13.0f}, {68.0f, 27.0f}, {67.0f, 40.0f}, {74.0f, 54.0f}, {81.0f, 59.0f}, {91.0f, 62.0f}};
        Eigen::Vector2f robotPosition{91.0f, 18.0f};

        algorithm::slam::Grid<utils::definitions::RGB> grid{100, 100};
        grid.fill({0, 0, 0});

        for (const auto& point : referencePoints)
            grid.insert(static_cast<size_t>(point.x()), static_cast<size_t>(point.y()), {0, 0, 255});

        for (const auto& point : currentPoints)
            grid.insert(static_cast<size_t>(point.x()), static_cast<size_t>(point.y()), {255, 0, 0});
        grid.insert(static_cast<size_t>(robotPosition.x()), static_cast<size_t>(robotPosition.y()), {255, 0, 0});

        Eigen::AffineCompact2f transfrom;
        transfrom.setIdentity();
        const auto result = icp.findAlignment(currentPoints, referencePoints, transfrom);

        for (const auto& point : currentPoints)
        {
            const auto newPoint = result.transformMatrix * point;
            grid.insertSafe(static_cast<size_t>(newPoint.x()), static_cast<size_t>(newPoint.y()), {0, 255, 0});
        }
        const auto newPoint = result.transformMatrix * robotPosition;
        grid.insertSafe(static_cast<size_t>(newPoint.x()), static_cast<size_t>(newPoint.y()), {0, 255, 0});

        utils::BMP bmp{grid};
        bmp.setResolution(500);
        bmp.save(baseDirectory / "open3d_icp_rotated_curve.bmp");
    }
}