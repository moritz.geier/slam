#pragma once

// std
#include <array>
#include <optional>
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>
#include <vulkan_backend/DescriptorPool.hpp>
#include <vulkan_backend/Framebuffer.hpp>
#include <vulkan_backend/Swapchain.hpp>
#include <vulkan_backend/ImageView.hpp>
#include <vulkan_backend/RenderPass.hpp>

#include "CommandBuffer.hpp"

class CommandPool;
class Instance;
class LogicalDevice;
class PhysicalDevice;
class Semaphore;
class Surface;
class Window;

class Gui final
{
public:
    Gui(const Window& window, const Instance& instance, const PhysicalDevice& physicalDevice, const LogicalDevice& logicalDevice, const Surface& surface, const CommandPool& commandPool, const size_t framesInFlight);
    Gui(Gui&&) = delete;
    Gui& operator=(Gui&&) = delete;
    Gui(const Gui&) = delete;
    Gui& operator=(const Gui&) = delete;
    ~Gui();

    const std::array<float, 4>& getClearColor() const;
    std::array<float, 4>& getClearColor();

    const Swapchain& getSwapchain() const;
    Swapchain& getSwapchain();
    void recreateSwapchain(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const Surface& surface, const Window& window);

    std::optional<uint32_t> newFrame(const Semaphore& imageAvailable);
    void demo();

    [[nodiscard]]
    const CommandBuffer& render();
    void recover();

private:
    Swapchain swapchain;
    ImageViews swapchainImages;
    RenderPass renderPass;
    Framebuffers framebuffers;
    std::vector<ui::CommandBuffer> commandBuffers;
    DescriptorPool descriptorPool;
    
    size_t currentFrame;
    std::optional<uint32_t> imageIndex;
    const size_t framesInFlight;

    std::array<float, 4> clearColor;
    bool showDemoWindow;
    bool showAnotherWindow;
};