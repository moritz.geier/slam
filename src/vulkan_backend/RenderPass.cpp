#include "RenderPass.hpp"
#include "LogicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
RenderPass::RenderPass(const LogicalDevice& logicalDevice, const vk::Format& format, const vk::ImageLayout finalLayout)
{
    vk::AttachmentDescription colorAttachment{};
    colorAttachment.format = format;
    colorAttachment.samples = vk::SampleCountFlagBits::e1;
    colorAttachment.loadOp = vk::AttachmentLoadOp::eClear;
    colorAttachment.storeOp = vk::AttachmentStoreOp::eStore;
    colorAttachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    colorAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    colorAttachment.initialLayout = vk::ImageLayout::eUndefined;
    colorAttachment.finalLayout = finalLayout;

    vk::AttachmentReference colorAttachmentRef{};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = vk::ImageLayout::eColorAttachmentOptimal;

    vk::SubpassDescription subpass{};
    subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;

    vk::SubpassDependency dependency{};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency.srcAccessMask = vk::AccessFlagBits::eNone;
    dependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;

    vk::RenderPassCreateInfo renderPassInfo{};
    renderPassInfo.sType = vk::StructureType::eRenderPassCreateInfo;
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &colorAttachment;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    renderPass = logicalDevice->createRenderPassUnique(renderPassInfo);
}

///////////////////////////////////////////////////////////////////////////
RenderPass::RenderPass(RenderPass&& moveRenderPass)
{
    renderPass = std::move(moveRenderPass.renderPass);
}

///////////////////////////////////////////////////////////////////////////
RenderPass& RenderPass::operator=(RenderPass&& moveRenderPass)
{
    if (&moveRenderPass != this)
        renderPass = std::move(moveRenderPass.renderPass);

    return *this;
}

///////////////////////////////////////////////////////////////////////////
RenderPass::~RenderPass()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::RenderPass& RenderPass::get() const
{
    return renderPass.get();
}

///////////////////////////////////////////////////////////////////////////
vk::RenderPass& RenderPass::get()
{
    return renderPass.get();
}