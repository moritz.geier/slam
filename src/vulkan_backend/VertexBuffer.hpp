#pragma once

// std
#include <concepts>
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

#include "LogicalDevice.hpp"
#include "PhysicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
template<class Type>
concept VertexType = requires
{
    { Type::getBindingDescription() } -> std::same_as<vk::VertexInputBindingDescription>;
    { Type::getAttributeDescriptions() } -> std::same_as<std::vector<vk::VertexInputAttributeDescription>>;
};

///////////////////////////////////////////////////////////////////////////
struct EmptyVertex
{
    static vk::VertexInputBindingDescription getBindingDescription() { return {}; };
    static std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions() { return {}; };
};

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
class VertexBuffer final
{
public:
    VertexBuffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, std::vector<Type>&& vertecies);
    VertexBuffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const std::vector<Type>& vertecies);
    VertexBuffer(VertexBuffer<Type>&&);
    VertexBuffer<Type>& operator=(VertexBuffer<Type>&&);
    VertexBuffer(const VertexBuffer<Type>&) = delete;
    VertexBuffer<Type>& operator=(const VertexBuffer<Type>&) = delete;
    ~VertexBuffer();

    const vk::Buffer& get() const;
    vk::Buffer& get();

    size_t size() const;

    void upload() const;

private:
    const LogicalDevice& logicalDevice;
    vk::UniqueBuffer buffer;
    vk::UniqueDeviceMemory memory;

    const size_t bufferSize;
    const std::vector<Type> vertecies;

    void allocateBuffer(const PhysicalDevice& physicalDevice);
};

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
VertexBuffer<Type>::VertexBuffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, std::vector<Type>&& vertecies)
: logicalDevice{logicalDevice}
, bufferSize{sizeof(Type) * vertecies.size()}
, vertecies{std::move(vertecies)}
{
    allocateBuffer(physicalDevice);
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
VertexBuffer<Type>::VertexBuffer(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const std::vector<Type>& vertecies)
: logicalDevice{logicalDevice}
, bufferSize{sizeof(Type) * vertecies.size()}
, vertecies{vertecies}
{
    allocateBuffer(physicalDevice);
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
VertexBuffer<Type>::VertexBuffer(VertexBuffer<Type>&& moveVertexBuffer)
{
    buffer = std::move(moveVertexBuffer.buffer);
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
VertexBuffer<Type>& VertexBuffer<Type>::operator=(VertexBuffer<Type>&& moveVertexBuffer)
{
    if (&moveVertexBuffer != this)
        buffer = std::move(moveVertexBuffer.buffer);
    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
VertexBuffer<Type>::~VertexBuffer()
{
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
const vk::Buffer& VertexBuffer<Type>::get() const
{
    return buffer.get();
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
vk::Buffer& VertexBuffer<Type>::get()
{
    return buffer.get();
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
size_t VertexBuffer<Type>::size() const
{
    return vertecies.size();
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
void VertexBuffer<Type>::upload() const
{
    auto* pBuffer = reinterpret_cast<Type*>(logicalDevice->mapMemory(memory.get(), 0, bufferSize));
    if (pBuffer != nullptr)
    {
        for(size_t i = 0; i < vertecies.size(); i++)
            pBuffer[i] = vertecies[i];
        logicalDevice->unmapMemory(memory.get());
    }
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
void VertexBuffer<Type>::allocateBuffer(const PhysicalDevice& physicalDevice)
{
    vk::BufferCreateInfo createInfo{};
    createInfo.sType = vk::StructureType::eBufferCreateInfo;
    createInfo.usage = vk::BufferUsageFlagBits::eVertexBuffer;
    createInfo.size = bufferSize;
    createInfo.sharingMode = vk::SharingMode::eExclusive;

    buffer = logicalDevice->createBufferUnique(createInfo);

    const auto requirements = logicalDevice->getBufferMemoryRequirements(buffer.get());
    const auto index = physicalDevice.getMemoryTypeIndex(requirements.memoryTypeBits, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

    vk::MemoryAllocateInfo allocateInfo{};
    allocateInfo.sType = vk::StructureType::eMemoryAllocateInfo;
    allocateInfo.allocationSize = requirements.size;
    allocateInfo.memoryTypeIndex = index;

    memory = logicalDevice->allocateMemoryUnique(allocateInfo);
    logicalDevice->bindBufferMemory(buffer.get(), memory.get(), 0);
}