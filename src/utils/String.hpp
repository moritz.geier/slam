#pragma once

// std
#include <string>
#include <vector>

namespace utils::string
{
    /// @brief Splits a string into a vector of string at the delimiter.
    /// @param toSplit The string that should be split.
    /// @param delimiter at which string the split should occur, the delimiter will not be present in the resulting strings.
    /// @return All substrings of `toSplit` that are split at the delimiter. They don't contain the delimiter.
    std::vector<std::string> split(const std::string& toSplit, const std::string& delimiter);
}