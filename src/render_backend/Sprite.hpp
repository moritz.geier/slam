#pragma once

// vulkan
#include <vulkan/vulkan.hpp>
#include <vulkan_backend/Image.hpp>
#include <vulkan_backend/ImageView.hpp>
#include <vulkan_backend/StagingBuffer.hpp>

class CommandPool;
class LogicalDevice;
class PhysicalDevice;

namespace utils::definitions
{
    struct RGBA;
}

namespace render
{
    ///////////////////////////////////////////////////////////////////////////
    class Sprite final
    {
    public:
        using TextureDataType = utils::definitions::RGBA;

        Sprite(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t width, size_t height);
        Sprite(Sprite&&);
        Sprite& operator=(Sprite&&) = delete;
        Sprite(const Sprite&) = delete;
        Sprite& operator=(const Sprite&) = delete;
        ~Sprite();

        const vk::ImageView& get() const;
        vk::ImageView& get();

        const Image& getImage() const;
        Image& getImage();

        const TextureDataType* data() const;
        TextureDataType* data();

        void upload(const CommandPool& commandPool) const;

        size_t size() const;

    private:
        const vk::Format textureFormat;
        const LogicalDevice& logicalDevice;
        vk::Extent2D dimensions;
        StagingBuffer<TextureDataType> stagingBuffer;
        Image texture;
        ImageView textureView;
    };

    ///////////////////////////////////////////////////////////////////////////
    class Sprites : public std::vector<Sprite>
    {
    public:
        Sprites(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t width, size_t height, size_t amount);

        std::vector<vk::ImageView> toImageViews() const;
    };
}
