#pragma once

// std
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>
#include <vulkan_backend/CommandBuffer.hpp>

class CommandPool;
class Framebuffer;
class LogicalDevice;
class RenderPass;
class Swapchain;

namespace ui
{
    ///////////////////////////////////////////////////////////////////////////
    class CommandBuffer : public ::CommandBuffer
    {
    public:
        CommandBuffer(vk::UniqueCommandBuffer&& buffer);
        CommandBuffer(const vk::CommandBuffer& buffer);
        CommandBuffer(CommandBuffer&&);
        CommandBuffer& operator=(CommandBuffer&&);
        CommandBuffer(const CommandBuffer&) = delete;
        CommandBuffer& operator=(const CommandBuffer&) = delete;
        virtual ~CommandBuffer() override;

        static std::vector<CommandBuffer> allocate(const LogicalDevice& device, const CommandPool& commandPool, size_t count);

        void record(const RenderPass& renderPass, const Framebuffer& framebuffer, const Swapchain& swapchain, std::array<float, 4> clearColor);
    };
}