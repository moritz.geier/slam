#include "ImageView.hpp"
#include "Image.hpp"
#include "LogicalDevice.hpp"

///////////////////////////////////////////////////////////////////////////
ImageView::ImageView(const LogicalDevice& logicalDevice, const vk::Format& format, const Image& image)
{
    vk::ImageViewCreateInfo imageViewCreateInfo{};
    imageViewCreateInfo.sType = vk::StructureType::eImageViewCreateInfo;
    imageViewCreateInfo.image = image.get();
    imageViewCreateInfo.viewType = vk::ImageViewType::e2D;
    imageViewCreateInfo.format = format;
    imageViewCreateInfo.components.r = vk::ComponentSwizzle::eIdentity;
    imageViewCreateInfo.components.g = vk::ComponentSwizzle::eIdentity;
    imageViewCreateInfo.components.b = vk::ComponentSwizzle::eIdentity;
    imageViewCreateInfo.components.a = vk::ComponentSwizzle::eIdentity;
    imageViewCreateInfo.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
    imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
    imageViewCreateInfo.subresourceRange.levelCount = 1;
    imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
    imageViewCreateInfo.subresourceRange.layerCount = 1;

    imageView = logicalDevice->createImageViewUnique(imageViewCreateInfo);
}

///////////////////////////////////////////////////////////////////////////
ImageView::ImageView(ImageView&& moveImageView)
{
    imageView = std::move(moveImageView.imageView);
}

///////////////////////////////////////////////////////////////////////////
ImageView& ImageView::operator=(ImageView&& moveImageView)
{
    if (this != &moveImageView)
        imageView = std::move(moveImageView.imageView);

    return *this;
}

///////////////////////////////////////////////////////////////////////////
ImageView::~ImageView()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::ImageView& ImageView::get() const
{
    return imageView.get();
}

///////////////////////////////////////////////////////////////////////////
vk::ImageView& ImageView::get()
{
    return imageView.get();
}

///////////////////////////////////////////////////////////////////////////
ImageViews::ImageViews(const LogicalDevice& device, const vk::Format& format, const Images& images)
: std::vector<ImageView>{}
{
    reserve(images.size());

    for (const auto& image : images)
        push_back(ImageView{device, format, image});
}