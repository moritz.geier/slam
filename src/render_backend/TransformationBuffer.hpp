#pragma once

// glm
#include <glm/glm.hpp>

///////////////////////////////////////////////////////////////////////////
namespace render
{
    struct TransformationBuffer
    {
        glm::mat4 model;
        glm::mat4 view;
        glm::mat4 projection;
    };
}