// catch2
#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

// cmake
#include <CMakeVariables.hpp>

// slam
#include <algorithm/slam/BackendLs.hpp>
#include <algorithm/slam/FrontendSimple.hpp>
#include <algorithm/slam/Slam.hpp>
#include <algorithm/nearest_neighbor/KdTree.hpp>
#include <algorithm/scan_matching/Open3dIcp.hpp>

// utils
#include <utils/BMP.hpp>
#include <utils/DrawPoseGraph.hpp>
#include <utils/LogReader.hpp>
#include <utils/ToGraph.hpp>

#include "../Utils.hpp"

///////////////////////////////////////////////////////////////////////////
namespace
{
    void saveGraphToBmp(const std::filesystem::path& path, const algorithm::slam::SlamGraph<utils::definitions::Timestep>& graph)
    {
        algorithm::slam::Grid<utils::definitions::RGBA> grid{100, 100};
        grid.fill({127, 127, 127, 255});
        utils::poseGraphView(grid, graph, {{255, 255, 0, 255}, {255, 0, 0, 255}, 10.0f});
        utils::BMP bmp{grid};
        bmp.save(path);
    }
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Testing the SLAM Backend algorithm.", "[slam::backend]")
{
    SECTION("Two points graph optimization.")
    {
        const auto baseDirectory = std::filesystem::current_path() / "slam";
        std::filesystem::create_directories(baseDirectory);
        algorithm::slam::SlamGraph<utils::definitions::Timestep> graph;
        float progress = 0;

        Eigen::AffineCompact2f poseOne;
        poseOne.setIdentity();
        Eigen::AffineCompact2f poseTwo;
        poseTwo.setIdentity();
        Eigen::AffineCompact2f measurement;
        measurement.linear() = Eigen::Rotation2D{std::numbers::pi}.toRotationMatrix().cast<float>();
        measurement(0, 2) = 1;

        auto& nodeOne = graph.addNode({{}, poseOne, {}, {}});
        auto& nodeTwo = graph.addNode({{}, poseTwo, {}, {}});

        algorithm::slam::EdgeValue errorOdometryOne{algorithm::slam::MeasurementType::eOdometry, measurement, nodeOne.id(), 0};
        graph.addEdge(errorOdometryOne, nodeOne, nodeTwo);

        algorithm::Open3dIcp::Configuration icpConfig;
        icpConfig.threshold = 50.0f;

        algorithm::slam::SolveConfiguration<utils::definitions::Timestep> config;
        config.backend = algorithm::slam::backendLs<utils::definitions::Timestep>;
        config.frontend = algorithm::slam::frontendSimple<utils::definitions::Timestep>;
        config.pScanMatcher = std::make_unique<algorithm::Open3dIcp>(icpConfig);
        config.runScanMatchingForOdometry = false;
        config.runLoopClosure = false;
        config.runBackend = true;

        algorithm::slam::solve(graph, progress, config);
        CheckMatrixEqual(nodeOne->pose.matrix(), Eigen::AffineCompact2f::Identity().matrix());
        CheckMatrixEqual(nodeTwo->pose.matrix(), measurement.matrix());
    }

    SECTION("Triangle graph optimization.")
    {
        const auto baseDirectory = std::filesystem::current_path() / "slam";
        std::filesystem::create_directories(baseDirectory);
        algorithm::slam::SlamGraph<utils::definitions::Timestep> graph;
        float progress = 0;

        Eigen::AffineCompact2f poseOne;
        poseOne.setIdentity();
        Eigen::AffineCompact2f poseTwo;
        poseTwo.setIdentity();
        poseTwo(0, 2) = 1;
        Eigen::AffineCompact2f poseThree;
        poseThree.setIdentity();
        poseThree(0, 2) = 1;
        poseThree(1, 2) = 1;

        auto& nodeOne = graph.addNode({{}, poseOne, {}, {}});
        auto& nodeTwo = graph.addNode({{}, poseTwo, {}, {}});
        auto& nodeThree = graph.addNode({{}, poseThree, {}, {}});

        Eigen::AffineCompact2f error = nodeThree->pose.inverse() * nodeOne->pose;
        error.translation() = error.translation().normalized() * -2;
        algorithm::slam::EdgeValue errorOdometryOne{algorithm::slam::MeasurementType::eOdometry, nodeOne->pose.inverse() * nodeTwo->pose, nodeOne.id(), 0};
        algorithm::slam::EdgeValue errorOdometryTwo{algorithm::slam::MeasurementType::eOdometry, nodeTwo->pose.inverse() * nodeThree->pose, nodeTwo.id(), 0};
        algorithm::slam::EdgeValue errorLoopClosure{algorithm::slam::MeasurementType::eLoopClosure, error, nodeThree.id(), 0};
        for(auto& element: errorLoopClosure.relativeTransformation.translation())
            element -= 0.2;
        graph.addEdge(errorOdometryOne, nodeOne, nodeTwo);
        graph.addEdge(errorOdometryTwo, nodeTwo, nodeThree);
        graph.addEdge(errorLoopClosure, nodeThree, nodeOne);

        saveGraphToBmp(baseDirectory / "pose_graph_before_backend.bmp", graph);

        algorithm::Open3dIcp::Configuration icpConfig;
        icpConfig.threshold = 50.0f;

        algorithm::slam::SolveConfiguration<utils::definitions::Timestep> config;
        config.backend = algorithm::slam::backendLs<utils::definitions::Timestep>;
        config.frontend = algorithm::slam::frontendSimple<utils::definitions::Timestep>;
        config.pScanMatcher = std::make_unique<algorithm::Open3dIcp>(icpConfig);
        config.runScanMatchingForOdometry = false;
        config.runLoopClosure = false;
        config.runBackend = true;

        algorithm::slam::solve(graph, progress, config);

        saveGraphToBmp(baseDirectory / "pose_graph_after_backend.bmp", graph);
    }
}