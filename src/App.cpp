#include "App.hpp"

// imgui
#include <imgui_impl_vulkan.h>

// std
#include <iostream>
#include <optional>

// ui
#include "ui/Benchmark.hpp"
#include "ui/Viewport.hpp"


///////////////////////////////////////////////////////////////////////////
App::App()
// Initialization order is important!
: window{height, width, name}
, instance{name, {"VK_LAYER_KHRONOS_validation"}}
, surface{instance, window}
, physicalDevice{instance, surface, {VK_KHR_SWAPCHAIN_EXTENSION_NAME}}
, logicalDevice{physicalDevice, surface, {VK_KHR_SWAPCHAIN_EXTENSION_NAME}}
, commandPool{logicalDevice, physicalDevice.getQueueFamilies(surface).graphics.value()}
, descriptorPool{logicalDevice}
, sampler{logicalDevice}
, gui{window, instance, physicalDevice, logicalDevice, surface, commandPool, framesInFlight}
, renderer{physicalDevice, logicalDevice, sampler, commandPool, descriptorPool, framesInFlight, windowSize}
{
    synchronisation.reserve(framesInFlight);
    for (size_t i = 0; i < framesInFlight; i++)
        synchronisation.emplace_back(logicalDevice);
    currentFrame = 0;
    
    shouldResize = false;
    window.onResize([this](Window&, size_t, size_t) { shouldResize = true; });
}

///////////////////////////////////////////////////////////////////////////
App::~App()
{
}

///////////////////////////////////////////////////////////////////////////
void App::run()
{
    std::vector<ImTextureID> viewportIds;
    viewportIds.reserve(framesInFlight);

    const auto recreateRenderer = [this, &viewportIds](size_t x, size_t y) {
        renderer.recreate(x, y);
        viewportIds.clear();
        for (auto& imageView : renderer.getImageViews())
            viewportIds.push_back(ImGui_ImplVulkan_AddTexture(sampler.get(), imageView.get(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL));
    };

    algorithm::slam::Grid<ui::ViewportGridColorMode> initGrid{1024, 1024};
    initGrid.fill({10, 10, 10, 255});
    renderer.updateSprite(initGrid, commandPool);

    while(!window.shouldClose())
    {
        window.pollEvents();

        // Draw code
        auto& currentSync = synchronisation[currentFrame];
        currentSync.inFlight.wait();
        currentSync.inFlight.reset();

        auto aquiredImageIndex = gui.newFrame(currentSync.imageAvailable);
        if (aquiredImageIndex.has_value())
        {

            // Gui
            float x = 0;
            float y = 0;
            float z = 0;
            try
            {
                ui::Benchmark();

                #pragma GCC diagnostic push
                #pragma GCC diagnostic ignored "-Wdangling-reference"
                const auto& viewportInfo = ui::Viewport(viewportIds[currentFrame], recreateRenderer);
                #pragma GCC diagnostic pop

                if (viewportInfo.gridHasChanged)
                    renderer.updateSprite(viewportInfo.grid, commandPool);

                x = viewportInfo.x;
                y = viewportInfo.y;
                z = viewportInfo.z;

            }
            catch (std::exception& ex)
            {
                std::cout << "UI loop failed with: " << ex.what() << std::endl;
                gui.recover();
            }

            const auto& uiCommandBuffer = gui.render();

            // Renderer
            auto& rendererCommandBuffer = renderer.render(x, y, z);

            // Submit work
            uint32_t imageIndex = aquiredImageIndex.value();
            logicalDevice.submit({rendererCommandBuffer.get(), uiCommandBuffer.get()}, currentSync.inFlight, currentSync.imageAvailable, currentSync.renderFinished);

            // Present UI
            auto& uiSwapchain = gui.getSwapchain();
            bool didPresent = logicalDevice.present(imageIndex, {uiSwapchain.get()}, currentSync.renderFinished);
            currentFrame = (currentFrame + 1) % framesInFlight;

            if (!didPresent || shouldResize)
            {
                recreateSwapchain();
                shouldResize = false;
            }
        }
        else
        {
            recreateSwapchain();
        }
    }

    logicalDevice.waitForIdle();
}

///////////////////////////////////////////////////////////////////////////
void App::recreateSwapchain()
{
    if (window.getHeight() == 0 || window.getWidth() == 0)
        while(window.isMinimized());

    logicalDevice.waitForIdle();

    gui.recreateSwapchain(logicalDevice, physicalDevice, surface, window);
}

///////////////////////////////////////////////////////////////////////////
App::Syncronisation::Syncronisation(const LogicalDevice& device)
: inFlight{device, true}
, imageAvailable{device}
, renderFinished{device}
{
}