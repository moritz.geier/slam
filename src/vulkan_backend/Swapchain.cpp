#include "Swapchain.hpp"
#include "LogicalDevice.hpp"
#include "PhysicalDevice.hpp"
#include "Semaphore.hpp"
#include "Surface.hpp"
#include "Window.hpp"

// glfw
#include <GLFW/glfw3.h>

// std
#include <limits>

///////////////////////////////////////////////////////////////////////////
Swapchain::Swapchain(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const Surface& surface, const Window& window)
{
    create(logicalDevice, physicalDevice, surface, window);
}

///////////////////////////////////////////////////////////////////////////
Swapchain::Swapchain(Swapchain&& moveSwapchain)
{
    swapchain = std::move(moveSwapchain.swapchain);
    device = std::move(moveSwapchain.device);
    format = std::move(moveSwapchain.format);
    extent = std::move(moveSwapchain.extent);
    images = std::move(moveSwapchain.images);
}

///////////////////////////////////////////////////////////////////////////
Swapchain& Swapchain::operator=(Swapchain&& moveSwapchain)
{
    if (&moveSwapchain != this)
    {
        swapchain = std::move(moveSwapchain.swapchain);
        device = std::move(moveSwapchain.device);
        format = std::move(moveSwapchain.format);
        extent = std::move(moveSwapchain.extent);
        images = std::move(moveSwapchain.images);
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Swapchain::~Swapchain()
{
    for (auto& image : images)
        image.release();
}

///////////////////////////////////////////////////////////////////////////
const vk::SwapchainKHR& Swapchain::get() const
{
    return swapchain.get();
}

///////////////////////////////////////////////////////////////////////////
vk::SwapchainKHR& Swapchain::get()
{
    return swapchain.get();
}

///////////////////////////////////////////////////////////////////////////
const vk::Extent2D& Swapchain::getExtent() const
{
    return extent;
}

///////////////////////////////////////////////////////////////////////////
vk::Extent2D& Swapchain::getExtent()
{
    return extent;
}

///////////////////////////////////////////////////////////////////////////
const vk::Format& Swapchain::getFormat() const
{
    return format;
}

///////////////////////////////////////////////////////////////////////////
vk::Format& Swapchain::getFormat()
{
    return format;
}

///////////////////////////////////////////////////////////////////////////
const Images& Swapchain::getImages() const
{
    return images;
}

///////////////////////////////////////////////////////////////////////////
Images& Swapchain::getImages()
{
    return images;
}

///////////////////////////////////////////////////////////////////////////
std::optional<uint32_t> Swapchain::aquireNextImage(const Semaphore& signalAfter) const
{
    auto result = device.acquireNextImageKHR(swapchain.get(), UINT64_MAX, signalAfter.get());

    if (result.result == vk::Result::eErrorOutOfDateKHR)
        return std::nullopt;
    if (result.result != vk::Result::eSuccess && result.result != vk::Result::eSuboptimalKHR)
        throw std::runtime_error{"[Swapchain] Failed to acquire next image."};

    return result.value;
}

///////////////////////////////////////////////////////////////////////////
void Swapchain::recreate(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const Surface& surface, const Window& window)
{
    for (auto& image : images)
        image.release();
    images.clear();

    swapchain.reset();
    create(logicalDevice, physicalDevice, surface, window);
}

///////////////////////////////////////////////////////////////////////////
void Swapchain::create(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const Surface& surface, const Window& window)
{
    device = logicalDevice.get();

    auto details = physicalDevice.getSwapchainSupport(surface);

    vk::SurfaceFormatKHR bestFormat = details.formats[0];
    for (const auto& availableFormat : details.formats)
    {
        if (availableFormat.format == vk::Format::eR8G8B8A8Unorm && availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
            bestFormat = availableFormat;
    }

    vk::PresentModeKHR bestPresentMode = vk::PresentModeKHR::eFifo;
    for (const auto& availablePresentMode : details.presentModes)
    {
        if (availablePresentMode == vk::PresentModeKHR::eMailbox)
            bestPresentMode = availablePresentMode;
    }

    vk::Extent2D swapExtent;
    if(details.capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
    {
        swapExtent = details.capabilities.currentExtent;
    }
    else
    {
        int width;
        int height;
        glfwGetFramebufferSize(window.get(), &width, &height);

        swapExtent = vk::Extent2D{static_cast<uint32_t>(width), static_cast<uint32_t>(height)};

        swapExtent.width = std::clamp(swapExtent.width, details.capabilities.minImageExtent.width, details.capabilities.maxImageExtent.width);
        swapExtent.height = std::clamp(swapExtent.height, details.capabilities.minImageExtent.height, details.capabilities.maxImageExtent.height);
    }

    uint32_t imageCount = details.capabilities.minImageCount + 1;
    if (details.capabilities.maxImageCount > 0 && imageCount > details.capabilities.maxImageCount)
        imageCount = details.capabilities.maxImageCount;

    vk::SwapchainCreateInfoKHR createInfo{};
    createInfo.sType = vk::StructureType::eSwapchainCreateInfoKHR;
    createInfo.surface = surface.get();
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = bestFormat.format;
    createInfo.imageColorSpace = bestFormat.colorSpace;
    createInfo.imageExtent = swapExtent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled;

    auto queueFamilies = physicalDevice.getQueueFamilies(surface);
    auto queueSet = queueFamilies.toSet();
    std::vector<uint32_t> queueVector;
    if (queueSet.size() > 1)
    {
        queueVector.reserve(queueSet.size());
        queueVector.insert(queueVector.begin(), queueSet.begin(), queueSet.end());

        createInfo.imageSharingMode = vk::SharingMode::eConcurrent;
        createInfo.queueFamilyIndexCount = queueVector.size();
        createInfo.pQueueFamilyIndices = queueVector.data();
    }
    else
    {
        createInfo.imageSharingMode = vk::SharingMode::eExclusive;
    }

    createInfo.preTransform = details.capabilities.currentTransform;
    createInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
    createInfo.presentMode = bestPresentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    swapchain = device.createSwapchainKHRUnique(createInfo);

    auto vulkanImages = device.getSwapchainImagesKHR(swapchain.get());
    images.reserve(vulkanImages.size());
    for (auto& vulkanImage : vulkanImages)
        images.emplace_back(std::move(vulkanImage));

    format = bestFormat.format;
    extent = swapExtent;
}
