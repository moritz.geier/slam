#pragma once

// std
#include <type_traits>

// vulkan
#include <vulkan/vulkan.hpp>

#include "DescriptorSetLayout.hpp"
#include "LogicalDevice.hpp"
#include "RenderPass.hpp"
#include "Shader.hpp"
#include "VertexBuffer.hpp"

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
class Pipeline final
{
public:
    Pipeline(const LogicalDevice& device, const RenderPass& renderPass, const DescriptorSetLayout& setLayout, const std::vector<Shader>& shaders);
    Pipeline(Pipeline&&);
    Pipeline& operator=(Pipeline&&);
    Pipeline(const Pipeline&) = delete;
    Pipeline& operator=(const Pipeline&) = delete;
    ~Pipeline();

    const vk::Pipeline& get() const;
    vk::Pipeline& get();

    const vk::PipelineLayout& getLayout() const;
    vk::PipelineLayout& getLayout();

private:
    vk::UniquePipeline pipeline;
    vk::UniquePipelineLayout pipelineLayout;
};

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
Pipeline<Type>::Pipeline(const LogicalDevice& logicalDevice, const RenderPass& renderPass, const DescriptorSetLayout& setLayout, const std::vector<Shader>& shaders)
{
    auto attributes = Type::getAttributeDescriptions();
    auto binding = Type::getBindingDescription();

    vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
        vertexInputInfo.sType = vk::StructureType::ePipelineVertexInputStateCreateInfo;
    if constexpr (std::is_same<Type, EmptyVertex>::value)
    {
        vertexInputInfo.vertexBindingDescriptionCount = 0;
        vertexInputInfo.pVertexBindingDescriptions = nullptr;
        vertexInputInfo.vertexAttributeDescriptionCount = 0;
        vertexInputInfo.pVertexAttributeDescriptions = nullptr;
    }
    else
    {
        vertexInputInfo.vertexBindingDescriptionCount = 1;
        vertexInputInfo.pVertexBindingDescriptions = &binding;
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributes.size());
        vertexInputInfo.pVertexAttributeDescriptions = attributes.data();
    }

    vk::PipelineInputAssemblyStateCreateInfo inputAssembly{};
    inputAssembly.sType = vk::StructureType::ePipelineInputAssemblyStateCreateInfo;
    inputAssembly.topology = vk::PrimitiveTopology::eTriangleList;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    std::vector<vk::DynamicState> dynamicStates{vk::DynamicState::eViewport, vk::DynamicState::eScissor};
    vk::PipelineDynamicStateCreateInfo dynamicState{};
    dynamicState.sType = vk::StructureType::ePipelineDynamicStateCreateInfo;
    dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
    dynamicState.pDynamicStates = dynamicStates.data();

    vk::PipelineViewportStateCreateInfo viewportState{};
    viewportState.sType = vk::StructureType::ePipelineViewportStateCreateInfo;
    viewportState.viewportCount = 1;
    viewportState.scissorCount = 1;

    vk::PipelineRasterizationStateCreateInfo rasterizer{};
    rasterizer.sType = vk::StructureType::ePipelineRasterizationStateCreateInfo;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = vk::PolygonMode::eFill;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = vk::CullModeFlagBits::eBack;
    rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f;
    rasterizer.depthBiasClamp = 0.0f;
    rasterizer.depthBiasSlopeFactor = 0.0f;

    vk::PipelineMultisampleStateCreateInfo multisampling{};
    multisampling.sType = vk::StructureType::ePipelineMultisampleStateCreateInfo;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;
    multisampling.minSampleShading = 1.0f;
    multisampling.pSampleMask = nullptr;
    multisampling.alphaToCoverageEnable = VK_FALSE;
    multisampling.alphaToOneEnable = VK_FALSE;

    vk::PipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.srcColorBlendFactor = vk::BlendFactor::eOne;
    colorBlendAttachment.dstColorBlendFactor = vk::BlendFactor::eZero;
    colorBlendAttachment.colorBlendOp = vk::BlendOp::eAdd;
    colorBlendAttachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
    colorBlendAttachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
    colorBlendAttachment.alphaBlendOp = vk::BlendOp::eAdd;

    vk::PipelineColorBlendStateCreateInfo colorBlending{};
    colorBlending.sType = vk::StructureType::ePipelineColorBlendStateCreateInfo;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = vk::LogicOp::eCopy;
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f;
    colorBlending.blendConstants[1] = 0.0f;
    colorBlending.blendConstants[2] = 0.0f;
    colorBlending.blendConstants[3] = 0.0f;

    vk::PipelineLayoutCreateInfo pipelineLayoutInfo{};
    pipelineLayoutInfo.sType = vk::StructureType::ePipelineLayoutCreateInfo;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &setLayout.get();
    pipelineLayoutInfo.pushConstantRangeCount = 0;
    pipelineLayoutInfo.pPushConstantRanges = nullptr;

    pipelineLayout = logicalDevice->createPipelineLayoutUnique(pipelineLayoutInfo);

    std::vector<vk::PipelineShaderStageCreateInfo> shaderStages;
    for (const auto& shader : shaders)
    {
        vk::PipelineShaderStageCreateInfo shaderStageInfo{};
        shaderStageInfo.sType = vk::StructureType::ePipelineShaderStageCreateInfo;
        shaderStageInfo.stage = shader.getType();
        shaderStageInfo.module = shader.get();
        shaderStageInfo.pName = "main";
        shaderStages.push_back(shaderStageInfo);
    }

    vk::GraphicsPipelineCreateInfo pipelineInfo{};
    pipelineInfo.sType = vk::StructureType::eGraphicsPipelineCreateInfo;
    pipelineInfo.stageCount = shaderStages.size();
    pipelineInfo.pStages = shaderStages.data();
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = nullptr;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = &dynamicState;
    pipelineInfo.layout = pipelineLayout.get();
    pipelineInfo.renderPass = renderPass.get();
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
    pipelineInfo.basePipelineIndex = -1;

    pipeline = logicalDevice->createGraphicsPipelineUnique({}, pipelineInfo).value;
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
Pipeline<Type>::Pipeline(Pipeline<Type>&& movePipeline)
{
    pipeline = std::move(movePipeline.pipeline);
    pipelineLayout = std::move(movePipeline.pipelineLayout);
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
Pipeline<Type>& Pipeline<Type>::operator=(Pipeline<Type>&& movePipeline)
{
    if (&movePipeline != this)
    {
        pipeline = std::move(movePipeline.pipeline);
        pipelineLayout = std::move(movePipeline.pipelineLayout);
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
Pipeline<Type>::~Pipeline()
{
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
const vk::Pipeline& Pipeline<Type>::get() const
{
    return pipeline.get();
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
vk::Pipeline& Pipeline<Type>::get()
{
    return pipeline.get();
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
const vk::PipelineLayout& Pipeline<Type>::getLayout() const
{
    return pipelineLayout.get();
}

///////////////////////////////////////////////////////////////////////////
template<VertexType Type>
vk::PipelineLayout& Pipeline<Type>::getLayout()
{
    return pipelineLayout.get();
}