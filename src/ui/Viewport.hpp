#pragma once

// imgui
#include <imgui.h>

// slam
#include <algorithm/slam/Grid.hpp>

// std
#include <functional>

// utils
#include <utils/Definitions.hpp>

#include "internal/ViewportConfig.hpp"

namespace ui
{
    struct ViewportReturnInformation
    {
        float x;
        float y;
        float z;

        bool gridHasChanged;
        algorithm::slam::Grid<ViewportGridColorMode> grid;
    };

    ViewportReturnInformation& Viewport(ImTextureID id, std::function<void (size_t, size_t)> callback);
}
