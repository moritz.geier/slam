#include "DrawPoseGraph.hpp"

// algorithm
#include <algorithm/line/AntiAliasedLine.hpp>
#include <algorithm/slam/SlamTypes.hpp>

// std
#include <type_traits>

// utils
#include <utils/Definitions.hpp>

///////////////////////////////////////////////////////////////////////////
utils::PoseGraphResult utils::poseGraphView(algorithm::slam::Grid<ui::ViewportGridColorMode>& grid, const algorithm::slam::SlamGraph<utils::definitions::Timestep>& graph, const PoseGraphConfig& config)
{
    const Eigen::Vector2f origin{grid.width() / 2, grid.height() / 2};
    constexpr auto lineMode = std::is_same_v<ui::ViewportGridColorMode, utils::definitions::RGBA> ? algorithm::line::LineMode::eAdditive : algorithm::line::LineMode::eAdditive;

    float minRmse = std::numeric_limits<float>().infinity();
    float maxRmse = -std::numeric_limits<float>().infinity();
    float rmseSum = 0;
    size_t rmseCount = 0;

    for (const auto& node : graph.getNodes())
    {
        Eigen::Vector2f currentPose = node->get().pose.translation();
        currentPose *= config.scale;
        currentPose += origin;

        for (const auto& connection : node->getConnections())
        {
            const auto& neighbor = *connection.node.lock().get();

            Eigen::Vector2f neighborPose = neighbor->pose.translation();
            neighborPose *= config.scale;
            neighborPose += origin;

            switch(connection.edge->measurement)
            {
                case algorithm::slam::MeasurementType::eOdometry:
                    algorithm::line::antiAliasedLine(grid, currentPose, neighborPose, config.odometry, lineMode);
                    break;
                case algorithm::slam::MeasurementType::eLoopClosure:
                    algorithm::line::antiAliasedLine(grid, currentPose, neighborPose, config.loopClosure, lineMode);
                    rmseSum += connection.edge->rmse;
                    rmseCount++;
                    minRmse = std::min(minRmse, connection.edge->rmse);
                    maxRmse = std::max(maxRmse, connection.edge->rmse);
                    break;
            }
        }
    }

    return {minRmse, maxRmse, rmseSum / rmseCount};
}