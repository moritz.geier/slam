// catch2
#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

// cmake
#include <CMakeVariables.hpp>

// math
#include <math/Graph.hpp>

// slam
#include <algorithm/slam/ToGrid.hpp>
#include <algorithm/line/AntiAliasedLine.hpp>
#include <algorithm/scan_matching/Open3dIcp.hpp>

// std
#include <filesystem>
#include <ranges>

// utils
#include <utils/BMP.hpp>
#include <utils/Definitions.hpp>
#include <utils/LogReader.hpp>

///////////////////////////////////////////////////////////////////////////
namespace
{
    void drawFrame(algorithm::slam::Grid<utils::definitions::RGB>& grid, const Eigen::AffineCompact2f& frame)
    {
        constexpr float axisLength = 10.0f;

        const Eigen::Vector2f xAxis{axisLength, 0.0f};
        algorithm::line::antiAliasedLine(grid, frame.translation(), frame * xAxis, {255, 0, 0});

        const Eigen::Vector2f yAxis{0.0f, axisLength};
        algorithm::line::antiAliasedLine(grid, frame.translation(), frame * yAxis, {0, 255, 0});

        grid.insertSafe(static_cast<size_t>(frame.translation().x()), static_cast<size_t>(frame.translation().y()), {0, 0, 255});
    }

    ///////////////////////////////////////////////////////////////////////////
    algorithm::slam::Grid<utils::definitions::RGB> createGrid(const math::Graph<utils::definitions::Timestep, uint8_t>& graph, float scale)
    {
        algorithm::slam::ToGridConfiguration<utils::definitions::RGB> config;
        config.freeSpace = {255, 255, 255};
        config.unknownSpace = {128, 128, 128};
        config.occupiedSpace = {0, 0, 0};
        config.size = 1000;
        config.scale = scale;

        return algorithm::slam::toGrid(graph, config);
    }

    ///////////////////////////////////////////////////////////////////////////
    void insertPoses(algorithm::slam::Grid<utils::definitions::RGB>& grid, const math::Graph<utils::definitions::Timestep, uint8_t>& graph, float scale)
    {
        const Eigen::Vector2f center{grid.width() / 2, grid.height() / 2};
        Eigen::Vector2f lastTranslation = graph.getNodes().front()->get().pose.translation() * scale + center;
        for (const auto& node : graph.getNodes())
        {
            auto currentPose = node->get().pose;
            currentPose.translation() = currentPose.translation() * scale + center;

            const Eigen::Vector2f currentTranslation = currentPose.translation();
            algorithm::line::antiAliasedLine(grid, currentTranslation, lastTranslation, {0, 255, 255}, algorithm::line::LineMode::eOverride);
            lastTranslation = currentTranslation;

            drawFrame(grid, currentPose);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    void insertPointcloud(algorithm::slam::Grid<utils::definitions::RGB>& grid, const std::vector<Eigen::Vector2f>& data, const utils::definitions::RGB& color, float scale)
    {
        const Eigen::Vector2f center{grid.width() / 2, grid.height() / 2};
        for (const auto& date : data)
        {
            const auto scaledDate = date * scale + center;
            grid.insertSafe(scaledDate.x(), scaledDate.y(), color);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    void saveGrid(const algorithm::slam::Grid<utils::definitions::RGB>& grid, const std::filesystem::path& bmpPath)
    {
        utils::BMP bmp{grid};
        bmp.save(bmpPath);
    }
}

///////////////////////////////////////////////////////////////////////////
TEST_CASE("Testing out some reallife ICP alignments.", "[algorithm::icp]")
{
    constexpr float scale = 30.0f;
    const std::filesystem::path baseDirectory{std::filesystem::current_path() / "icp"};
    const std::filesystem::path sourceDirectory{CMake::programRootDirectory / "tests" / "algorithm" / "icp"};
    std::filesystem::create_directories(baseDirectory);

    math::Graph<utils::definitions::Timestep, uint8_t> graph;

    auto data = utils::LogReader::allSteps(sourceDirectory / "straight_hallway.log");

    if (data.empty())
        return;

    const Eigen::Vector2f firstTranslation = data[0].pose.translation();
    for (auto& date : data)
        date.pose.translation() = date.pose.translation() - firstTranslation;

    {
        auto* pLastNode = &graph.addNode(data[0]);
        for (auto pEntry = data.begin() + 1; pEntry != data.end(); pEntry++)
        {
            auto* pCurrentNode = &graph.addNode(*pEntry);
            graph.addEdge(0, *pCurrentNode, *pLastNode);
            pLastNode = pCurrentNode;
        }

        auto grid = createGrid(graph, scale);
        insertPoses(grid, graph, scale);
        saveGrid(grid, baseDirectory / "advanced_icp_tests_without_icp.bmp");
    }

    SECTION("Single step.")
    {
        for (const auto& node : graph.getNodes())
        {
            algorithm::slam::ToGridConfiguration<utils::definitions::RGB> config;
            config.freeSpace = {255, 255, 255};
            config.unknownSpace = {128, 128, 128};
            config.occupiedSpace = {0, 0, 0};
            config.size = 1000;
            config.scale = scale;

            auto step = node->get();
            const Eigen::Vector2f center{config.size / 2, config.size / 2};
            step.pose.translation() = center;
            auto grid = algorithm::slam::toGrid(step, config);
            drawFrame(grid, step.pose);
            saveGrid(grid, baseDirectory / std::format("advanced_icp_tests_single_step_{}.bmp", node->id()));
        }
    }

    SECTION("Incremental Poses")
    {
        algorithm::slam::Grid<utils::definitions::RGB> grid{1000, 1000};
        grid.fill({128, 128, 128});

        const Eigen::Vector2f center{grid.width() / 2, grid.height() / 2};

        auto pLastNode = graph.getNodes().front();
        const Eigen::Vector2f offset = center - pLastNode->get().pose.translation();
        pLastNode->get().pose.translation() = offset;
        drawFrame(grid, pLastNode->get().pose);

        for (auto pNode = ++graph.getNodes().begin(); pNode != graph.getNodes().end(); pNode++)
        {
            auto pCurrentValue = *pNode;
            pCurrentValue->get().pose.translation() = offset + pCurrentValue->get().pose.translation() * scale;
            const auto initialAlignment = pLastNode->get().pose.inverse() * pCurrentValue->get().pose; // pose - lastPose
            const auto calculatedPose = pLastNode->get().pose * initialAlignment;
            algorithm::line::antiAliasedLine(grid, pLastNode->get().pose.translation(), calculatedPose.translation(), {0, 255, 255});
            drawFrame(grid, calculatedPose);
            pLastNode = pCurrentValue;
        }
        saveGrid(grid, baseDirectory / "advanced_icp_incremantal_poses.bmp");
    }

    SECTION("Full test")
    {
        algorithm::Open3dIcp::Configuration icpConfig;
        icpConfig.threshold = 0.5f;

        algorithm::Open3dIcp icp{icpConfig};

        auto grid = createGrid(graph, scale);
        insertPoses(grid, graph, scale);

        const Eigen::Vector2f center{grid.width() / 2, grid.height() / 2};
        auto pLastNode = graph.getNodes().front();
        for (auto pNode = ++graph.getNodes().begin(); pNode != graph.getNodes().end(); pNode++)
        {
            auto pCurrentValue = *pNode;
            const auto initialAlignment = pLastNode->get().pose.inverse() * pCurrentValue->get().pose;
            const auto result = icp.findAlignment(pCurrentValue->get().measurements, pLastNode->get().measurements, initialAlignment);

            const Eigen::Vector2f currentTranslation = pCurrentValue->get().pose.translation() * scale + center;
            Eigen::AffineCompact2f correctedPose = pLastNode->get().pose * result.transformMatrix;
            pCurrentValue->get().pose = correctedPose;
            const Eigen::Vector2f correctedTranslation = correctedPose.translation() * scale + center;
            correctedPose.translation() = correctedTranslation;

            const auto reverseInitialAlignment = initialAlignment * pLastNode->get().pose;
            algorithm::line::antiAliasedLine(grid, reverseInitialAlignment.translation() * scale + center, pLastNode->get().pose.translation() * scale + center, {255, 255, 0}, algorithm::line::LineMode::eOverride);
            algorithm::line::antiAliasedLine(grid, currentTranslation, correctedTranslation, {0, 0, 255}, algorithm::line::LineMode::eOverride);

            drawFrame(grid, correctedPose);

            pLastNode = pCurrentValue;
        }

        saveGrid(grid, baseDirectory / "advanced_icp_tests_with_icp_error.bmp");

        auto fullGrid = createGrid(graph, scale);
        insertPoses(fullGrid, graph, scale);
        saveGrid(fullGrid, baseDirectory / "advanced_icp_tests_with_icp.bmp");

    }

    SECTION("Transformation test")
    {
        const auto initialAlignment = data[0].pose.inverse() * data[1].pose;

        {
            algorithm::slam::Grid<utils::definitions::RGB> grid{1000, 1000};
            grid.fill({255, 255, 255});
            insertPointcloud(grid, data[0].measurements, {0, 0, 0}, scale);

            auto dataTwo = data[1].measurements;
            for (auto& date : dataTwo)
                date = initialAlignment * date;
            insertPointcloud(grid, dataTwo, {0, 0, 255}, scale);
            saveGrid(grid, baseDirectory / "advanced_icp_transformation_test.bmp");
        }

        algorithm::Open3dIcp::Configuration icpConfig;
        icpConfig.threshold = 1.0f;
        algorithm::Open3dIcp icp{icpConfig};

        const auto result = icp.findAlignment(data[1].measurements, data[0].measurements, initialAlignment);

        {
            algorithm::slam::Grid<utils::definitions::RGB> grid{1000, 1000};
            grid.fill({255, 255, 255});
            insertPointcloud(grid, data[0].measurements, {0, 0, 0}, scale);

            const auto newPose = data[0].pose * result.transformMatrix;
            auto dataTwo = data[1].measurements;
            const auto initialAlignment = data[0].pose.inverse() * newPose;
            for (auto& date : dataTwo)
                date = initialAlignment * date;
            insertPointcloud(grid, dataTwo, {0, 0, 255}, scale);
            saveGrid(grid, baseDirectory / "advanced_icp_transformation_test_after_icp.bmp");
        }
    }
}