#pragma once

// std
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

class LogicalDevice;

///////////////////////////////////////////////////////////////////////////
class Fence
{
public:
    Fence(const LogicalDevice& logicalDevice, bool isSignaled);
    Fence(Fence&&);
    Fence& operator=(Fence&&);
    Fence(const Fence&) = delete;
    Fence& operator=(const Fence&) = delete;
    ~Fence();

    const vk::Fence& get() const;
    vk::Fence& get();

    void wait() const;
    void reset() const;

private:
    vk::UniqueFence fence;
    vk::Device device;
};
