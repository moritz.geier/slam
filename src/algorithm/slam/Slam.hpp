#pragma once

#include "SlamTypes.hpp"

///////////////////////////////////////////////////////////////////////////
namespace algorithm::slam
{
    ///////////////////////////////////////////////////////////////////////////
    template<utils::concepts::Timestep StepType>
    SlamGraph<StepType> solve(const SlamGraph<StepType>& inputGraph, float& progress, const SolveConfiguration<StepType>& config)
    {
        progress = 0;

        auto graph = config.frontend(inputGraph, config, progress);

        return graph;
    }

    ///////////////////////////////////////////////////////////////////////////
    template<utils::concepts::Timestep StepType>
    SlamGraph<StepType> solve(const SlamGraph<StepType>& inputGraph, const SolveConfiguration<StepType>& config)
    {
        float progress = 0;
        solve(inputGraph, progress, config);
    }
}