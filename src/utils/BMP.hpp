#pragma once

// std
#include <filesystem>
#include <fstream>
#include <stdint.h>
#include <vector>

// slam
#include <algorithm/slam/Grid.hpp>

namespace utils
{
    namespace definitions
    {
        struct RGB;
        struct RGBA;
    }

    ///////////////////////////////////////////////////////////////////////////
    struct BmpHeader
    {
        const uint16_t magicNumber = 0x4D42; // 'BM'
        uint32_t fileSize;
        uint16_t appSpecific1;
        uint16_t appSpecific2;
        uint32_t offsetBitmap;

        static constexpr size_t size() { return 14; };
        void writeTo(std::ostream& output) const;
    };

    ///////////////////////////////////////////////////////////////////////////
    struct DibHeader
    {
        const uint32_t headerSize = 40;
        uint32_t width;
        uint32_t height;
        uint16_t colorPlanes;
        uint16_t bitsPerPixel;
        uint32_t compression;
        uint32_t imageSize;
        uint32_t horizontalResolution;
        uint32_t verticalResolution;
        uint32_t paletteSize;
        uint32_t importantColors;

        static constexpr size_t size() { return 40; };
        void writeTo(std::ostream& output) const;
    };

    ///////////////////////////////////////////////////////////////////////////
    class BMP final
    {
    public:
        BMP(const algorithm::slam::Grid<uint8_t>& grid);
        BMP(const algorithm::slam::Grid<definitions::RGB>& grid);
        BMP(const algorithm::slam::Grid<definitions::RGBA>& grid);
        ~BMP();

        const BmpHeader& getBmpHeader() const;
        const DibHeader& getDibHeader() const;
        const std::vector<definitions::RGB>& getImageData() const;

        void setResolution(uint32_t resolution);

        void save(const std::filesystem::path& bmp) const;

    private:
        BmpHeader bmpHeader;
        DibHeader dibHeader;
        std::vector<definitions::RGB> imageData;
    };
}