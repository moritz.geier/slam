#pragma once

// utils
#include <utils/Definitions.hpp>

namespace algorithm::line
{
    using PointType = float;
    using SignedPointType = float;

    ///////////////////////////////////////////////////////////////////////////
    template<typename T>
    inline bool isLineThroughPointsShallow(utils::definitions::Point2D<T> pointOne, utils::definitions::Point2D<T>pointTwo)
    {
        return std::abs(static_cast<float>(pointOne.x()) - static_cast<float>(pointTwo.x())) > std::abs(static_cast<float>(pointOne.y()) - static_cast<float>(pointTwo.y()));
    }
}
