#pragma once

// glfw
#include <GLFW/glfw3.h>

// std
#include <functional>
#include <string>

class Window final
{
public:
    Window(size_t height, size_t width, const std::string& name);
    Window(Window&&);
    Window& operator=(Window&&);
    Window(const Window&) = delete;
    Window& operator=(const Window&) = delete;
    ~Window();

    GLFWwindow* get() const;

    size_t getHeight() const;
    size_t getWidth() const;

    bool isMinimized();
    bool shouldClose() const;
    void pollEvents() const;

    using ResizeCallback = std::function<void(Window&, size_t, size_t)>;
    void onResize(ResizeCallback callback);

    static void onResizeGLFWCallback(GLFWwindow* window, int width, int height);

private:
    size_t height;
    size_t width;
    GLFWwindow* window;
    bool shouldDestroy;
    ResizeCallback resizeCallback;
};