#include "String.hpp"

///////////////////////////////////////////////////////////////////////////
std::vector<std::string> utils::string::split(const std::string& toSplit, const std::string& delimiter)
{
    const size_t delimiterLength = delimiter.length();
    std::vector<std::string> splitStrings;
    size_t position;
    size_t lastPosition = 0;

    if (toSplit.empty())
        return {};

    if (delimiter.empty())
        return {toSplit};

    while ((position = toSplit.find(delimiter, lastPosition)) != std::string::npos)
    {
        const size_t substringLength = position - lastPosition;
        const auto substring = toSplit.substr(lastPosition, substringLength);
        splitStrings.emplace_back(substring);
        lastPosition = position + delimiterLength;
    }

    const auto substring = toSplit.substr(lastPosition);
    splitStrings.emplace_back(substring);

    return splitStrings;
}
