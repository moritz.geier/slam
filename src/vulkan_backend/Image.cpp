#include "Image.hpp"
#include "CommandBuffer.hpp"
#include "LogicalDevice.hpp"
#include "PhysicalDevice.hpp"

// glfw
#include <GLFW/glfw3.h>

// std
#include <algorithm>
#include <limits>

///////////////////////////////////////////////////////////////////////////
Image::Image(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const vk::Format& format, const vk::Extent2D& extent, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties)
{
    vk::ImageCreateInfo imageCreateInfo{};
    imageCreateInfo.sType = vk::StructureType::eImageCreateInfo;
    imageCreateInfo.imageType = vk::ImageType::e2D;
    imageCreateInfo.extent.width = extent.width;
    imageCreateInfo.extent.height = extent.height;
    imageCreateInfo.extent.depth = 1;
    imageCreateInfo.mipLevels = 1;
    imageCreateInfo.arrayLayers = 1;
    imageCreateInfo.format = format;
    imageCreateInfo.tiling = tiling;
    imageCreateInfo.initialLayout = vk::ImageLayout::eUndefined;
    imageCreateInfo.usage = usage;
    imageCreateInfo.samples = vk::SampleCountFlagBits::e1;
    imageCreateInfo.sharingMode = vk::SharingMode::eExclusive;

    image = logicalDevice->createImageUnique(imageCreateInfo);

    auto requirements = logicalDevice->getImageMemoryRequirements(image.get());
    vk::MemoryAllocateInfo allocateInfo{};
    allocateInfo.sType = vk::StructureType::eMemoryAllocateInfo;
    allocateInfo.allocationSize = requirements.size;
    allocateInfo.memoryTypeIndex = physicalDevice.getMemoryTypeIndex(requirements.memoryTypeBits, properties);

    imageMemory = logicalDevice->allocateMemoryUnique(allocateInfo);
    logicalDevice->bindImageMemory(image.get(), imageMemory.get(), 0);
}

///////////////////////////////////////////////////////////////////////////
Image::Image(Image&& moveImage)
{
    image = std::move(moveImage.image);
    imageMemory = std::move(moveImage.imageMemory);
}

///////////////////////////////////////////////////////////////////////////
Image& Image::operator=(Image&& moveImage)
{
    if (this == &moveImage)
        return *this;

    image = std::move(moveImage.image);
    imageMemory = std::move(moveImage.imageMemory);

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Image::Image(const vk::Image& moveImage)
{
    image = vk::UniqueImage{moveImage};
    imageMemory = {};
}

///////////////////////////////////////////////////////////////////////////
Image& Image::operator=(const vk::Image& moveImage)
{
    image = vk::UniqueImage{moveImage};
    imageMemory = {};

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Image::~Image()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::Image& Image::get() const
{
    return image.get();
}

///////////////////////////////////////////////////////////////////////////
vk::Image& Image::get()
{
    return image.get();
}

///////////////////////////////////////////////////////////////////////////
const vk::DeviceMemory& Image::getMemory() const
{
    return imageMemory.get();
}

///////////////////////////////////////////////////////////////////////////
vk::DeviceMemory& Image::getMemory()
{
    return imageMemory.get();
}

///////////////////////////////////////////////////////////////////////////
void Image::release()
{
    image.release();
}

///////////////////////////////////////////////////////////////////////////
void Image::changeLayout(const vk::CommandBuffer& buffer, const vk::ImageLayout& oldLayout, const vk::ImageLayout& newLayout) const
{
    vk::ImageMemoryBarrier barrier{};
    barrier.sType = vk::StructureType::eImageMemoryBarrier;
    barrier.image = image.get();
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    vk::PipelineStageFlags sourceStage;
    vk::PipelineStageFlags destinationStage;

    if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eTransferDstOptimal)
    {
        barrier.srcAccessMask = vk::AccessFlagBits::eNone;
        barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

        sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
        destinationStage = vk::PipelineStageFlagBits::eTransfer;
    }
    else if (oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal)
    {
        barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
        barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

        sourceStage = vk::PipelineStageFlagBits::eTransfer;
        destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
    }
    else if (oldLayout == vk::ImageLayout::ePresentSrcKHR && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal)
    {
        barrier.srcAccessMask = vk::AccessFlagBits::eColorAttachmentRead;
        barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

        sourceStage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
        destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
    }
    else
    {
        throw std::invalid_argument("unsupported layout transition!");
    }

    buffer.pipelineBarrier(sourceStage, destinationStage, vk::DependencyFlagBits::eByRegion, {}, {}, barrier);
}

///////////////////////////////////////////////////////////////////////////
Images::Images(const LogicalDevice& device, const PhysicalDevice& physicalDevice, const vk::Format& format, const vk::Extent2D& extent, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties, size_t count)
: std::vector<Image>{}
{
    reserve(count);

    for (size_t i = 0; i < count; i++)
        push_back(Image{device, physicalDevice, format, extent, tiling, usage, properties});
}

///////////////////////////////////////////////////////////////////////////
Images::Images(std::vector<Image>&& images)
: std::vector<Image>{std::move(images)}
{
}

///////////////////////////////////////////////////////////////////////////
Images::Images()
{
}