#pragma once

// utils
#include <utils/Definitions.hpp>

namespace algorithm::slam
{
    template<typename T>
    struct ToGridConfiguration;
}

namespace ui
{
    using ViewportGridColorMode = utils::definitions::RGBA;

    ///////////////////////////////////////////////////////////////////////////
    ViewportGridColorMode getPathColor();

    ///////////////////////////////////////////////////////////////////////////
    ViewportGridColorMode getLoopClosureColor();

    ///////////////////////////////////////////////////////////////////////////
    algorithm::slam::ToGridConfiguration<ViewportGridColorMode> getGridConfiguration(float scale);
}
