#pragma once

// vulkan
#include <vulkan/vulkan.hpp>

class LogicalDevice;

///////////////////////////////////////////////////////////////////////////
class Sampler final
{
public:
    Sampler(const LogicalDevice& device);
    Sampler(Sampler&&);
    Sampler& operator=(Sampler&&);
    Sampler(const Sampler&) = delete;
    Sampler& operator=(const Sampler&) = delete;
    ~Sampler();

    const vk::Sampler& get() const;
    vk::Sampler& get();

private:
    vk::UniqueSampler sampler;
};
