#include "Sprite.hpp"

// utils
#include <utils/Definitions.hpp>

// vulkan
#include <vulkan_backend/CommandBuffer.hpp>
#include <vulkan_backend/CommandPool.hpp>
#include <vulkan_backend/LogicalDevice.hpp>
#include <vulkan_backend/PhysicalDevice.hpp>

///////////////////////////////////////////////////////////////////////////
render::Sprite::Sprite(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t width, size_t height)
: textureFormat{vk::Format::eR8G8B8A8Unorm}
, logicalDevice{logicalDevice}
, dimensions{static_cast<uint32_t>(width), static_cast<uint32_t>(height)}
, stagingBuffer{logicalDevice, physicalDevice, width * height}
, texture{logicalDevice, physicalDevice, textureFormat, dimensions, vk::ImageTiling::eLinear, vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal}
, textureView{logicalDevice, textureFormat, texture}
{
}

///////////////////////////////////////////////////////////////////////////
render::Sprite::Sprite(Sprite&& sprite)
: textureFormat{sprite.textureFormat}
, logicalDevice{sprite.logicalDevice}
, dimensions{sprite.dimensions}
, stagingBuffer{std::move(sprite.stagingBuffer)}
, texture{std::move(sprite.texture)}
, textureView{std::move(sprite.textureView)}
{
}

///////////////////////////////////////////////////////////////////////////
render::Sprite::~Sprite()
{
}

///////////////////////////////////////////////////////////////////////////
const vk::ImageView& render::Sprite::get() const
{
    return textureView.get();
}

///////////////////////////////////////////////////////////////////////////
vk::ImageView& render::Sprite::get()
{
    return textureView.get();
}

///////////////////////////////////////////////////////////////////////////
const Image& render::Sprite::getImage() const
{
    return texture;
}

///////////////////////////////////////////////////////////////////////////
Image& render::Sprite::getImage()
{
    return texture;
}

///////////////////////////////////////////////////////////////////////////
const render::Sprite::TextureDataType* render::Sprite::data() const
{
    return stagingBuffer.data();
}

///////////////////////////////////////////////////////////////////////////
render::Sprite::TextureDataType* render::Sprite::data()
{
    return stagingBuffer.data();
}

///////////////////////////////////////////////////////////////////////////
void render::Sprite::upload(const CommandPool& commandPool) const
{
    auto commandBuffer = CommandBuffer::singleTimeBegin(logicalDevice, commandPool);
    texture.changeLayout(commandBuffer, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal);
    CommandBuffer::singleTimeEnd(logicalDevice, commandPool, commandBuffer);

    commandBuffer = CommandBuffer::singleTimeBegin(logicalDevice, commandPool);
    
    vk::BufferImageCopy region{};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;

    region.imageOffset = vk::Offset3D{0, 0, 0};
    region.imageExtent = vk::Extent3D{dimensions.width, dimensions.height, 1};

    commandBuffer.copyBufferToImage(stagingBuffer.get(), texture.get(), vk::ImageLayout::eTransferDstOptimal, 1, &region);

    CommandBuffer::singleTimeEnd(logicalDevice, commandPool, commandBuffer);

    commandBuffer = CommandBuffer::singleTimeBegin(logicalDevice, commandPool);
    texture.changeLayout(commandBuffer, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);
    CommandBuffer::singleTimeEnd(logicalDevice, commandPool, commandBuffer);
}

///////////////////////////////////////////////////////////////////////////
size_t render::Sprite::size() const
{
    return dimensions.height * dimensions.width;
}

///////////////////////////////////////////////////////////////////////////
render::Sprites::Sprites(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, size_t width, size_t height, size_t amount)
: std::vector<Sprite>{}
{
    for (size_t i = 0; i < amount; i++)
        emplace_back(logicalDevice, physicalDevice, width, height);
}

///////////////////////////////////////////////////////////////////////////
std::vector<vk::ImageView> render::Sprites::toImageViews() const
{
    std::vector<vk::ImageView> imageViews;
    imageViews.reserve(size());

    for (const auto& sprite : *this)
        imageViews.push_back(sprite.get());

    return imageViews;
}