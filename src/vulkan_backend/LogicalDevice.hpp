#pragma once

// std
#include <vector>

// vulkan
#include <vulkan/vulkan.hpp>

class CommandBuffer;
class Fence;
class PhysicalDevice;
class Semaphore;
class Surface;

class LogicalDevice final
{
public:
    LogicalDevice(const PhysicalDevice& physicalDevice, const Surface& surface, const std::vector<const char*>& extensions);
    LogicalDevice(LogicalDevice&&);
    LogicalDevice& operator=(LogicalDevice&&);
    LogicalDevice(const LogicalDevice&) = delete;
    LogicalDevice& operator=(const LogicalDevice&) = delete;
    ~LogicalDevice();

    const vk::Device& get() const;
    vk::Device& get();

    const vk::UniqueDevice& operator->() const;
    vk::UniqueDevice& operator->();

    const vk::Queue& getGraphicsQueue() const;
    vk::Queue& getGraphicsQueue();

    void submit(const std::vector<vk::CommandBuffer>& buffers, const Fence& setAfter, const Semaphore& waitBefore, const Semaphore& signalAfter) const;
    bool present(const uint32_t imageIndex, const std::vector<vk::SwapchainKHR>& swapchain, const Semaphore& waitBefore) const;

    void waitForIdle() const;

private:
    vk::UniqueDevice device;
    float queuePriorities;
    vk::Queue graphicsQueue;
    vk::Queue presentationQueue;
};