#include "File.hpp"

///////////////////////////////////////////////////////////////////////////
std::string utils::file::readAll(std::ifstream& file, size_t chunkSize)
{
    std::string stringFile;
    std::string buffer(chunkSize, '\0');
    while (file.read(buffer.data(), chunkSize))
        stringFile.append(buffer, 0, file.gcount());
    stringFile.append(buffer, 0, file.gcount());

    return stringFile;
}
