# Build ui library
set(
SOURCES
    internal/ViewportConfig.cpp
    internal/ViewportFullMap.cpp
    internal/ViewportReplay.cpp
    Benchmark.cpp
    Toggle.cpp
    Viewport.cpp
)

add_library(slam_ui ${SOURCES})
target_link_libraries(slam_ui slam_utils imgui nfd slam_scanmatcher)