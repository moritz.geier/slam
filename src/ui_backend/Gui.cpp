#include "Gui.hpp"

// cmake
#include <CMakeVariables.hpp>

// imgui
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_vulkan.h>
#include <imgui_internal.h>

// std
#include <filesystem>

// vulkan
#include <vulkan_backend/CommandPool.hpp>
#include <vulkan_backend/Instance.hpp>
#include <vulkan_backend/LogicalDevice.hpp>
#include <vulkan_backend/Semaphore.hpp>
#include <vulkan_backend/Surface.hpp>
#include <vulkan_backend/PhysicalDevice.hpp>
#include <vulkan_backend/Window.hpp>

///////////////////////////////////////////////////////////////////////////
Gui::Gui(const Window& window, const Instance& instance, const PhysicalDevice& physicalDevice, const LogicalDevice& logicalDevice, const Surface& surface, const CommandPool& commandPool, const size_t framesInFlight)
: swapchain{logicalDevice, physicalDevice, surface, window}
, swapchainImages{logicalDevice, swapchain.getFormat(), swapchain.getImages()}
, renderPass{logicalDevice, swapchain.getFormat(), vk::ImageLayout::ePresentSrcKHR}
, framebuffers{logicalDevice, renderPass, swapchainImages, swapchain.getExtent()}
, commandBuffers{ui::CommandBuffer::allocate(logicalDevice, commandPool, framesInFlight)}
, descriptorPool{logicalDevice}
, currentFrame{0}
, imageIndex{std::nullopt}
, framesInFlight{framesInFlight}
, clearColor{0.45f, 0.55f, 0.60f, 1.00f}
, showDemoWindow{true}
, showAnotherWindow{false}
{
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;         // Enable Docking
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;       // Enable Multi-Viewport / Platform Windows

    const std::filesystem::path iniPath = std::filesystem::current_path() / "imgui.ini";
    if (!std::filesystem::exists(iniPath))
    {
        const std::filesystem::path defaultIniPath = CMake::programRootDirectory / "resources" / "default_windows.ini";
        if (!std::filesystem::exists(defaultIniPath))
            throw std::runtime_error{"Couldn't find default ini file at " + defaultIniPath.string()};
        std::filesystem::copy_file(defaultIniPath, iniPath);
    }

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForVulkan(window.get(), true);
    ImGui_ImplVulkan_InitInfo initInfo = {};
    initInfo.Instance = instance.get();
    initInfo.PhysicalDevice = physicalDevice.get();
    initInfo.Device = logicalDevice.get();
    initInfo.QueueFamily = physicalDevice.getQueueFamilies(surface).graphics.value();
    initInfo.Queue = logicalDevice.getGraphicsQueue();
    initInfo.PipelineCache = nullptr;
    initInfo.DescriptorPool = descriptorPool.get();
    initInfo.Subpass = 0;
    initInfo.MinImageCount = 2;
    initInfo.ImageCount = framesInFlight;
    initInfo.MSAASamples = VK_SAMPLE_COUNT_1_BIT;
    initInfo.Allocator = nullptr;
    initInfo.CheckVkResultFn = nullptr;
    ImGui_ImplVulkan_Init(&initInfo, renderPass.get());
}

///////////////////////////////////////////////////////////////////////////
Gui::~Gui()
{
    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

///////////////////////////////////////////////////////////////////////////
const std::array<float, 4>& Gui::getClearColor() const
{
    return clearColor;
}

///////////////////////////////////////////////////////////////////////////
std::array<float, 4>& Gui::getClearColor()
{
    return clearColor;
}

///////////////////////////////////////////////////////////////////////////
const Swapchain& Gui::getSwapchain() const
{
    return swapchain;
}

///////////////////////////////////////////////////////////////////////////
Swapchain& Gui::getSwapchain()
{
    return swapchain;
}

///////////////////////////////////////////////////////////////////////////
void Gui::recreateSwapchain(const LogicalDevice& logicalDevice, const PhysicalDevice& physicalDevice, const Surface& surface, const Window& window)
{
    framebuffers.clear();
    swapchainImages.clear();

    swapchain.recreate(logicalDevice, physicalDevice, surface, window);

    swapchainImages = ImageViews{logicalDevice, swapchain.getFormat(), swapchain.getImages()};
    const auto& extent = swapchain.getExtent();
    framebuffers = Framebuffers{logicalDevice, renderPass, swapchainImages, extent};
}

///////////////////////////////////////////////////////////////////////////
std::optional<uint32_t> Gui::newFrame(const Semaphore& imageAvailable)
{
    imageIndex = swapchain.aquireNextImage(imageAvailable);

    if (imageIndex.has_value())
    {
        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        ImGui::DockSpaceOverViewport(ImGui::GetMainViewport());
    }

    return imageIndex;
}

///////////////////////////////////////////////////////////////////////////
void Gui::demo()
{
    // Our state
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
    if (showDemoWindow)
        ImGui::ShowDemoWindow(&showDemoWindow);

    // 2. Show a simple window that we create ourselves. We use a Begin/End pair to create a named window.
    {
        static float f = 0.0f;
        static int counter = 0;

        ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

        ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
        ImGui::Checkbox("Demo Window", &showDemoWindow);      // Edit bools storing our window open/close state
        ImGui::Checkbox("Another Window", &showAnotherWindow);

        ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
        ImGui::ColorEdit3("clear color", clearColor.data()); // Edit 3 floats representing a color

        if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
            counter++;
        ImGui::SameLine();
        ImGui::Text("counter = %d", counter);

        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);
        ImGui::End();
    }

    // 3. Show another simple window.
    if (showAnotherWindow)
    {
        ImGui::Begin("Another Window", &showAnotherWindow);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
        ImGui::Text("Hello from another window!");
        if (ImGui::Button("Close Me"))
            showAnotherWindow = false;
        ImGui::End();
    }
}

///////////////////////////////////////////////////////////////////////////
const CommandBuffer& Gui::render()
{
    // Our state
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Rendering
    ImGui::Render();

    // Update and Render additional Platform Windows
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
    }

    if(imageIndex == std::nullopt)
        throw std::runtime_error{"[Gui] Tried to render without starting a new frame."};

    commandBuffers[currentFrame].record(renderPass, framebuffers[imageIndex.value()], swapchain, clearColor);
    const auto& currentCommandBuffer = commandBuffers[currentFrame];
    currentFrame = (currentFrame + 1) % framesInFlight;
    return currentCommandBuffer;
}

///////////////////////////////////////////////////////////////////////////
void Gui::recover()
{
    ImGui::ErrorCheckEndFrameRecover({});
}