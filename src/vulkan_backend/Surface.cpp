#include "Surface.hpp"
#include "Instance.hpp"
#include "Window.hpp"

///////////////////////////////////////////////////////////////////////////
Surface::Surface(const Instance& instance, const Window& window)
: instance{instance.get()}
{
    VkSurfaceKHR temporarySurface;
    if (glfwCreateWindowSurface(instance.get(), window.get(), nullptr, &temporarySurface) != VK_SUCCESS)
        throw std::runtime_error{"[Surface] Could not create window surface"};
    surface = vk::UniqueSurfaceKHR{temporarySurface};
}

///////////////////////////////////////////////////////////////////////////
Surface::Surface(Surface&& moveSurface)
{
    surface = std::move(moveSurface.surface);
    instance = std::move(moveSurface.instance);
}

///////////////////////////////////////////////////////////////////////////
Surface& Surface::operator=(Surface&& moveSurface)
{
    if (&moveSurface != this)
    {
        surface = std::move(moveSurface.surface);
        instance = std::move(moveSurface.instance);
    }

    return *this;
}

///////////////////////////////////////////////////////////////////////////
Surface::~Surface()
{
    instance.destroySurfaceKHR(surface.get());
    surface.release();
}

///////////////////////////////////////////////////////////////////////////
const vk::SurfaceKHR& Surface::get() const
{
    return surface.get();
}

///////////////////////////////////////////////////////////////////////////
vk::SurfaceKHR& Surface::get()
{
    return surface.get();
}