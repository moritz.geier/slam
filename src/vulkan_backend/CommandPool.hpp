#pragma once

// vulkan
#include <vulkan/vulkan.hpp>

class LogicalDevice;

///////////////////////////////////////////////////////////////////////////
class CommandPool final
{
public:
    CommandPool(const LogicalDevice& device, uint32_t queueFamilyIndex);
    CommandPool(CommandPool&&);
    CommandPool& operator=(CommandPool&&);
    CommandPool(const CommandPool&) = delete;
    CommandPool& operator=(const CommandPool&) = delete;
    ~CommandPool();

    const vk::CommandPool& get() const;
    vk::CommandPool& get();

private:
    vk::UniqueCommandPool commandPool;
};
