#pragma once

// eigen
#include <Eigen/Dense>

// std
#include <algorithm>
#include <concepts>
#include <limits>
#include <memory>
#include <queue>
#include <ranges>
#include <vector>

#include "INearestNeighbor.hpp"

///////////////////////////////////////////////////////////////////////////
namespace algorithm
{
    ///////////////////////////////////////////////////////////////////////////
    template<typename T, typename Comp>
    class FixedSizedMaximumHeap : std::priority_queue<T, std::vector<T>, Comp>
    {
    public:
        FixedSizedMaximumHeap(size_t size, const T& init);
        virtual ~FixedSizedMaximumHeap();

        const T& top() const;
        void pushPop(const T& value);
        const std::vector<T>& get() const;
    };

    ///////////////////////////////////////////////////////////////////////////
    template<NearestNeighborEntry EntryType>
    class KdTree : public INearestNeighbors<EntryType>
    {
    private:
        static constexpr int Dimensions = decltype(EntryType::coordinate)::RowsAtCompileTime;
    public:
        struct Node
        {
            EntryType value;
            std::unique_ptr<Node> smallerChild;
            std::unique_ptr<Node> biggerChild;

            Node();
        };

        KdTree();
        virtual ~KdTree();

        void set(const std::vector<EntryType>& referencePoints) override;
        INearestNeighbors<EntryType>::Result find(const Eigen::Vector<float, Dimensions>& point) const override;
        std::vector<typename INearestNeighbors<EntryType>::Result> find(const Eigen::Vector<float, Dimensions>& point, size_t amount) const;
        std::vector<typename INearestNeighbors<EntryType>::Result> findInRadius(const Eigen::Vector<float, Dimensions>& point, float radius) const;

        const Node* data() const;
        Node* data();

    private:
        struct PointIsCloser
        {
            bool operator()(const INearestNeighbors<EntryType>::Result& left,const INearestNeighbors<EntryType>::Result& right) const;
        };
        using MaximumHeap = FixedSizedMaximumHeap<typename INearestNeighbors<EntryType>::Result, PointIsCloser>;

        std::unique_ptr<Node> root;

        using ConstructionIterator = typename std::vector<EntryType>::iterator;
        static void recursiveTreeConstruction(std::unique_ptr<Node>& node, ConstructionIterator childrenBegin, ConstructionIterator childrenEnd, size_t dimension);
        static void recursiveTreeSearch(Node* node, const Eigen::Vector<float, Dimensions>& point, MaximumHeap& bestResults, size_t dimension);
        static void recursiveRadiusTreeSearch(Node* node, const Eigen::Vector<float, Dimensions>& point, std::vector<typename INearestNeighbors<EntryType>::Result>& bestResults, float radius, size_t dimension);
    };
}

///////////////////////////////////////////////////////////////////////////
template<typename T, typename Comp>
algorithm::FixedSizedMaximumHeap<T, Comp>::FixedSizedMaximumHeap(size_t amount, const T& init)
: std::priority_queue<T, std::vector<T>, Comp>{}
{
    std::priority_queue<T, std::vector<T>, Comp>::c.reserve(amount);
    for (size_t i = 0; i < amount; i++)
        std::priority_queue<T, std::vector<T>, Comp>::push(init);
}

///////////////////////////////////////////////////////////////////////////
template<typename T, typename Comp>
algorithm::FixedSizedMaximumHeap<T, Comp>::~FixedSizedMaximumHeap()
{
}

///////////////////////////////////////////////////////////////////////////
template<typename T, typename Comp>
const T& algorithm::FixedSizedMaximumHeap<T, Comp>::top() const
{
    return std::priority_queue<T, std::vector<T>, Comp>::top();
}

///////////////////////////////////////////////////////////////////////////
template<typename T, typename Comp>
void algorithm::FixedSizedMaximumHeap<T, Comp>::pushPop(const T& value)
{
    std::priority_queue<T, std::vector<T>, Comp>::pop();
    std::priority_queue<T, std::vector<T>, Comp>::push(value);
}

///////////////////////////////////////////////////////////////////////////
template<typename T, typename Comp>
const std::vector<T>& algorithm::FixedSizedMaximumHeap<T, Comp>::get() const
{
    return std::priority_queue<T, std::vector<T>, Comp>::c;
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
algorithm::KdTree<EntryType>::Node::Node()
: value{}
, smallerChild{nullptr}
, biggerChild{nullptr}
{
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
algorithm::KdTree<EntryType>::KdTree()
: root{std::make_unique<Node>()}
{
    static_assert(Dimensions > 0);
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
algorithm::KdTree<EntryType>::~KdTree()
{
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
void algorithm::KdTree<EntryType>::set(const std::vector<EntryType>& referencePoints)
{
    std::vector<EntryType> points{referencePoints};
    recursiveTreeConstruction(root, points.begin(), points.end(), 0);
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
algorithm::INearestNeighbors<EntryType>::Result algorithm::KdTree<EntryType>::find(const Eigen::Vector<float, Dimensions>& point) const
{
    const auto result = find(point, 1);
    if (result.empty())
        return {nullptr, std::numeric_limits<float>().infinity()};
    return result[0];
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
std::vector<typename algorithm::INearestNeighbors<EntryType>::Result> algorithm::KdTree<EntryType>::find(const Eigen::Vector<float, Dimensions>& point, size_t amount) const
{
    if (amount == 0)
        return {};

    MaximumHeap nearest{amount, {nullptr, std::numeric_limits<float>().infinity()}};
    recursiveTreeSearch(root.get(), point, nearest, 0);

    auto resultsView = nearest.get()
        | std::views::filter([](const auto& element) { return element.pNeighbor != nullptr; })
        | std::views::transform([](const auto& element) { return typename INearestNeighbors<EntryType>::Result{element.pNeighbor, std::sqrt(element.distance)}; })
        | std::views::reverse
        | std::views::common;
    std::vector<typename INearestNeighbors<EntryType>::Result> results = {resultsView.begin(), resultsView.end()};
    std::sort(results.begin(), results.end(), [](const auto& left, const auto& right) { return left.distance < right.distance; });
    return results;
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
std::vector<typename algorithm::INearestNeighbors<EntryType>::Result> algorithm::KdTree<EntryType>::findInRadius(const Eigen::Vector<float, Dimensions>& point, float radius) const
{
    if (radius <= 0)
        return {};

    std::vector<typename INearestNeighbors<EntryType>::Result> nearest;
    recursiveRadiusTreeSearch(root.get(), point, nearest, radius, 0);

    std::ranges::sort(nearest, {}, &INearestNeighbors<EntryType>::Result::distance);
    for (auto& value : nearest)
        value.distance = std::sqrt(value.distance);
    return nearest;
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
const typename algorithm::KdTree<EntryType>::Node* algorithm::KdTree<EntryType>::data() const
{
    return root.get();
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
algorithm::KdTree<EntryType>::Node* algorithm::KdTree<EntryType>::data()
{
    return root.get();
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
void algorithm::KdTree<EntryType>::recursiveTreeConstruction(std::unique_ptr<Node>& node, ConstructionIterator childrenBegin, ConstructionIterator childrenEnd, size_t dimension)
{
    if (childrenBegin == childrenEnd)
    {
        node = nullptr;
        return;
    }

    const size_t center = std::distance(childrenBegin, childrenEnd) / 2;
    std::nth_element(childrenBegin, childrenBegin + center, childrenEnd, [dimension](const auto& left, const auto& right) { return left.coordinate[dimension] < right.coordinate[dimension]; });

    const size_t nextDimension = (dimension + 1) % Dimensions;
    node->value = std::move(childrenBegin[center]);
    node->smallerChild = std::make_unique<Node>();
    node->biggerChild = std::make_unique<Node>();

    recursiveTreeConstruction(node->smallerChild, childrenBegin, childrenBegin + center, nextDimension);
    recursiveTreeConstruction(node->biggerChild, childrenBegin + center + 1, childrenEnd, nextDimension);
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
void algorithm::KdTree<EntryType>::recursiveTreeSearch(Node* node, const Eigen::Vector<float, Dimensions>& point, MaximumHeap& bestResults, size_t dimension)
{
    if (node == nullptr)
        return;

    // Set current canidate for nearest point
    const float distance = (point - node->value.coordinate).squaredNorm();
    if (bestResults.top().distance > distance)
    {
        bestResults.pushPop({&node->value, distance});
    }

    // Select search order
    const float dimensionDeltaToCurrentPoint = node->value.coordinate[dimension] - point[dimension];
    Node* pCheckFirst;
    Node* pCheckSecond;
    if (0 < dimensionDeltaToCurrentPoint)
    {
        pCheckFirst = node->smallerChild.get();
        pCheckSecond = node->biggerChild.get();
    }
    else
    {
        pCheckFirst = node->biggerChild.get();
        pCheckSecond = node->smallerChild.get();
    }

    // Check first branch
    const size_t nextDimension = (dimension + 1) % Dimensions;
    recursiveTreeSearch(pCheckFirst, point, bestResults, nextDimension);

    // If necessary check second branch
    const float dimensionDeltaToCurrentPointSquared = dimensionDeltaToCurrentPoint * dimensionDeltaToCurrentPoint;
    if (dimensionDeltaToCurrentPointSquared < bestResults.top().distance)
        recursiveTreeSearch(pCheckSecond, point, bestResults, nextDimension);
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
void algorithm::KdTree<EntryType>::recursiveRadiusTreeSearch(Node* node, const Eigen::Vector<float, Dimensions>& point, std::vector<typename INearestNeighbors<EntryType>::Result>& bestResults, float radius, size_t dimension)
{
    if (node == nullptr)
        return;

    // Set current canidate for nearest point
    const float distance = (point - node->value.coordinate).squaredNorm();
    if (radius * radius >= distance)
        bestResults.emplace_back(&node->value, distance);

    const auto nextDimension = (dimension + 1) % Dimensions;
    const auto valueDimension = node->value.coordinate[dimension];
    const auto pointDimension = point[dimension];
    if (pointDimension - radius < valueDimension)
        recursiveRadiusTreeSearch(node->smallerChild.get(), point, bestResults, radius, nextDimension);
    if (pointDimension + radius >= valueDimension)
        recursiveRadiusTreeSearch(node->biggerChild.get(), point, bestResults, radius, nextDimension);
}

///////////////////////////////////////////////////////////////////////////
template<algorithm::NearestNeighborEntry EntryType>
bool algorithm::KdTree<EntryType>::PointIsCloser::operator()(const INearestNeighbors<EntryType>::Result& left,const INearestNeighbors<EntryType>::Result& right) const
{
    return left.distance < right.distance;
}