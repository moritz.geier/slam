#include "Open3dIcp.hpp"

// open3d
#include <open3d/pipelines/registration/GeneralizedICP.h>
#include <open3d/geometry/PointCloud.h>

///////////////////////////////////////////////////////////////////////////
algorithm::Open3dIcp::Open3dIcp(const Configuration& configuration)
: threshold{configuration.threshold}
{
}

///////////////////////////////////////////////////////////////////////////
algorithm::Open3dIcp::~Open3dIcp()
{
}

///////////////////////////////////////////////////////////////////////////
// TODO: PCD format?
// TODO: Corresponding best vs Corresponding free
// TODO: Benchmark max rot and transform
// TODO: https://github.com/MIT-SPARK/TEASER-plusplus
// TODO: PCL
// TODO: ML Scanmatching
// TODO: https://github.com/PRBonn/kiss-icp
// TODO: Different distributions in pointcould, e.g. different sensors
algorithm::IScanMatching<utils::definitions::Timestep>::AlignmentResult algorithm::Open3dIcp::findAlignment(IScanMatching<StepType>::PointcloudType pointsToMatch, const IScanMatching<StepType>::PointcloudType& referencePoints, const IScanMatching<StepType>::TransformationType& initialAlignment)
{
    std::vector<Eigen::Vector3d> pointsToMatch3d;
    pointsToMatch3d.resize(pointsToMatch.size());
    std::transform(pointsToMatch.begin(), pointsToMatch.end(), pointsToMatch3d.begin(), [](const auto& point) { return Eigen::Vector3d{static_cast<double>(point.x()), static_cast<double>(point.y()), 0.0}; });

    std::vector<Eigen::Vector3d> referencePoints3d;
    referencePoints3d.resize(referencePoints.size());
    std::transform(referencePoints.begin(), referencePoints.end(), referencePoints3d.begin(), [](const auto& point) { return Eigen::Vector3d{static_cast<double>(point.x()), static_cast<double>(point.y()), 0.0}; });

    open3d::geometry::PointCloud pointCloud{pointsToMatch3d};
    open3d::geometry::PointCloud referencePointCloud{referencePoints3d};

    Eigen::Matrix4d initialAlignment3d;
    initialAlignment3d.setIdentity();
    initialAlignment3d.block<2,2>(0,0) = initialAlignment.matrix().block<2,2>(0, 0).cast<double>();
    initialAlignment3d.block<2,1>(0,3) = initialAlignment.matrix().block<2,1>(0, 2).cast<double>();

    const auto result = open3d::pipelines::registration::RegistrationGeneralizedICP(pointCloud, referencePointCloud, threshold, initialAlignment3d);

    algorithm::IScanMatching<StepType>::AlignmentResult icpResult;
    icpResult.transformMatrix.matrix().block<2, 2>(0, 0) = result.transformation_.block<2, 2>(0, 0).cast<float>();
    icpResult.transformMatrix.matrix().block<2, 1>(0, 2) = result.transformation_.block<2, 1>(0, 3).cast<float>();
    icpResult.error = static_cast<float>(result.inlier_rmse_);
    return icpResult;
}
