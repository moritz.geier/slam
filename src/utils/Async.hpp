#pragma once

// std
#include <functional>
#include <future>
#include <type_traits>

///////////////////////////////////////////////////////////////////////////
namespace utils
{
    ///////////////////////////////////////////////////////////////////////////
    template<typename ReturnType, typename ...Args>
    class Async
    {
    public:
        Async();
        Async(const std::function<ReturnType(Args...)>& asyncFunction);
        Async<ReturnType, Args...>& operator=(const std::function<ReturnType(Args...)>& asyncFunction);
        Async(Async<ReturnType, Args...>&&) = delete;
        Async<ReturnType, Args...>& operator=(Async<ReturnType, Args...>&&) = delete;
        Async(const Async<ReturnType, Args...>&) = delete;
        Async<ReturnType, Args...>& operator=(const Async<ReturnType, Args...>&) = delete;
        virtual ~Async();

        virtual void start(Args... args);

        inline bool isRunning() const;
        inline bool isFinished() const;
        inline void wait() const;
        inline ReturnType getResult();

    protected:
        static ReturnType asyncRunner(bool& isRunning, bool& isFinished, const std::function<ReturnType(Args...)>& function, Args... args);

    private:
        bool isAsyncRunning;
        bool isAsyncFinished;
        std::function<ReturnType(Args...)> asyncFunction;
        std::future<ReturnType> future;

    };
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
utils::Async<ReturnType, Args...>::Async()
: isAsyncRunning{false}
, isAsyncFinished{false}
, asyncFunction{}
, future{}
{
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
utils::Async<ReturnType, Args...>::Async(const std::function<ReturnType(Args...)>& asyncFunction)
: isAsyncRunning{false}
, isAsyncFinished{false}
, asyncFunction{asyncFunction}
, future{}
{
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
utils::Async<ReturnType, Args...>& utils::Async<ReturnType, Args...>::operator=(const std::function<ReturnType(Args...)>& asyncFunction)
{
    isAsyncRunning = false;
    isAsyncFinished = false;
    this->asyncFunction = asyncFunction;
    future = {};
    return *this;
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
utils::Async<ReturnType, Args...>::~Async()
{
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
void utils::Async<ReturnType, Args...>::start(Args... args)
{
    if (asyncFunction != nullptr)
        future = std::async(std::launch::async, asyncRunner, std::ref(isAsyncRunning), std::ref(isAsyncFinished), asyncFunction, args...);
    else
        throw std::runtime_error{"[utils::Async::start()] Invoking this function without valid function to call. Check wheter you passed in a function to call in the constructor."};
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
bool utils::Async<ReturnType, Args...>::isRunning() const
{
    return isAsyncRunning;
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
bool utils::Async<ReturnType, Args...>::isFinished() const
{
    return isAsyncFinished;
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
void utils::Async<ReturnType, Args...>::wait() const
{
    return future.wait();
}

///////////////////////////////////////////////////////////////////////////
/// @brief Returns result of async function. The function waits until the result
///        is available. The behavior is undefined if Async::isFinished is false
///         and you call this function. `Async::isFinished()`
///        is false after this call.
/// @return Result of the function specified in the constructor.
template<typename ReturnType, typename ...Args>
ReturnType utils::Async<ReturnType, Args...>::getResult()
{
    isAsyncFinished = false;
    return future.get();
}

///////////////////////////////////////////////////////////////////////////
template<typename ReturnType, typename ...Args>
ReturnType utils::Async<ReturnType, Args...>::asyncRunner(bool& isRunning, bool& isFinished, const std::function<ReturnType(Args...)>& function, Args... args)
{
    isRunning = true;
    if constexpr (std::is_same_v<ReturnType, void>)
    {
        function(args...);
        isRunning = false;
        isFinished = true;
    }
    else
    {
        const auto result = function(args...);
        isRunning = false;
        isFinished = true;
        return result;
    }
}
