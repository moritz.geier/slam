#pragma once

// eigen
#include <Eigen/Geometry>

// std
#include <chrono>
#include <filesystem>
#include <fstream>
#include <optional>
#include <vector>

namespace utils
{
    struct Relation
    {
        std::string idOne;
        std::string idTwo;
        Eigen::AffineCompact2f transformation;
    };

    class RelationsReader final
    {
    public:
        RelationsReader(const std::filesystem::path& log);
        ~RelationsReader();

        std::optional<Relation> nextStep();

        static std::vector<Relation> allSteps(const std::filesystem::path& log, float& progress);
        static std::vector<Relation> allSteps(const std::filesystem::path& log);

    private:
        std::ifstream logFile;

        static std::ifstream openFile(const std::filesystem::path& log);
        static std::optional<Relation> parseLine(const std::string& line);
    };
}